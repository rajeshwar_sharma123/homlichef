//
//  HMCSignUpModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 25/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCSignUpModel {
    var identityProof       : UIImage!
    var addressProof        : UIImage!
    var bankProof           : UIImage!
    var profilePicture      : UIImage!
    var identityProofUrl    : String!
    var addressProofUrl     : String!
    var bankProofUrl        : String!
    var profilePictureUrl   : String!
    var cuisines            = NSMutableArray()
    var numberOfPlate       : String!
    var veg                 : Bool! = false
    var nonVeg              : Bool! = false
    var cuisinesArry        = [HMCCuisinesModel]()
    var imgArry             = NSMutableArray()
    
    static let instance = HMCSignUpModel()
    class func sharedInstance() -> HMCSignUpModel
    {
        return instance;
    }
    
    func setInitialValue()
    {
        identityProof = nil
        addressProof = nil
        bankProof = nil
        profilePicture = nil
        identityProofUrl = ""
        addressProofUrl = ""
        bankProofUrl = ""
        profilePictureUrl = ""
        cuisines = NSMutableArray()
        numberOfPlate = ""
        veg  = false
        nonVeg = false
        
        
    }
    
    
}