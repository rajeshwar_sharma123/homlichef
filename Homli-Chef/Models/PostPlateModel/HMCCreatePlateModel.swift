//
//  HMCCreatePlateModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 22/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCCreatePlateModel {
    var mainCourse          = NSDictionary()
    var subCourse1          = NSDictionary()
    var subCourse2          = NSDictionary()
    var completeCourseName  = ""
    var numberOfPlate       = "1"
    var fromWhereStr        = ""
    var plateCode           = ""
    var plateName           = ""
    var plateDescription    = ""
    var flag                = false
    static let instance = HMCCreatePlateModel()
    class func sharedInstance() -> HMCCreatePlateModel
    {
        return instance;
}
    func setInitialValue()
    {
        mainCourse = NSDictionary()
        subCourse1 = NSDictionary()
        subCourse2 = NSDictionary()
        completeCourseName = ""
        numberOfPlate = "1"
        fromWhereStr  = ""
        plateCode = ""
        plateName = ""
        plateDescription = ""
        flag  = false

        
        
    }
    
    
    func bindRecommendedDataWithModel(dic:NSDictionary, numberOfPlate:String){
        self.subCourse1 = NSDictionary()
        self.subCourse2 = NSDictionary()
        self.fromWhereStr = PostPlateCheck.RECOMMENDATION.description
        self.numberOfPlate = numberOfPlate
        self.plateCode = dic["plateCode"] as! String
        self.plateName = dic["plateName"] as! String
        self.plateDescription = dic["plateDescription"] as! String
        
        let dishesArr = dic["dishes"] as! NSArray
        for i in 0  ..< dishesArr.count  {
            let dishes = dishesArr.objectAtIndex(i) as! NSDictionary
            if dishes["dishCategory"] as! String == DishCategoryCourse.MAIN_COURSE.description{
                self.mainCourse = dishes
            }
            else if dishes["dishCategory"] as! String == DishCategoryCourse.SUB_COURSE.description && self.subCourse1.count == 0{
                self.subCourse1 = dishes
            }
            else if dishes["dishCategory"] as! String == DishCategoryCourse.SUB_COURSE.description && self.subCourse2.count == 0{
                self.subCourse2 = dishes
            }
            
        }
    }
    
    func bindPreviousPlateDataWithModel(prviousPlate:HMCPreviousPlateModel){
        
        self.fromWhereStr = PostPlateCheck.PREVIOUS_PLATE.description
        self.numberOfPlate = "1"
        self.plateCode = prviousPlate.plateCode!
        self.plateName = prviousPlate.plateName!
        self.plateDescription = prviousPlate.plateDescription!
        
        let dishesArr = prviousPlate.dishes
        
        for i in 0  ..< dishesArr.count  {
            let dish = dishesArr[i]
            
            let dishes : NSDictionary = [
                "dishId": dish["dishId"],
                "dishCode": dish["dishCode"],
                "dishName": dish["dishName"],
                "dishCategory": dish["dishCategory"],
                "dishDescription": dish["dishDescription"],
            ]
            print(dishes)
            if dishes["dishCategory"] as! String == DishCategoryCourse.MAIN_COURSE.description{
                self.mainCourse = dishes
            }
            else if dishes["dishCategory"] as! String  == DishCategoryCourse.SUB_COURSE_1.description{
                self.subCourse1 = dishes
            }
            else if dishes["dishCategory"] as! String == DishCategoryCourse.SUB_COURSE_2.description{
                self.subCourse2 = dishes
            }
            
        }
        
        
    }


}