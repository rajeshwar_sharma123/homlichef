//
//  HMCDishesModel.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/21/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation

class HMCDishesModel {
    var dishId                      : String!
    var dishCode                    : String!
    var dishName                    : String!
    var dishCategory                : String!
    var dishDescription             : String!
}
