//
//  HMCRecommendationModel.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/22/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCRecommendationModel {
    var chefMobileNumber                   : String?
    var recommendations                    = []
    class func bindDataWithModel(recommendationList:NSArray) -> [HMCRecommendationListModel]
    {
        
        var recommendationInfo = [HMCRecommendationListModel]()
        for plates  in recommendationList  {
            let plates = plates as! NSDictionary
            let plate = HMCRecommendationListModel()
            plate.plateName = plates["plateName"] as? String
            plate.plateCode = plates["plateCode"] as? String
            plate.numberOfPlates = String(plates["numberOfPlates"] as! Int)
            recommendationInfo.append(plate)
            
        }
        
        return recommendationInfo
    }
    
}