//
//  HMCPreviousPlatesModel.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/21/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation

class HMCPreviousPlateModel {
    var plateId                   : String?
    var plateName                 : String?
    var plateDescription          : String?
    var dishes                      = []
    var plateCode                   : String?
    class func bindDataWithModel(platesList:NSArray) -> [HMCPreviousPlateModel]
    {
       
        var platesInfo = [HMCPreviousPlateModel]()
        for plate  in platesList  {
              let order = HMCPreviousPlateModel()
          let plate = plate as! NSDictionary
            order.plateName = plate["plateName"] as? String
            order.plateId = plate["plateId"] as? String
            order.plateCode = plate["plateCode"] as? String
            order.plateDescription = plate["plateDescription"] as? String
            order.dishes =  plate["dishes"] as! [HMCDishesModel]
           
            platesInfo.append(order)
          
    }
        
    return platesInfo
    }
    
}