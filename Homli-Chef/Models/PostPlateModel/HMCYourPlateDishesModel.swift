//
//  HMCYourPlateDishesModel.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/22/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCYourPlateDishesModel
{
    var dishId                      : String!
    var dishCode                    : String!
    var dishName                    : String!
    var dishCategory                : String!
    var dishDescription             : String!
}
