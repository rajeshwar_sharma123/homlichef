//
//  HMCYourPlateModel.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/22/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCYourPlateModel {
    var noOfPlates                :String?
    var plateId                   : String?
    var plateName                 : String?
    var plateDescription          : String?
    var dishes                      = []
    var plateCode                   : String?
    class func bindDataWithModel(platesList:NSArray) -> [HMCYourPlateModel]
    {
        
        var platesInfo = [HMCYourPlateModel]()
        
        for plateobj  in platesList  {
            let order = HMCYourPlateModel()
            let plateobj = plateobj as! NSDictionary
            
            let plate = plateobj["plate"] as! NSDictionary
            
            var prz = plate["numberOfPlates"] as? Int
            if prz == nil{
               prz = 0
            }
            order.noOfPlates = String(prz!)
            order.plateName = plate["plateName"] as? String
            order.plateId = plate["plateId"] as? String
            order.plateCode = plate["plateCode"] as? String
            order.plateDescription = plate["plateDescription"] as? String
            order.dishes =  plate["dishes"] as! [HMCYourPlateDishesModel]
            platesInfo.append(order)
            
        }
        
        return platesInfo
    }
    
}