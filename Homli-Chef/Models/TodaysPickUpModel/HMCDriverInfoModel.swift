//
//  HMCDriverInfoModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 19/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCDriverInfoModel {
    var driverName                   : String!
    var driverMobile                 : String!
    var pickupTime                   : String!
    var driverPlates                 = []
    
    class func bindDataWithModel(driversList:NSArray) -> [HMCDriverInfoModel]
    {
        var driverInfo = [HMCDriverInfoModel]()
        for dic  in driversList  {
            let dic = dic as! NSDictionary
            let order = HMCDriverInfoModel()
            
            let dicDriver = dic["driverInfo"] as! NSDictionary
            
            order.driverName = dicDriver["driverName"] as? String
            order.driverMobile = dicDriver["driverMobile"] as? String
            order.pickupTime = dicDriver["pickupTime"] is NSNull ? "" :dicDriver["pickupTime"] as? String
            order.driverPlates =  dic["driverPlates"] as!  [HMCDriverPlatesModel]
            driverInfo.append(order)
            
        }
        return driverInfo
    }
    
}