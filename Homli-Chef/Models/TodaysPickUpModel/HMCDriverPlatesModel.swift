//
//  HMCDriverPlatesModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 19/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCDriverPlatesModel {
    var plateCode                     : String!
    var plateName                     : String!
    var orderNumber                   : String!
    var numberOfPlates                : String!
    var addressInfo                   : String!
 }
