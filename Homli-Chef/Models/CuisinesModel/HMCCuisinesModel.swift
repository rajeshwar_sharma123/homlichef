//
//  HMCCuisinesModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 30/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCCuisinesModel {
    var cuisine                : String!
    var cuisineDescription     : String!
    var cuisineId              : String!
    
   class func bindDataWithModel(cuisines:NSArray) -> [HMCCuisinesModel]
    {
    var cuisinesObj = [HMCCuisinesModel]()
        for dic in cuisines{
        let dic = dic as! NSDictionary
            let tmpCuisine = HMCCuisinesModel()
            tmpCuisine.cuisine = dic["cuisine"] as! String
            tmpCuisine.cuisineDescription = dic["cuisineDescription"] as! String
            tmpCuisine.cuisineId =  String(dic["cuisineId"] as! Int)
            cuisinesObj.append(tmpCuisine)
       
        }
        return cuisinesObj
    }
    
}