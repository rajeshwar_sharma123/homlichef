//
//  HMCYourOrderModel.swift
//  Homli-Chef
//
//  Created by daffolapmac on 18/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCYourOrderModel {
    var plateName                 : String!
    var plateCode                 : String!
    var numberOfPlates            : String!
    var orderItemIds              = []
    var isExpanded                = false
    var isChild                   = false
    class func bindDataWithModel(cuisines:NSArray) -> [HMCYourOrderModel]
    {
        //let orderList = NSMutableArray()
        var orderList = [HMCYourOrderModel]()
        for dic in cuisines{
            let dic = dic as! NSDictionary
                let order = HMCYourOrderModel()
                order.plateName = dic["plateName"] as! String
                order.plateCode = dic["plateCode"] as! String
                order.numberOfPlates =  String(dic["numberOfPlates"] as! Int)
                order.orderItemIds =  dic["orderItemIds"] as!  [HMCOrderItem]
                orderList.append(order)
            
            
          
        }
        return orderList
    }
    
}