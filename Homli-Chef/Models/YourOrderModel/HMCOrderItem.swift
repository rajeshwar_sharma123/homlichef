//
//  HMCOrderItem.swift
//  Homli-Chef
//
//  Created by daffolapmac on 18/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCOrderItem {
    var orderItemId                     : String!
    var orderItemStatus                 : String!
}