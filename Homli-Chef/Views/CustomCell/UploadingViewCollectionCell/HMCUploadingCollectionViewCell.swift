//
//  HMCUploadingCollectionViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins_New on 5/16/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCUploadingCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imgViewDone: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       // self.contentView.frame = self.bounds
    }

}
