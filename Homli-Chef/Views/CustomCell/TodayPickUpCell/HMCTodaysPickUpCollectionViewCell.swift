//
//  HMCTodaysPickUpCollectionViewCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 19/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTodaysPickUpCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblDriverName: UILabel!
    @IBOutlet var driverImgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        driverImgView.layer.cornerRadius = driverImgView.frame.size.width/2
        driverImgView.clipsToBounds = true
    }

}
