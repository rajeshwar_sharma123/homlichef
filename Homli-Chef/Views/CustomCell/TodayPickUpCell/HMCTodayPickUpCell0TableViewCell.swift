//
//  HMCTodayPickUpCell0TableViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/15/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTodayPickUpCell0TableViewCell: UITableViewCell {

    @IBOutlet weak var deriverName: UILabel!
    @IBOutlet var driverImgView: UIImageView!
    @IBOutlet weak var deriverMobileNo: UILabel!
    @IBOutlet weak var contactLineView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //deriverprofileimageview.layer.cornerRadius = 30
        driverImgView.layer.cornerRadius = driverImgView.frame.size.width/2
        driverImgView.clipsToBounds = true

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
