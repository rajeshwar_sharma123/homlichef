//
//  HMCTodayPickUpCell1TableViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/15/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTodayPickUpCell1TableViewCell: UITableViewCell {
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
