//
//  HMCOrderDeatailCell1.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/25/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCOrderDeatailCell1: UITableViewCell {

    @IBOutlet weak var nameOfDeriver: UILabel!
    @IBOutlet weak var mobileNo: UILabel!
    @IBOutlet weak var userImgView : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        userImgView.layer.cornerRadius = userImgView.frame.size.width/2
        userImgView.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
