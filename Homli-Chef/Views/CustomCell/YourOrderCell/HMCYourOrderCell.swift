//
//  HMCYourOrderCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 18/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCYourOrderCell: UITableViewCell {
    @IBOutlet weak var lblDishName: UILabel!
    @IBOutlet weak var lblCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCount.layer.cornerRadius = 4
        lblCount.clipsToBounds = true
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
    }
    
}
