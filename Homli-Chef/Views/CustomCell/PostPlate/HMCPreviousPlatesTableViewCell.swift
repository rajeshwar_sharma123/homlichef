//
//  HMCPreviousPlatesTableViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/21/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCPreviousPlatesTableViewCell: UITableViewCell {

    @IBOutlet weak var detailOfReceipe: UILabel!
    @IBOutlet weak var nameOfReceipe: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
