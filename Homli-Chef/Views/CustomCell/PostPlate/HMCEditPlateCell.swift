//
//  HMCEditPlateCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 24/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCEditPlateCell: UITableViewCell {
    @IBOutlet var lblPlateCount: UILabel!

    @IBOutlet var btnDecrease: UIButton!
    @IBOutlet var btnIncrease: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
