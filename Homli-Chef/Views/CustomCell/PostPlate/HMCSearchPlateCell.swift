//
//  HMCSearchPlateCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 21/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCSearchPlateCell: UITableViewCell {
    @IBOutlet var lblPlateName: UILabel!

    @IBOutlet var lblPlateDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
