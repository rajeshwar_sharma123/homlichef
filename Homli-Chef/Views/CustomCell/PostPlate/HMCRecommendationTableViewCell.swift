//
//  HMCRecommendationTableViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/21/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCRecommendationTableViewCell: UITableViewCell {

    @IBOutlet weak var forwordImage: UIImageView!
  
    @IBOutlet weak var noOfPlates: UILabel!
    @IBOutlet weak var detailOfPlates: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        noOfPlates.clipsToBounds = true
        noOfPlates.layer.cornerRadius = 2
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
