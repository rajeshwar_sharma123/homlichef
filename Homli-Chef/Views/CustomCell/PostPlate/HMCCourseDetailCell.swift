//
//  HMCCourseDetailCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 24/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCCourseDetailCell: UITableViewCell {
    @IBOutlet var lblCourseName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
