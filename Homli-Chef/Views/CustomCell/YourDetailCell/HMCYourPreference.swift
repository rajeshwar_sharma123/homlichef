//
//  HMCYourPreference.swift
//  Homli-Chef
//
//  Created by daffolapmac on 04/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCYourPreference: UITableViewCell {

    @IBOutlet var lblTypeName: UILabel!
    @IBOutlet var lblCuisineName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
