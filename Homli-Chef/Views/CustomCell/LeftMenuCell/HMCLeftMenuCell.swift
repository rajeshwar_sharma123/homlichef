//
//  HMCLeftMenuCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 28/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCLeftMenuCell: UITableViewCell {
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        imgView.layer.cornerRadius = imgView.frame.size.width/2
//        imgView.clipsToBounds = true

    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
