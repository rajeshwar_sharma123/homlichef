//
//  HMCChooseCuisinesCell.swift
//  Homli-Chef
//
//  Created by daffolapmac on 29/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCChooseCuisinesCell: UITableViewCell {

    @IBOutlet var imgViewCheckBox: UIImageView!
    @IBOutlet var lblCuisineName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(cuisineObj:HMCCuisinesModel)
    {
        
        
      let signUpModel = HMCSignUpModel.sharedInstance()
      lblCuisineName.text = cuisineObj.cuisine
        if !signUpModel.cuisines.containsObject(cuisineObj){
         self.imgViewCheckBox.image = UIImage(named: "icn_checkbox_unselected")
        }
        else
        {
            self.imgViewCheckBox.image = UIImage(named: "icn_checkbox_selected")
        }
    }
    
}
