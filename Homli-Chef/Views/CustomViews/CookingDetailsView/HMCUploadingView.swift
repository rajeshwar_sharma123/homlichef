//
//  HMCUploadingView.swift
//  Homli-Chef
//
//  Created by Jenkins_New on 5/16/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCUploadingView: UIView {

    @IBOutlet weak var collectionView: UICollectionView!
    let signUpModel = HMCSignUpModel.sharedInstance()
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func initWithDefaultValue(){
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.registerNib(UINib(nibName: "HMCUploadingCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HMCUploadingCollectionViewCell")

    }
    
    // MARK:- CollectionView DataSource and Delegate Method
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return signUpModel.imgArry.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let dic = signUpModel.imgArry.objectAtIndex(indexPath.row) as! NSMutableDictionary
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HMCUploadingCollectionViewCell", forIndexPath: indexPath) as! HMCUploadingCollectionViewCell
        cell.imgView.image = dic["image"] as? UIImage
        
        let checkDone = dic["checkDone"] as! Bool
        if checkDone{
            cell.indicator.hidden = true
            cell.imgViewDone.hidden = false
            cell.indicator.stopAnimating()
        }
        else{
            cell.indicator.hidden = false
            cell.imgViewDone.hidden = true
            cell.indicator.startAnimating()
        }
        
        
        
        return cell
    }
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: (screenWidth-36)/2, height: 140) // The size of one cell
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(12, 12, 12, 12) // margin between cells
    }

    @IBAction func tapGestureAction(sender: AnyObject) {
        
        //self.removeFromSuperview()
    }
    
    func reloadData(){
        collectionView.reloadData()
    }
    
    
}
