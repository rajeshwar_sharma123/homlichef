//
//  HMCCookingDetailsView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 29/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import AWSS3
protocol HMCCookingDetailsViewDelegate{
    func setCuisinesViewForTextField(textField:UITextField)
}
class HMCCookingDetailsView: UIView,UITextFieldDelegate {
    var completionHandler : AWSS3TransferUtilityUploadCompletionHandlerBlock!
    var delegate:HMCCookingDetailsViewDelegate! = nil
    @IBOutlet var txtFldChooseCuisine: HMCTextField!
    @IBOutlet var txtFldNumberOfPlates: HMCTextField!
    @IBOutlet var btnVeg: UIButton!
    @IBOutlet var btnNonVeg: UILabel!
    let signUpModel = HMCSignUpModel.sharedInstance()
    var count = 0
    var urlCount = 0
    var uploadingView:HMCUploadingView!
    let keyNameArr = NSMutableArray()
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        txtFldChooseCuisine.addButtonOnRightWithImageName("ic_arrow_down")
        
        
    }
    // MARK:- Custome Button Action
    
    
    //=========vegbutton=======
    @IBAction func vegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        //let signUpModel = HMCSignUpModel.sharedInstance()
        if(btn.tag==0)
        {
            signUpModel.veg = true
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            signUpModel.veg = false
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
    }
    
    //=======nonveg button=======
    @IBAction func nonVegButtonAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        //let signUpModel = HMCSignUpModel.sharedInstance()
        if(btn.tag==0)
        {
            signUpModel.nonVeg = true
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            signUpModel.nonVeg = false
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    // MARK:- UITextFieldDelegate Method
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if textField.isEqual(txtFldChooseCuisine)
        {
            if textField.isAskingCanBecomeFirstResponder == false{
                
                delegate.setCuisinesViewForTextField(textField)
                textField.resignFirstResponder()
                
            }
            
            
            return false
            
        }
        return true;
    }
    func textFieldDidEndEditing(textField: UITextField) {
        if textField.isEqual(txtFldNumberOfPlates)
        {
            //let signUpModel = HMCSignUpModel.sharedInstance()
            signUpModel.numberOfPlate = textField.text
            
        }
        
    }
    func addDataInImgArr(){
        signUpModel.imgArry.removeAllObjects()
        var mutDic = NSMutableDictionary()
        mutDic.setObject(signUpModel.identityProof, forKey: "image")
        mutDic.setObject(false, forKey: "checkDone")
        signUpModel.imgArry.addObject(mutDic)
        
        mutDic = NSMutableDictionary()
        mutDic.setObject(signUpModel.addressProof, forKey: "image")
        mutDic.setObject(false, forKey: "checkDone")
        signUpModel.imgArry.addObject(mutDic)
        
        mutDic = NSMutableDictionary()
        mutDic.setObject(signUpModel.bankProof, forKey: "image")
        mutDic.setObject(false, forKey: "checkDone")
        signUpModel.imgArry.addObject(mutDic)
        
        mutDic = NSMutableDictionary()
        mutDic.setObject(signUpModel.profilePicture, forKey: "image")
        mutDic.setObject(false, forKey: "checkDone")
        signUpModel.imgArry.addObject(mutDic)
        
        
    }
    
    func setUploadingImageView(){
        addDataInImgArr()
        let signUpVC = HMCUtlities.getTopViewController()
        uploadingView =  NSBundle.mainBundle().loadNibNamed("HMCUploadingView", owner: self, options: nil)[0] as! HMCUploadingView
        uploadingView.frame = self.frame
        signUpVC.view.addSubview(uploadingView)
        
    }
    /**
      Method use to perform registeration operation
     */
    @IBAction func doneButtonAction(sender: AnyObject) {
        
       if !HMCValidation.validateSignUp(){
            return
        }
        
        self.setUploadingImageView()
        keyNameArr.removeAllObjects()
        self.count = 0
        urlCount = 0
        let imgArr = NSMutableArray()
        imgArr.addObject(signUpModel.identityProof)
        imgArr.addObject(signUpModel.addressProof)
        imgArr.addObject(signUpModel.bankProof)
        imgArr.addObject(signUpModel.profilePicture)
        for image in imgArr{
            let fileName: String = NSProcessInfo.processInfo().globallyUniqueString.stringByAppendingString(".jpg")
            let imgData = getCompressedImageDataFromImage(image as! UIImage, width: 800, height: 600)
            callAWSServiceToupload(imgData, withFileName: fileName)
        }
        
        
        
    }
    
    /**
      Calling Service to Upload images on AWS(Amazon Web Services)
     */
    func callAWSServiceToupload(imageData: NSData, withFileName fileName: String)
    {
        let credentialProvider = AWSCognitoCredentialsProvider(regionType: CognitoRegionType, identityPoolId: CognitoIdentityPoolId)
        let configuration = AWSServiceConfiguration(region: DefaultServiceRegionType,credentialsProvider: credentialProvider)
        AWSServiceManager.defaultServiceManager().defaultServiceConfiguration = configuration
        keyNameArr.addObject(fileName)
        let awsUrl =  "https://s3.amazonaws.com/\(S3BucketName)/\(fileName)"
        urlCount = urlCount + 1
        saveAWS3URL(awsUrl)
        print(awsUrl)
        
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.uploadProgress = {(task: AWSS3TransferUtilityTask, bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) in
            dispatch_async(dispatch_get_main_queue(), {
                
                NSLog("byte sent... %lld",bytesSent);
                NSLog("total byte sent... %lld",totalBytesSent);
                NSLog("total byte expected to sent... %lld",totalBytesExpectedToSend);
            })
        }
        
        self.completionHandler = { (task, error) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                if ((error) != nil){
                    NSLog("Failed with error")
                    NSLog("Error: %@",error!);
                    HMCUtlities.hideLoader()
                    self.uploadingView.removeFromSuperview()
                }
                    
                else{
                    
                    print("Uploaded successfully")
                    
                    print("rajeshwar...\(task)" )
                    print(self.keyNameArr)
                    
                    for i in 0  ..< self.keyNameArr.count {
                        
                        let name = self.keyNameArr.objectAtIndex(i) as! String
                        if name == task.key {
                            
                          let dic =  self.signUpModel.imgArry.objectAtIndex(i) as! NSMutableDictionary
                            dic.setObject(true, forKey: "checkDone")
                            self.uploadingView.reloadData()
                            break
                            
                            
                        }
                    }
                    
                    self.count = self.count+1
                    if self.count == 4{
                        self.registerUser()
                        self.count = 0;
                    }
                }
            })
        }
        
        let transferUtility = AWSS3TransferUtility.defaultS3TransferUtility()
        
        
        transferUtility.uploadData(imageData, bucket: S3BucketName, key: fileName, contentType: "image/jpg", expression: expression, completionHander: completionHandler).continueWithBlock{ (task) -> AnyObject! in
            
            
            if let _ = task.error {
                print("aws raj 1")
                HMCUtlities.hideLoader()
                //HMCUtlities.showErrorMessage(error.localizedDescription)
                
            }
            if let exception = task.exception {
                print("aws raj 2")
                NSLog("Exception: %@",exception.description);
                
            }
            if let _ = task.result {
                
                NSLog("Upload Starting!")
                // Do something with uploadTask.
            }
            
            return nil;
        }
        
        
    }
    
    func saveAWS3URL(url:String)
    {
        switch(urlCount)
        {
        case 1 :
            signUpModel.identityProofUrl = url
            
        case 2 :
            signUpModel.addressProofUrl = url
            
        case 3 :
            signUpModel.bankProofUrl = url
            
        case 4 :
            signUpModel.profilePictureUrl = url
            
            
        default :
            print("")
            
        }
    }
    /**
     Method use to register user
     */
    func registerUser()
    {
        uploadingView.removeFromSuperview()
        HMCUtlities.showLoader()
        let cuisineNameObjArry = NSMutableArray()
        for obj in signUpModel.cuisines{
            let cuisine = obj as! HMCCuisinesModel
            cuisineNameObjArry.addObject(cuisine.cuisine)
            
        }
        let parameters = [
            "addressProofDocs":[["url":signUpModel.addressProofUrl,"status":"NEW"]],
            
            "bankProofDocs":[["url":signUpModel.bankProofUrl,"status":"NEW"]],
            "identityProofDocs":[["url":signUpModel.identityProofUrl,"status":"NEW"]],
            "profilePicUrl":signUpModel.profilePictureUrl,
            "cookingDetails":[
                "cuisines":cuisineNameObjArry,
                "nonVeg":signUpModel.nonVeg,
                "numberOfPlates":signUpModel.numberOfPlate,
                "veg":signUpModel.veg,
                
            ],
            
            "mobileNumber":HMCUtlities.getUserContactNumber()
            
            ] as Dictionary
        
        print(parameters)
        let url : String = HMCServiceUrls.getCompleteUrlFor(Url_Register)
        
        HMCAppServices.postServiceRequest(urlString:url, parameter: parameters  , successBlock: { (responseData) -> () in
            
            print("register raj 1")
            
            let response : NSDictionary = responseData as! NSDictionary
            HMCUtlities.hideLoader()
            print(response)
            
            let chefStatus = response["chefStatus"] as? String
            
            if chefStatus == ChefStatus.REGISTRATION_UNDER_REVIEW.description {
                self.showAlertView()
                
            }
            else{
                HMCUtlities.showErrorMessage(response["errorMessage"] as! String)
            }
            
            
            
            }) { (errorMessage) -> () in
                print("register raj 1")
                HMCUtlities.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                
        }
        
        
        }
    func showAlertView()
    {
         let signUpVC = HMCUtlities.getTopViewController()
        
            
            let alert=UIAlertController(title: "Notification", message: "Thanks for taking the time out to register with Homli. We will shortly get back to you.", preferredStyle: UIAlertControllerStyle.Alert);
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                HMCUtlities.sharedInstance.navigateToHomeViewController()
                self.signUpModel.setInitialValue()
                HMCUtlities.setUserCurrentStatus(ChefStatus.REGISTRATION_UNDER_REVIEW.description)
                
            }))
        
            signUpVC.presentViewController(alert, animated: true, completion: nil);
            
        
    }

    
    func getCompressedImageDataFromImage(image: UIImage, width: CGFloat, height: CGFloat) -> NSData {
        let originalSize: CGSize = image.size
        var data: NSData
        if originalSize.width > 800 && originalSize.height > 600 {
            let newSize: CGSize = CGSizeMake(width, height)
            UIGraphicsBeginImageContext(newSize)
            image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
            let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            data = UIImageJPEGRepresentation(newImage, 0.8)!
            return data
        }
        data = UIImageJPEGRepresentation(image, 1.0)!
        return data
    }
    
}
