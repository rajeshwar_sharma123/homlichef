//
//  HMCSearchDishesView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 04/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
protocol HMCSearchDishesViewDelegate{

    func getSelectedDishes(dishDic:NSDictionary)
    
}
class HMCSearchDishesView: UIView,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var middleView: UIView!
    @IBOutlet var txtFldSearchDishes: UITextField!
    @IBOutlet var ContentView: UIView!
    @IBOutlet var tableView: UITableView!
    var delegate : HMCSearchDishesViewDelegate! = nil
    var dishesArray = NSArray()
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        tableView.tableFooterView = UIView()
        ContentView.layer.cornerRadius = 5
        ContentView.clipsToBounds = true
        self.transparentBackground(withAlphaValue: 0.5)
        txtFldSearchDishes.layer.borderWidth = 1
        txtFldSearchDishes.layer.borderColor = ColorFromHexaCode(k_Theme_Color).CGColor
        txtFldSearchDishes.layer.cornerRadius = txtFldSearchDishes.frame.size.height/2
        txtFldSearchDishes.clipsToBounds = true
        txtFldSearchDishes.leftPadding(25)
        
        self.tableView.registerNib(UINib(nibName: "HMCSearchDishesCell", bundle: nil), forCellReuseIdentifier: "HMCSearchDishesCell")
        
        
    }
    
    // MARK:- UITableViewDelegate & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dishesArray.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let dic = dishesArray[indexPath.row] as! NSDictionary
        
        let cell: HMCSearchDishesCell! = tableView.dequeueReusableCellWithIdentifier("HMCSearchDishesCell") as? HMCSearchDishesCell
        
        cell.lblDishName.text = dic["dishName"] as? String
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  55
        
        
    }
     func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       
        self.delegate.getSelectedDishes(self.dishesArray.objectAtIndex(indexPath.row) as! NSDictionary)
        self.removeFromSuperview()
    }

    
    // MARK:- UITextFieldDelegate Method
    func textField(textField: UITextField, shouldChangeCharactersInRange range:NSRange, replacementString string: String) -> Bool {
        var dishName:NSString = textField.text! as NSString
        dishName = dishName.stringByReplacingCharactersInRange(range, withString: string)
       getDishesListFromServer(dishName as String)
        return true
        
        
        
    }
    /**
    Calling Api to get Dishes List
     */
    func getDishesListFromServer(dishName:String)
    {

        let parameters = [
            "dishName": dishName,
            "dishCategory" : "MAIN_COURSE"
            
        ]
      
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Find_Dish), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            
            self.dishesArray = (response["dishes"] as? NSArray)!
            
            self.tableView .reloadData()
            
            
            
            }) { (errorMessage) -> () in
                
                
        }

    
    }
    
    
    @IBAction func cancelTapGestureAction(sender: AnyObject) {
        //self.delegate.getSelectedDishes( ["dishName":"rajeshwar"])
        self.removeFromSuperview()
    }
}
