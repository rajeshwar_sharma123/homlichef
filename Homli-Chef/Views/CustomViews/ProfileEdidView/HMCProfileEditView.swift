//
//  HMCProfileEditView.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/11/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
protocol HMCProfileEditViewDelegate{
    func cookingDetailsUpdated();
    
}
class HMCProfileEditView:UIView,UITextFieldDelegate
{
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var nonvegbtn: UIButton!
    @IBOutlet weak var vegbtn: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet var chooseTextField: HMCTextField!
    var chooseCuisinesView : HMCChooseCuisinesView!
    var delegate : HMCProfileEditViewDelegate! = nil
    let signUpModel = HMCSignUpModel.sharedInstance()
   
    /**
    Method use to initialize default value
     */
    func initWithDefaultValue()
    {
        contentView.layer.cornerRadius = 4
        contentView.clipsToBounds = true
        if signUpModel.cuisinesArry.count == 0{
            getCuisinesListFromServer()
        }else {
            self.updateCuisineArray()
        }
        let userInfo = HMCUtlities.getSignInUserDetail()
        var  completeDetail = NSDictionary()
        if (userInfo["cookingDetails"] != nil)
        {
            completeDetail = userInfo["cookingDetails"] as! NSDictionary
            signUpModel.veg = completeDetail["veg"] as! Bool
            signUpModel.nonVeg = completeDetail["nonVeg"] as! Bool
            
             
            if signUpModel.veg as Bool{
                vegbtn.tag = 0
                btnVegAction(vegbtn)
            }

            if signUpModel.nonVeg as Bool{
                nonvegbtn.tag = 0
                btnNonVegAction(nonvegbtn)
            }

        }

    
    }
    func updateCuisineArray()
    {
        let userInfo = HMCUtlities.getSignInUserDetail()
        var  completeDetail = NSDictionary()
        if (userInfo["cookingDetails"] != nil)
        {
            
            
            completeDetail = userInfo["cookingDetails"] as! NSDictionary
            let arry = completeDetail["cuisines"] as! NSArray
            for model in signUpModel.cuisinesArry{
                
                let tmpModel = model as HMCCuisinesModel
                
                if arry.containsObject(tmpModel.cuisine){
                    signUpModel.cuisines.addObject(tmpModel)
                }
            }
            
        }
    }
    
    /**
     Method to get Cuisine List
     */
    func getCuisinesListFromServer()
    {
        
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Cuisines_List)
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            
            print(responseData)
            
            let dic : NSDictionary = responseData as! NSDictionary
            self.signUpModel.cuisinesArry = HMCCuisinesModel.bindDataWithModel(dic["cuisines"] as! NSArray)
            
            self.updateCuisineArray()
           
         }) { (errorMessage) -> () in
                HMCUtlities.showNavigationBar()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.removeFromSuperview()
                
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
   
        self.transparentBackground(withAlphaValue: 0.5)
        chooseTextField.addButtonOnRightWithImageName("ic_arrow_down")
        
            }
    @IBAction func btnVegAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        let signUpModel = HMCSignUpModel.sharedInstance()
        if(btn.tag==0)
        {
            
          signUpModel.veg = true
            vegbtn.tag = 1;
            vegbtn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            signUpModel.veg = false
            vegbtn.tag = 0
            vegbtn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    
    
    
    @IBAction func btnNonVegAction(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        let signUpModel = HMCSignUpModel.sharedInstance()
        if(btn.tag==0)
        {
            signUpModel.nonVeg = true
            nonvegbtn.tag = 1;
            nonvegbtn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            signUpModel.nonVeg = false
            nonvegbtn.tag = 0
            nonvegbtn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    
    @IBAction func btnCancelAction(sender: AnyObject) {
        HMCUtlities.showNavigationBar()
        self.signUpModel.setInitialValue()
        self.removeFromSuperview()
    }
    
    @IBAction func btnUpdateAction(sender: AnyObject) {

        if !HMCValidation.validateUpdateCookingDetails(){
            return
        }
        let userInfo = HMCUtlities.getSignInUserDetail()
        var  completeDetail = NSDictionary()
        if (userInfo["cookingDetails"] != nil)
        {
          completeDetail = userInfo["cookingDetails"] as! NSDictionary
        }
        let cuisineNameObjArry = NSMutableArray()
        for obj in signUpModel.cuisines{
            let cuisine = obj as! HMCCuisinesModel
            cuisineNameObjArry.addObject(cuisine.cuisine)
            
        }
        var numberOfPlates = ""
        if completeDetail["numberOfPlates"] != nil{
          numberOfPlates =  String( completeDetail["numberOfPlates"] as! Int )
        }
        let parameters = [
            "chefMobileNumber" : HMCUtlities.getUserContactNumber(),
            "cookingDetails" : [
                "cuisines" : cuisineNameObjArry,
                "nonVeg" : signUpModel.nonVeg,
                "veg" : signUpModel.veg,
                "numberOfPlates" : numberOfPlates
            ]
    ]
        HMCUtlities.showLoader()
        
        
        print(parameters)
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Update_Cooking_Details), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            HMCUtlities.hideLoader()
            print(response)
            self.removeFromSuperview()
            
            
            let userDetail : NSMutableDictionary = userInfo.mutableCopy() as! NSMutableDictionary
            print(userDetail)
            var  completeDetail = NSMutableDictionary()
            if (userInfo["cookingDetails"] != nil)
            {
                let dic = userInfo["cookingDetails"] as! NSDictionary
                completeDetail = dic.mutableCopy() as! NSMutableDictionary
                
                completeDetail.setObject(cuisineNameObjArry, forKey: "cuisines")
                completeDetail.setObject(self.signUpModel.nonVeg, forKey: "nonVeg")
                completeDetail.setObject(self.signUpModel.veg, forKey: "veg")
                userDetail.setObject(completeDetail, forKey: "cookingDetails")
                HMCUtlities.saveUserProfile(userDetail)
                HMCUtlities.showSuccessMessage(Update_Cooking_Details_Successful)
                
                
            }
            self.delegate.cookingDetailsUpdated()
            self.signUpModel.setInitialValue()
            
            }) { (errorMessage) -> () in
                
                HMCUtlities.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                
                
        }
        

    }
    
    // MARK:- UITextFieldDelegate Method
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        if textField.isEqual(chooseTextField)
        {
            if textField.isAskingCanBecomeFirstResponder == false{
                
                setCussineView()
                textField.resignFirstResponder()
                
            }
            
            
            return false
            
        }
        return true;
    }
    func setCussineView()
    {
                
        chooseCuisinesView =  NSBundle.mainBundle().loadNibNamed("HMCChooseCuisinesView", owner: self, options: nil)[0] as! HMCChooseCuisinesView
        chooseCuisinesView.frame = self.frame
        self.addSubview(chooseCuisinesView)
        

        
    }
    
    
}

