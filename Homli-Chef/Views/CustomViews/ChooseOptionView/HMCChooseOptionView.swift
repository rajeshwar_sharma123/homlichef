//
//  HMCChooseOptionView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 25/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
protocol HMCChooseOptionViewDelegate{
    func setSelectedImage(image:UIImage,atIndex index:NSInteger)
}
class HMCChooseOptionView: UIView,UIImagePickerControllerDelegate,UINavigationControllerDelegate ,ImageCropViewControllerDelegate{
    var delegate:HMCChooseOptionViewDelegate! = nil
    var imagePicker : UIImagePickerController!
    var signUpVC : HMCSignUpViewController!
    var index : NSInteger!
    @IBOutlet var contentView: UIView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.transparentBackground(withAlphaValue: 0.3)
        contentView.layer.cornerRadius = 5
        contentView.clipsToBounds = true
        signUpVC =  self.delegate as! HMCSignUpViewController
   
    }
    
    @IBAction func camerraViewTapGestureAction(sender: AnyObject) {
       
        if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
         imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        imagePicker.cameraCaptureMode = .Photo
        imagePicker.modalPresentationStyle = .FullScreen
        signUpVC.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
        noCamera()
        }
        
    }
    @IBAction func galleryViewTapGestureAction(sender: AnyObject) {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        signUpVC.presentViewController(imagePicker, animated: true, completion: nil)
        
    }
    @IBAction func cancelViewTapGestureAction(sender: AnyObject) {
        self.removeFromSuperview()
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String :
        AnyObject]) {
          
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let cropVC = ImageCropViewController(image: pickedImage)
            cropVC.delegate = self
            signUpVC.navigationController?.pushViewController(cropVC, animated: false)
            
        }
        
        signUpVC.dismissViewControllerAnimated(true, completion: nil)
    }
    
      func imagePickerControllerDidCancel(picker: UIImagePickerController)
    {
        self.removeFromSuperview()
        signUpVC.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera",message: "Sorry, this device has no camera",
            preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK",style:.Default,handler: nil)
           alertVC.addAction(okAction)
        signUpVC.presentViewController(alertVC,animated: true,completion: nil)
    }
    
    func ImageCropViewControllerSuccess(controller: UIViewController!, didFinishCroppingImage croppedImage: UIImage!) {
        
        self.delegate.setSelectedImage(croppedImage, atIndex: self.index)
        signUpVC.navigationController?.popViewControllerAnimated(true)
        self.removeFromSuperview()
    }
    
    func ImageCropViewControllerDidCancel(controller: UIViewController!) {
        self.removeFromSuperview()
         signUpVC.navigationController?.popViewControllerAnimated(true)
    }

}
