//
//  HMCChooseCuisinesView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 29/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCChooseCuisinesView: UIView,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    @IBOutlet var contentView: UIView!
    //var cuisinesArry = [HMCCuisinesModel]()
    let signUpModel = HMCSignUpModel.sharedInstance()
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    // MARK:- TableView Delegate Method
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        self.transparentBackground(withAlphaValue: 0.5)
        contentView.layer.cornerRadius = 4
        contentView.clipsToBounds = true
        
        self.tableView.registerNib(UINib(nibName: "HMCChooseCuisinesCell", bundle: nil), forCellReuseIdentifier: "HMCChooseCuisinesCell")
        if signUpModel.cuisinesArry.count == 0{
            getCuisinesListFromServer()
        }
        else
        {
           tableView.reloadData()
        }
        
    }
    
    /**
     Calling Api to get CuisineList
     */
    func getCuisinesListFromServer()
    {

        
        let vc = HMCUtlities.getTopViewController()
        vc.view.showLoader()
        
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Cuisines_List)
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            
            print(responseData)
            
            let dic : NSDictionary = responseData as! NSDictionary
            self.signUpModel.cuisinesArry = HMCCuisinesModel.bindDataWithModel(dic["cuisines"] as! NSArray)
            self.tableView.reloadData()
            
            vc.view.hideLoader()
            
            }) { (errorMessage) -> () in
                
                vc.view.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.removeFromSuperview()
                
        }
    }
    
   //MARK:- TableView Delegates & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return signUpModel.cuisinesArry.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cuisineObj = signUpModel.cuisinesArry[indexPath.row] as HMCCuisinesModel
        let cell: HMCChooseCuisinesCell! = tableView.dequeueReusableCellWithIdentifier("HMCChooseCuisinesCell") as? HMCChooseCuisinesCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.configureCell(cuisineObj)
        return cell
        
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return  60
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell : HMCChooseCuisinesCell = tableView.cellForRowAtIndexPath(indexPath) as! HMCChooseCuisinesCell
        let cuisineObj = signUpModel.cuisinesArry[indexPath.row] as HMCCuisinesModel
        
        if signUpModel.cuisines.containsObject(cuisineObj){
        signUpModel.cuisines.removeObject(cuisineObj)
        cell.imgViewCheckBox.image = UIImage(named: "icn_checkbox_unselected")
        }
        else
        {
            cell.imgViewCheckBox.image = UIImage(named: "icn_checkbox_selected")
            signUpModel.cuisines.addObject(cuisineObj)
        }
        
    }
    // MARK:- Custom Method
    @IBAction func bottomViewTapGesture(sender: AnyObject) {
        self.removeFromSuperview()
    }
}
