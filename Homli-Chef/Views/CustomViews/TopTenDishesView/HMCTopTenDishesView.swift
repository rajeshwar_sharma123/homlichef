//
//  HMCTopTenDishesView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 11/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
protocol HMCTopTenDishesViewDelegate{
    func topTenDishesUpdated();

}
class HMCTopTenDishesView: UIView ,HMCSearchDishesViewDelegate,ANTagsViewProtocol{
    @IBOutlet var contentView: UIView!
    var dishesTag : ANTagsView!
    var dishNameArr = NSMutableArray()
    var dishesArr = NSMutableArray()
    @IBOutlet var lblDishesNumber: UILabel!
    @IBOutlet var topTenDishesView: UIView!
    @IBOutlet var addDishesBtn: UIButton!
    var delegate : HMCTopTenDishesViewDelegate! = nil
    @IBOutlet var topDishesView: UIView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        super.layoutSubviews()
        self.transparentBackground(withAlphaValue: 0.5)
    }
    
    func initWithDefaultValue()
    {
        contentView.layer.cornerRadius = 4
        contentView.clipsToBounds = true
        topDishesView.layer.cornerRadius = topDishesView.frame.size.height/2
        topDishesView.clipsToBounds = true
        addDishesBtn.layer.cornerRadius = addDishesBtn.frame.size.width/2
        addDishesBtn.clipsToBounds = true
        let tmpArr = HMCUtlities.getUserPreferredDishes()
        let arr = tmpArr.valueForKey("dishName") as! NSArray
        self.dishNameArr = arr.mutableCopy() as! NSMutableArray
        for dishDic in tmpArr{
            dishesArr.addObject(dishDic)
        }
        
        setDishesTagView()
    }
    /**
     Method to set Custom Dishes Tag View
     */
    func setDishesTagView()
    {
        lblDishesNumber.text = "Top Dishes(\(String(self.dishNameArr.count)))"
                if dishNameArr.count == 0{
                    topDishesView.hidden = false
                    if (self.dishesTag != nil){
                      self.dishesTag.removeFromSuperview()
                    }
        
                    return
                }
        topDishesView.hidden = true
        if (self.dishesTag != nil){
            
            self.dishesTag.removeFromSuperview()
        }
        
        self.dishesTag = ANTagsView(tags: self.dishNameArr  as [AnyObject], frame: CGRectMake(10, 35, topTenDishesView.frame.size.width-20, 10))
        self.dishesTag.setTagCornerRadius(10)
        self.dishesTag.tag = 1
        self.dishesTag.delegate = self
        self.dishesTag.setTagBackgroundColor(UIColor.clearColor())
        
        self.dishesTag.setTagTextColor(UIColor.blackColor())
        self.dishesTag.setTagTextFont(15)
        self.dishesTag.backgroundColor = ColorFromHexaCode(0xECECEC)
        
        self.dishesTag.setFrameWidth(Int32(screenWidth-40))
        self.dishesTag.layer.cornerRadius = 15
        self.dishesTag.clipsToBounds = true
        self.topTenDishesView.addSubview(self.dishesTag)
        self.dishesTag.setTagBackgroundShadowEffect()
    }

    @IBAction func addDishesButtonAction(sender: AnyObject) {
        if (dishNameArr.count == 10)
        {
            HMCUtlities.showErrorMessage(Exceeded_top_dishes)
            return
            
        }
        
        let searchDishesView =  NSBundle.mainBundle().loadNibNamed("HMCSearchDishesView", owner: self, options: nil)[0] as! HMCSearchDishesView
        searchDishesView.delegate = self
        
        searchDishesView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        
        self.addSubview(searchDishesView)
    }

    @IBAction func cancelButtonAction(sender: AnyObject) {
        HMCUtlities.showNavigationBar()
        self.removeFromSuperview()
    }
    @IBAction func updateButtonAction(sender: AnyObject) {
        
        
        if dishesArr.count == 0{
            HMCUtlities.showErrorMessage(Add_top_dishes)
            return
        }
        HMCUtlities.showNavigationBar()
        let preferredDishes = NSMutableArray()
        for dic in dishesArr{
            
            
            let dishDic = NSMutableDictionary()
            dishDic.setObject(dic["dishCode"], forKey: "dishCode")
            dishDic.setObject(dic["dishName"], forKey: "dishName")
            preferredDishes.addObject(dishDic)
            
        }
        
        let parameters = [
            "chefMobileNumber" : HMCUtlities.getUserContactNumber(),
            "preferredDishes" : preferredDishes,
         
            
        ]
        HMCUtlities.showLoader()
        
        
        print(parameters)
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Update_Top_Dishes), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            HMCUtlities.hideLoader()
            print(response)
            
            let userInfo = HMCUtlities.getSignInUserDetail()
           
            let userDetail : NSMutableDictionary = userInfo.mutableCopy() as! NSMutableDictionary
            print(userDetail)
            var  chefQnADic = NSMutableDictionary()
            
            if (userInfo["chefQnA"] != nil)
            {
                let dic = userInfo["chefQnA"] as! NSDictionary
                chefQnADic = dic.mutableCopy() as! NSMutableDictionary
                chefQnADic.setObject(preferredDishes, forKey: "preferredDishes")
                userDetail.setObject(chefQnADic, forKey: "chefQnA")
                HMCUtlities.saveUserProfile(userDetail)
                HMCUtlities.showSuccessMessage(Top_Dishes_Updated_Successfully)
                
            }

            
            
            self.removeFromSuperview()
            self.delegate.topTenDishesUpdated()
                        
            
            }) { (errorMessage) -> () in
                
                HMCUtlities.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                
                
        }
        

        
    }
    
    func getSelectedDishes(dishDic: NSDictionary) {
        for i in 0 ..< dishNameArr.count{
            
            let dishName = dishNameArr.objectAtIndex(i) as! String
            if (dishName == dishDic["dishName"] as! String)
            {
                HMCUtlities.showErrorMessage("You have already added \"\(dishName)\" in your top dishes")
                return
                
            }
        }
        dishesArr.addObject(dishDic)
        dishNameArr.addObject(dishDic["dishName"] as! String)
        self.setDishesTagView()
        
    }
    //MARK:- ANTagsViewProtocol Method
    
    func clickedANTagsView(antObj: ANTagsView!, tagIndex index: Int32) {
        print(index)
        
        
        showDishesAlertView(self.dishNameArr.objectAtIndex(Int(index)) as! String, atIndex: Int (index))
    }
    
    func showDishesAlertView(name:String, atIndex index:Int)
    {
        
        let alert=UIAlertController(title: "", message: "Do you want to delete \"\(name)\" from your Top 10 Dishes", preferredStyle: UIAlertControllerStyle.Alert);
        
        //event handler with closure
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            self.dishNameArr.removeObjectAtIndex(index)
            self.dishesArr.removeObjectAtIndex(index)
            self.setDishesTagView()
           
            
            
        }))
        //no event handler (just close dialog box)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            
            
            
        }));
        let vc =   HMCUtlities.getTopViewController()
        vc.presentViewController(alert, animated: true, completion: nil);
        
    }


}
