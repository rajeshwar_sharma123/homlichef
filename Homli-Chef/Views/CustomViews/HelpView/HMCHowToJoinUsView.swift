//
//  HMCHowToJoinUsViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/5/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCHowToJoinUsView: UIView{

    
    @IBOutlet weak var gotIt: UIButton!

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func BackToSignUp(sender: AnyObject) {
        self.removeFromSuperview()
    }
}
