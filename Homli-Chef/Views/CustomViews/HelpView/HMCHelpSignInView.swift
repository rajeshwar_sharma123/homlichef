//
//  HMCHelpViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 3/29/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCHelpSignInView: UIView {

    @IBOutlet weak var helpLabel: UILabel!
    @IBAction func backToSignIn(sender: AnyObject) {
      
        removeFromSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        helpLabel.text = "To Sign in, Enter your Mobile Number and press Sign In Button"
        helpLabel.sizeToFit()
       
       
        
    }
  
    
}
