//
//  HMCNoDataView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 27/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCNoDataView: UIView {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    func initWithDefaultValue(title:String,description:String){
        lblTitle.text = title
        lblDescription.text = description
        
    }
    
   class func initNoDataView(parentView:UIView)->HMCNoDataView{
        let noDataView =  NSBundle.mainBundle().loadNibNamed("HMCNoDataView", owner: self, options: nil)[0] as! HMCNoDataView
        noDataView.frame = parentView.bounds
        return noDataView
    }
}
