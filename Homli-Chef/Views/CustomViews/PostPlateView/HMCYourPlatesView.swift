//
//  HMCYourPlatesView.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/20/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
protocol HMCYourPlatesViewDelegate {
    func pressedUpdatebutton(message: String)
    
}
class HMCYourPlatesView: UIView {
    
    var yourPlateModel:HMCYourPlateModel!
    var delegate: HMCYourPlatesViewDelegate?
    @IBOutlet weak var titleofplates: UILabel!
    var numberOfPlate : String!
    @IBOutlet weak var noOfPlates: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }
    
    func initWithDefaultValue(yourPlate:HMCYourPlateModel){
        numberOfPlate = yourPlate.noOfPlates
        yourPlateModel = yourPlate
        titleofplates.text = yourPlate.plateName
        noOfPlates.text = yourPlate.noOfPlates
    }
    
    
    @IBAction func pressedDecrease(sender: AnyObject) {
        let a:Int? = Int(noOfPlates.text!)
        if(a! > 1 )
        {
            noOfPlates.text = (String)(a! - 1)
            yourPlateModel.noOfPlates = noOfPlates.text
            
        }
    }
    
    @IBAction func pressedIncreased(sender: AnyObject) {
        let a:Int? = Int(noOfPlates.text!)
        if(a! < 10)
        {
            noOfPlates.text = (String)(a! + 1)
            yourPlateModel.noOfPlates = noOfPlates.text
        }
    }
    
    
    @IBAction func pressedcancelbutton(sender: AnyObject) {
        self.removeFromSuperview()
        yourPlateModel.noOfPlates = numberOfPlate
    }
    
    @IBAction func pressedUpdatebutton(sender: AnyObject) {
        self.removeFromSuperview()
        updateNumberOfPlate()
        
        
    }
    /**
     Method use to update number of plates
     */
    func updateNumberOfPlate()
    {
        HMCUtlities.showLoader()
        let parameters = [
            "chefMobileNumber": HMCUtlities.getUserContactNumber(),
            "plateCode" : yourPlateModel.plateCode!,
            "numberOfPlates" : yourPlateModel.noOfPlates!
        ]
        print(parameters)
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Update_Plate_Num), parameter: parameters, successBlock: { (responseData) -> () in
            HMCUtlities.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            if response["msg"] != nil{
                HMCUtlities.showSuccessMessage(response["msg"] as! String)
                self.delegate?.pressedUpdatebutton(self.noOfPlates.text!)
            }
            else
            {
                self.yourPlateModel.noOfPlates = self.numberOfPlate
                HMCUtlities.showErrorMessage(response["errorMessage"] as! String)
            }
        }) { (errorMessage) -> () in
            HMCUtlities.hideLoader()
            self.yourPlateModel.noOfPlates = self.numberOfPlate
        }
        
        
    }
    
    
}
