//
//  HMCConfirmCancelPlate.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/22/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation

protocol HMCConfirmCancelPlateDelegate{
    func confirmCancelPlate(check:Bool)
}
class HMCConfirmCancelPlate: UIView {
    
    @IBOutlet var contentView: UIView!
    var delegate: HMCConfirmCancelPlateDelegate?
    
    @IBOutlet weak var message: UILabel!
    
    @IBAction func okButtonClickedAction(sender: AnyObject) {
        delegate?.confirmCancelPlate(true)
        self.removeFromSuperview()
    }
    
    @IBAction func noAction(sender: AnyObject) {
        self.removeFromSuperview()
        delegate?.confirmCancelPlate(false)
    }
    override func layoutSubviews() {
        contentView.layer.cornerRadius = 5
        contentView.clipsToBounds = true
        self.transparentBackground(withAlphaValue: 0.5)
        super.layoutSubviews()
        
    }
}
