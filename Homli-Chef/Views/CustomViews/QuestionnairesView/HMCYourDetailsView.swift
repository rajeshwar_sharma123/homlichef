//
//  HMCYourDetailsView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 31/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import SDWebImage

class HMCYourDetailsView: UIView,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    var dataArray = NSMutableArray()
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    
    func initWithDefaultValue()
    {
        self.tableView.registerNib(UINib(nibName: "HMCYourdetailCell", bundle: nil), forCellReuseIdentifier: "HMCYourdetailCell")
        self.tableView.registerNib(UINib(nibName: "HMCYourPreference", bundle: nil), forCellReuseIdentifier: "HMCYourPreference")

        getUserCurrentDocumentProof()
    }
    
    override func layoutSubviews() {
        
        
    }
    
    /**
     Calling Api to get User DocumentProof
     */
    func getUserCurrentDocumentProof()
    {
        HMCUtlities.showLoader()
        
        let parameters = [
            "mobileNumber": HMCUtlities.getUserContactNumber()
                      
        ]
        
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Get_Doument_Proof), parameter: parameters, successBlock: { (responseData) -> () in
            
            HMCUtlities.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            var tmpArr = response["identityProofUrl"] as! NSArray
            var tmpDic = tmpArr.firstObject as! NSDictionary
            self.dataArray.addObject(tmpDic["url"] as! String)
            
            tmpArr = response["addressProofUrl"] as! NSArray
            tmpDic = tmpArr.firstObject as! NSDictionary
            self.dataArray.addObject(tmpDic["url"] as! String)
            
            tmpArr = response["bankProofUrl"] as! NSArray
            tmpDic = tmpArr.firstObject as! NSDictionary
            self.dataArray.addObject(tmpDic["url"] as! String)
            
            self.tableView .reloadData()
            
            
            
            }) { (errorMessage) -> () in
                
                 HMCUtlities.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
        
        
    }

    
    // MARK:- UITableViewDelegate & DataSource Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return dataArray.count
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.section==3{
            let cell: HMCYourPreference! = tableView.dequeueReusableCellWithIdentifier("HMCYourPreference") as? HMCYourPreference
            return cell
        }
        
        let cell: HMCYourdetailCell! = tableView.dequeueReusableCellWithIdentifier("HMCYourdetailCell") as? HMCYourdetailCell
        let url = NSURL(string: dataArray.objectAtIndex(indexPath.section) as! String)
        cell.proofImgView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "icn_default"))
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.section == 3
        {
        return  130 
        }
        return  200
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView(frame: CGRectMake(0, 0, screenWidth, 35))
        headerView.backgroundColor = UIColor.whiteColor()
        
        let subView = UIView(frame: CGRectMake(10, 0, screenWidth-20, 35))
        subView.backgroundColor = ColorFromHexaCode(0xC3A465)
        
        let lblHeaderTitle = UILabel(frame: CGRectMake(10, 5, subView.frame.size.width-20, 25))
        lblHeaderTitle.backgroundColor = UIColor.clearColor()
        lblHeaderTitle.textColor = UIColor.whiteColor()
        lblHeaderTitle.font = UIFont(name: "OpenSans", size: 17)
        
        subView.addSubview(lblHeaderTitle)
        switch(section)
        {
        case 0 :
            lblHeaderTitle.text = "Your Identity Proof"
            
        case 1 :
            lblHeaderTitle.text = "Your Address Proof"
            
        case 2 :
            lblHeaderTitle.text = "Your Bank Proof"
            
        case 3 :
            lblHeaderTitle.text = "Your Preference"
            
            
        default :
            print("")
            
        }

        
        headerView.addSubview(subView)
        
        return headerView
        
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    
}
