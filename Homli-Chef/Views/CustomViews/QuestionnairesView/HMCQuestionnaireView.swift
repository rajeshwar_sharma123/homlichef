//
//  HMCQuestionnaireView.swift
//  Homli-Chef
//
//  Created by daffolapmac on 31/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import RangeSliderView
class HMCQuestionnaireView: UIView,ANTagsViewProtocol,HMCSearchDishesViewDelegate {
    var dishesTag : ANTagsView!
    @IBOutlet var dishesTagView: UIView!
    @IBOutlet var lblDishCount: UILabel!
    @IBOutlet var sliderView: UIView!
    @IBOutlet var dishesTagViewHeightConstraint: NSLayoutConstraint!
    var dishNameArr = NSMutableArray()
    var dishesArr = NSMutableArray()
    
    @IBOutlet var topTenDishesView: UIView!
    @IBOutlet var addDishesBtn: UIButton!
    @IBOutlet var lowerView: UIView!
    @IBOutlet var uperView: UIView!
    @IBOutlet var lowerLabel: UILabel!
    @IBOutlet var uperLabel: UILabel!
    @IBOutlet var rangeSlider: NMRangeSlider!
    @IBOutlet var lblPlateBetween: UILabel!
    
    
    // var markerRangeView : MARKRangeSlider!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
    // Drawing code
    }
    */
    func initWithDefaultValue()
    {
        addDishesBtn.layer.cornerRadius = addDishesBtn.frame.size.width/2
        addDishesBtn.clipsToBounds = true
        setDishesTagView()
        topTenDishesView.layer.cornerRadius = topTenDishesView.frame.size.height/2
        topTenDishesView.clipsToBounds = true
        self.tintColor = ColorFromHexaCode(0xEAC879)
        self.rangeSlider.minimumValue = 1;
        self.rangeSlider.maximumValue = 7;
        self.rangeSlider.lowerValue = 4;
        self.rangeSlider.upperValue = 5;
        self.rangeSlider.minimumRange = 0; //0.73
        self.rangeSlider.lowerHandleImageNormal = UIImage(named: "ic_circular");
        self.rangeSlider.lowerHandleImageHighlighted = UIImage(named: "ic_circular");
        self.rangeSlider.upperHandleImageNormal = UIImage(named: "ic_circular");
        self.rangeSlider.upperHandleImageHighlighted = UIImage(named: "ic_circular");
        self.rangeSlider.setLowerValue(1, upperValue: 7, animated: true)
        
        setTimerToManageRangeSliderView()
    }
    
    func setTimerToManageRangeSliderView()
    {
     NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: #selector(HMCQuestionnaireView.setSliderWithIntialValue), userInfo: nil, repeats: false)
    }
    /**
     Method use to set Dishes Tag View
     */
    func setDishesTagView()
    {
        lblDishCount.text = String(10-self.dishNameArr.count)
        if dishNameArr.count == 0{
            topTenDishesView.hidden = false
            self.dishesTagViewHeightConstraint.constant = 95
            if (self.dishesTag != nil){
                
                self.dishesTag.removeFromSuperview()
            }
            
            return
        }
        topTenDishesView.hidden = true
        if (self.dishesTag != nil){
            
            self.dishesTag.removeFromSuperview()
        }
        
        self.dishesTag = ANTagsView(tags: self.dishNameArr  as [AnyObject], frame: CGRectMake(15, 35, screenWidth - 30, 10))
        self.dishesTag.setTagCornerRadius(10)
        self.dishesTag.tag = 1
        self.dishesTag.delegate = self
        self.dishesTag.setTagBackgroundColor(UIColor.whiteColor())
        self.dishesTag.setTagTextColor(UIColor.blackColor())
        self.dishesTag.setTagTextFont(15)
        self.dishesTag.backgroundColor = ColorFromHexaCode(0xECECEC)
        self.dishesTag.setFrameWidth(Int32(screenWidth-30))
        self.dishesTag.layer.cornerRadius = 15
        self.dishesTag.clipsToBounds = true
        self.dishesTagView.addSubview(self.dishesTag)
        
        self.dishesTagViewHeightConstraint.constant = self.dishesTag.frame.size.height + self.dishesTag.frame.origin.y
        
    }
    
    func setSliderWithIntialValue() {
        // Something after a delay
        rangeSliderAction(self.rangeSlider)
    }
    
    
    
    func addSliderView()
    {
   
    }
    
    /**
     Method to submit User top ten dishes
    */
    
    @IBAction func submitButtonAction(sender: AnyObject) {
        
        if dishesArr.count != 10{
            HMCUtlities.showErrorMessage("Please add your top 10 dishes")
            return
        }
        
         let preferredDishes = NSMutableArray()
        for dic in dishesArr{
            let dishDic = NSMutableDictionary()
            dishDic.setObject(dic["dishCode"], forKey: "dishCode")
            dishDic.setObject(dic["dishName"], forKey: "dishName")
            preferredDishes.addObject(dishDic)
        
        }
       let plateDetail = lblPlateBetween.text!
        let parameters = [
          "mobileNumber" : HMCUtlities.getUserContactNumber(),
          "preferredDishes" : preferredDishes,
          "chefQnAData" : [
            [ "question" : "How many plates you can cook in a day", "answers" : [plateDetail] ]
            
            ]
        
        
        ]
        HMCUtlities.showLoader()
        
        
        print(parameters)
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Submit_QnA), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            HMCUtlities.hideLoader()
            print(response)
            HMCUtlities.setQNAStatus(true)
            
            let vc = HMCUtlities.getTopViewController()
            let welcomeScreenViewController = vc.storyboard!.instantiateViewControllerWithIdentifier("HMCWelcomeScreenViewController") as! HMCWelcomeScreenViewController
             vc.navigationController?.pushViewController(welcomeScreenViewController, animated: true)
            
            
            
            }) { (errorMessage) -> () in
                
                HMCUtlities.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                
                
        }
        
    
        
      
    }
    
    @IBAction func addDishesButtonClicked(sender: AnyObject) {
        
        if (dishNameArr.count == 10)
        {
            HMCUtlities.showErrorMessage("You have already added 10 dishes")
            return
            
        }
        
        let searchDishesView =  NSBundle.mainBundle().loadNibNamed("HMCSearchDishesView", owner: self, options: nil)[0] as! HMCSearchDishesView
        searchDishesView.delegate = self
        
        searchDishesView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        
        self.addSubview(searchDishesView)
        
        
    }
    //MARK:- ANTagsViewProtocol Method
    
    func clickedANTagsView(antObj: ANTagsView!, tagIndex index: Int32) {
        print(index)
        
        
        showDishesAlertView(self.dishNameArr.objectAtIndex(Int(index)) as! String, atIndex: Int (index))
    }
    
    
    func showDishesAlertView(name:String, atIndex index:Int)
    {
        
        let alert=UIAlertController(title: "", message: "Do you want to delete \"\(name)\" from your top 10 dishes", preferredStyle: UIAlertControllerStyle.Alert);
        
        //event handler with closure
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
            self.dishNameArr.removeObjectAtIndex(index)
            self.dishesArr.removeObjectAtIndex(index)
            self.setDishesTagView()
            self.setTimerToManageRangeSliderView()
            
            
        }))
        //no event handler (just close dialog box)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
            
            
            
        }));
        let vc =   HMCUtlities.getTopViewController()
        vc.presentViewController(alert, animated: true, completion: nil);
        
    }
    
    /**
    Custom range slider action to define number of plate shafe can make
     */
    @IBAction func rangeSliderAction(sender: NMRangeSlider) {
        
        var lowerCenter = CGPoint()
        lowerCenter.x = self.rangeSlider.lowerCenter.x + self.rangeSlider.frame.origin.x
        lowerCenter.y = self.rangeSlider.center.y - 25.0
        lowerView.center = lowerCenter
        lowerLabel.text = String(Int(self.rangeSlider.lowerValue))
        
        var uperCenter = CGPoint()
        uperCenter.x = self.rangeSlider.upperCenter.x + self.rangeSlider.frame.origin.x
        uperCenter.y = self.rangeSlider.center.y - 25.0
        uperView.center = uperCenter
        uperLabel.text = String(Int(self.rangeSlider.upperValue))
        if Int(self.rangeSlider.lowerValue) == Int(self.rangeSlider.upperValue){
           lblPlateBetween.text = String(Int(self.rangeSlider.lowerValue)) + " plates"
        }
        else
        {
        lblPlateBetween.text = String(Int(self.rangeSlider.lowerValue)) + " to " + String(Int(self.rangeSlider.upperValue)) + " plates"
        }
        print(Int(self.rangeSlider.lowerValue))
        print(Int(self.rangeSlider.upperValue))
        
    }
    
    // MARK:- HMCSearchDishesViewDelegate Method
    func getSelectedDishes(dishDic: NSDictionary) {
      
        setTimerToManageRangeSliderView()
        for i in 0 ..< dishNameArr.count{
            
            let dishName = dishNameArr.objectAtIndex(i) as! String
            if (dishName == dishDic["dishName"] as! String)
            {
                HMCUtlities.showErrorMessage("You have already added \"\(dishName)\" in your top dishes")
                return
                
            }
        }
        dishesArr.addObject(dishDic)
        dishNameArr.addObject(dishDic["dishName"] as! String)
        self.setDishesTagView()
       
    }
    
}
