//
//  HMCTodaysPickupsViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/15/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
class HMCTodaysPickupsViewController: HMCBaseViewController,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var derList:NSArray?
    var deriverInfo = [NSDictionary]()
    
    var driverListArr = [HMCDriverInfoModel]()
    var selectedIndex = 0
    var mobileNumber = "0000000000"
    @IBOutlet var collectionView: UICollectionView!
    
    var detailReceipes:NSArray?
    var deriverInfoReceipe = [String]()
    var deriverOrderNo = [String]()
    var qty = [Int]()
    @IBOutlet weak var tableView: UITableView!
    var noDataView : HMCNoDataView!
    override func viewDidLoad() {
        super.viewDidLoad()
        settable()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "TODAY'S PICKUPS"
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.removeEmptyRows()
        collectionView.registerNib(UINib(nibName: "HMCTodaysPickUpCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HMCTodaysPickUpCollectionViewCell")
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("No pickups available", description: "Check back after sometime")
        noDataView.frame = CGRectMake(0, 0, screenWidth, screenHeight-100)
        self.view.addSubview(noDataView)

        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = true
        getDeliveryDetailFromServer()
        setSideMenuPanModeDefault()
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
     Calling APi to get Delivery Detail
     */
    func getDeliveryDetailFromServer()
    {
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_TodayPickUps) + "/\(HMCUtlities.getUserContactNumber())"
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            print(responseData)
            let dic : NSDictionary = responseData as! NSDictionary
            self.driverListArr = HMCDriverInfoModel.bindDataWithModel(dic["driversList"] as! NSArray)
            self.collectionView.reloadData()
            self.tableView.reloadData()
            self.view.hideLoader()
            }) { (errorMessage) -> () in
                print(errorMessage.localizedDescription)
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription as String)
                
        }
        
    }
    
    // MARK:- CollectionView DataSource and Delegate Method
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if driverListArr.count == 0{
            
            noDataView.hidden = false
        }
        else{
            noDataView.hidden = true
        }

        return driverListArr.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
        let driver = driverListArr[indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HMCTodaysPickUpCollectionViewCell", forIndexPath: indexPath) as! HMCTodaysPickUpCollectionViewCell
        cell.lblDriverName.text = driver.driverName
        
        if indexPath.row == selectedIndex {
            cell.lblDriverName.font = UIFont.boldSystemFontOfSize(16.0)
            collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .None)
        }
        else
        {
            cell.lblDriverName.font = UIFont(name:"OpenSans", size: 14.0)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 130, height: 110) // The size of one cell
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0) // margin between cells
    }
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath){
        
        collectionView.selectItemAtIndexPath(indexPath, animated: true, scrollPosition: .None)
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! HMCTodaysPickUpCollectionViewCell
        cell.lblDriverName.font = UIFont.boldSystemFontOfSize(16.0)
        selectedIndex = indexPath.row
        tableView.reloadData()
        
    }
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath)
    {
        print(indexPath)
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as? HMCTodaysPickUpCollectionViewCell
        cell?.lblDriverName.font = UIFont(name:"OpenSans", size: 14.0)
        
    }
    
    
    func setArray(response: NSDictionary)
    {
        self.derList = response["driversList"] as? NSArray
        for deriverList in derList!
        {
          
            let deriverList = deriverList as! NSDictionary
            self.deriverInfo.append(deriverList["driverInfo"] as! NSDictionary)
            print(deriverInfo)
            self.detailReceipes = deriverList["driverPlates"] as? NSArray
            print("@@@@@@@@@@@@@@@@@@@detail Recepies@@@@@@@@@@@@@@@@@@@")
            print(detailReceipes)
            
        }
        for detailReceipess in detailReceipes!
        {
            let detailReceipess = detailReceipess as! NSDictionary
            self.deriverInfoReceipe.append(detailReceipess["plateName"] as! String)
            self.deriverOrderNo.append(detailReceipess["orderNumber"] as! String)
            self.qty.append(detailReceipess["numberOfPlates"] as! Int)
            print("########################")
            print(deriverInfoReceipe)
        }
        self.tableView.reloadData()
    }
}
extension HMCTodaysPickupsViewController:  UITableViewDelegate, UITableViewDataSource
{
    
    
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View Delegates & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if driverListArr.count != 0 {
            let driver = driverListArr[selectedIndex]
            return  driver.driverPlates.count + 1
        }
        return 0
        
        
    }
      func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let driver = driverListArr[selectedIndex]
        
        if indexPath.row == 0{
            let cell0 =  tableView.dequeueReusableCellWithIdentifier("Cell0", forIndexPath: indexPath) as! HMCTodayPickUpCell0TableViewCell
            cell0.selectionStyle = .None
            cell0.deriverName.text = driver.driverName
            if driver.driverMobile == "UNASSIGNED"{
                cell0.deriverMobileNo.text = driver.driverMobile
                cell0.deriverMobileNo.textColor = ColorFromHexaCode(0x616161)
                cell0.deriverMobileNo.font = UIFont(name: cell0.deriverMobileNo.font.fontName, size: 12)
                cell0.contactLineView.hidden = true
                cell0.deriverMobileNo.userInteractionEnabled =  false
            }
            else{
                mobileNumber = driver.driverMobile
                cell0.deriverMobileNo.text = driver.driverMobile
                cell0.deriverMobileNo.textColor = ColorFromHexaCode(0xEFD18B)
                cell0.deriverMobileNo.font = UIFont(name: cell0.deriverMobileNo.font.fontName, size: 13)
                let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HMCTodaysPickupsViewController.labelAction(_:)))
                cell0.deriverMobileNo.userInteractionEnabled =  true
                cell0.deriverMobileNo.addGestureRecognizer(tap)
                cell0.contactLineView.hidden = false

            }
            return cell0
           
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell1", forIndexPath: indexPath) as! HMCTodayPickUpCell1TableViewCell
        let driverPlatesArr = driver.driverPlates
        cell.selectionStyle = .None
        let driverPlate = driverPlatesArr[indexPath.row-1] as! NSDictionary
        cell.item.text = driverPlate["plateName"] as? String
        cell.orderNo.text = driverPlate["orderNumber"] as? String
        cell.quantity.text = String(driverPlate["numberOfPlates"] as! Int)
        
        return cell
        
    }
    func labelAction(gr:UITapGestureRecognizer)
    {
        if let url = NSURL(string: "telprompt://91\(mobileNumber)") {
            UIApplication.sharedApplication().openURL(url)
        }
        else
        {
            self.showErrorMessage("Please check for SIM card")
        }
  
        
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return 40
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 195
        }
        return UITableViewAutomaticDimension
    }
 
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

