//
//  HMCOTPVerificationViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 06/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCOTPVerificationViewController: HMCBaseViewController {

    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var btnResendOTP: UIButton!
    @IBOutlet var txtFldOTP: HMCTextField!
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "VERIFICATION"
        self.txtFldOTP.leftPadding(0)
        setBackButton()
        lblMsg.text = "One Time Password has been sent to " + HMCUtlities.getUserContactNumber()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        
        
    }
    /**
     Callling Api to Validate OTP
   
     */
    @IBAction func verifyButtonAction(sender: AnyObject) {
        
        if(!HMCValidation.validateOTP(self.txtFldOTP.text!))
        {
            return;
        }
        
        self.view.showLoader()
        
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Confirm_OTP)+"/\(HMCUtlities.getUserContactNumber())" + "/\(self.txtFldOTP.text!)"
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            
            print(responseData)
             let responseData = responseData as! NSDictionary
            let validOTP : Bool = responseData["validOTP"] as! Bool
            if !validOTP {
                HMCUtlities.showErrorMessage(responseData["message"] as! String)
                HMCUtlities.setOTPStatus(true)
                self.checkUserTermsNConditionStatus()
                
            }
            else
            {
                self.view.hideLoader()
                HMCUtlities.showErrorMessage(responseData["message"] as! String)
            }
            

            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }

    }
    
    /**
     Method use to check User terms & condition accepted or not
     */
    func checkUserTermsNConditionStatus()
    {
        
         let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Check_TnC_Status)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            print(responseData)
            let responseData = responseData as! NSDictionary
            let success : Bool = responseData["isTnCAccepted"] as! Bool
            if success {
                self.checkUserQuestionnarieStatus()
              
            }
            else
            {
                self.view.hideLoader()
                let terms_ConditionsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCTerms_ConditionsViewController") as? HMCTerms_ConditionsViewController
                self.navigationController?.pushViewController(terms_ConditionsViewController!, animated: true)
                
            }
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
        
    }
    /**
     Method use to check user Questionaries Status
     */
    func checkUserQuestionnarieStatus()
    {
        
          let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Check_QnA_Status)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            
            print(responseData)
            let responseData = responseData as! NSDictionary
//            if (responseData["error"] == nil){
//                
//                return
//            }
            var statusChefQnA = Bool()
            
            if (responseData["chefQnA"] is NSNull) {
                statusChefQnA = false
                
            }else
            {
                statusChefQnA = true
            }
            
            if statusChefQnA {
                HMCUtlities.setQNAStatus(true)
                self.view.hideLoader()
                HMCUtlities.setUserCurrentStatus(ChefStatus.ACTIVE.description)
                HMCUtlities.sharedInstance.navigateToHomeViewController()
                
            }
            else
            {
                self.view.hideLoader()
                let questionnaireViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCQuestionnaireViewController") as? HMCQuestionnaireViewController
                self.navigationController?.pushViewController(questionnaireViewController!, animated: true)
            }
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
        
    }
 
    
    /**
     Method use to resend the OTP
    */
    
    @IBAction func resendOTPButtonAction(sender: AnyObject) {
        
        count = count + 1
        self.view.showLoader()
        let contact : String = HMCUtlities.getUserContactNumber()
        let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Resend_OTP)+"/\(contact)"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
            let responseData = responseData as! NSDictionary
            let success : Bool = responseData["success"] as! Bool
            if success {
               
            }
            else
            {
            HMCUtlities.showErrorMessage(responseData["message"] as! String)
            }
            
            }) { (errorMessage) -> () in
              self.view.hideLoader()
             HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
           if count == 3
          {
           btnResendOTP.hidden = true
          }
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
