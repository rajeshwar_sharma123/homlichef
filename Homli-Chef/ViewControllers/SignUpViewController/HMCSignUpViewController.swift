//
//  HMCSignUpViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 25/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit



class HMCSignUpViewController: UIViewController,SEFilterControlDelegate,HMCChooseOptionViewDelegate,HMCCookingDetailsViewDelegate{
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblSubName: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var cameraContentView: UIView!
    @IBOutlet var sliderContentView: UIView!
    @IBOutlet var logoImgView: UIImageView!
    @IBOutlet var btnHelp: UIButton!
    var sliderView : SEFilterControl!
    @IBOutlet var selectedImgContentView: UIView!
    @IBOutlet var selectedImgView: UIImageView!
    @IBOutlet var lblSubNameConstraintHeight: NSLayoutConstraint!
    @IBOutlet var logoCenterConstraintY: NSLayoutConstraint!
    var index : NSInteger!
    var cookingDetailsView : HMCCookingDetailsView!
    var chooseCuisinesView : HMCChooseCuisinesView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setHowToJoinUsView()
        cameraContentView.layer.cornerRadius = 22
        cameraContentView.clipsToBounds = true
        self.selectedImgContentView.layer.cornerRadius = 24
        self.selectedImgContentView.layer.borderWidth = 2.5
        self.selectedImgContentView.layer.borderColor = UIColor.whiteColor().CGColor
        self.selectedImgContentView.clipsToBounds = true
        lblName.setAttributedStringWith("To get started,", fontFamily: "OpenSans", fontSize: COMMON_SIZE, hexaColorCode:0xF4F4F4)
        lblSubName.setTwoTypesAttributedString("Add your ID Proof", subText: "ID Proof", fontFamilyForTxtName: "OpenSans", fontFamilyForSubTxtName: "OpenSans-Semibold", fontSizeTxtName: COMMON_SIZE, fontSizeSubTxtName: COMMON_SIZE)
        initCustomView()
        setSliderView()
    }
    override  func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setHowToJoinUsView(){
        let howToJoinUsView =  NSBundle.mainBundle().loadNibNamed("HMCHowToJoinUsView", owner: self, options: nil)[0] as! HMCHowToJoinUsView
        howToJoinUsView.frame = self.view.frame
        self.view.addSubview(howToJoinUsView)

    }
    //******************** Initializing Custome View ****************
    func initCustomView()
    {
        cookingDetailsView =  NSBundle.mainBundle().loadNibNamed("HMCCookingDetailsView", owner: self, options: nil)[0] as! HMCCookingDetailsView
        cookingDetailsView.delegate = self
        cookingDetailsView.frame = self.view.frame
        chooseCuisinesView =  NSBundle.mainBundle().loadNibNamed("HMCChooseCuisinesView", owner: self, options: nil)[0] as! HMCChooseCuisinesView
        chooseCuisinesView.frame = self.view.frame

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = true
 }
    
    func setSliderView()
    {
        let titleArr = ["IDENTITY PROOF", "ADDRESS PROOF", "BANK PROOF","PROFILE PICTURE","COOKING DETAILS"]
        let imagesArr = [
            
            [   "imgSmall" : "ic_identity",
                "imgNormal" : "ic_plate_identity",
                "imgSelected": "ic_plate_identity_blue"
            ],
            [    "imgSmall" : "ic_home",
                "imgNormal" : "ic_plate_home",
                "imgSelected": "ic_plate_home_blue"
            ],
            [    "imgSmall" : "ic_bank",
                "imgNormal" : "ic_plate_bank",
                "imgSelected": "ic_plate_bank_blue"
            ],
            [    "imgSmall" : "ic_user",
                "imgNormal" : "ic_plate_user",
                "imgSelected": "ic_plate_user_blue"
            ],
            [    "imgSmall" : "ic_cooking",
                "imgNormal" : "ic_plate_cooking",
                "imgSelected": "ic_plate_cooking_blue"
            ]
        
        ]
        sliderView  = SEFilterControl(frame: CGRectMake(5, 10, screenWidth-10, 10), titles: titleArr, images: imagesArr)
        sliderView.delegate = self
        self.sliderContentView.addSubview(sliderView)
        index = 0
        
    }
    //MARK:- SEFilterControlDelegate Method
    func handelerMoveAtIndex(index: Int) {
        
        let signUpModel = HMCSignUpModel.sharedInstance()
        self.index = index
        lblSubNameConstraintHeight.constant = 0
        logoCenterConstraintY.constant = -93
        cameraContentView.hidden = false
        self.selectedImgContentView.hidden = true
        cookingDetailsView.removeFromSuperview()
       
        switch(index)
        {
        case 0 :
            lblName.setAttributedStringWith("To get started,", fontFamily: "OpenSans", fontSize: COMMON_SIZE, hexaColorCode:0xF4F4F4)
            lblDescription.text = "Your Identity Proof"
            lblSubName.setTwoTypesAttributedString("Add your ID Proof", subText: "ID Proof", fontFamilyForTxtName: "OpenSans", fontFamilyForSubTxtName: "OpenSans-Semibold", fontSizeTxtName: COMMON_SIZE, fontSizeSubTxtName: COMMON_SIZE)

            lblSubNameConstraintHeight.constant = 30
            if signUpModel.identityProof != nil{
                setLogoCenterConstraintConstant()
                cameraContentView.hidden = true
                self.selectedImgContentView.hidden = false
                self.selectedImgView.image = signUpModel.identityProof
            }
            
            
        case 1 :
            lblName.setAttributedStringWith("Address Proof", fontFamily: "OpenSans-Semibold", fontSize: COMMON_SIZE, hexaColorCode:0xFFFFFF)
            lblDescription.text = "Your Address Proof"
            if signUpModel.addressProof != nil{
                setLogoCenterConstraintConstant()
                cameraContentView.hidden = true
                self.selectedImgContentView.hidden = false
                self.selectedImgView.image = signUpModel.addressProof
            }
        case 2 :
            lblName.setAttributedStringWith("Bank Proof", fontFamily: "OpenSans-Semibold", fontSize: COMMON_SIZE, hexaColorCode:0xFFFFFF)
            lblDescription.text = "Your Bank Proof"
            if signUpModel.bankProof != nil{
                setLogoCenterConstraintConstant()
                cameraContentView.hidden = true
                self.selectedImgContentView.hidden = false
                self.selectedImgView.image = signUpModel.bankProof
            }
            
        case 3 :
            lblName.setAttributedStringWith("Profile Picture", fontFamily: "OpenSans-Semibold", fontSize: COMMON_SIZE, hexaColorCode:0xFFFFFF)
            lblDescription.text = "Your Profile Picture"
            if signUpModel.profilePicture != nil{
                setLogoCenterConstraintConstant()
                cameraContentView.hidden = true
                self.selectedImgContentView.hidden = false
                self.selectedImgView.image = signUpModel.profilePicture
            }
            
        case 4 :
           
            self.view.addSubview(cookingDetailsView)
            self.view.bringSubviewToFront(btnHelp)
            self.view.bringSubviewToFront(sliderContentView)
            
            
        default :
            print("")
            
        }
        
        
    }
    
    func setAttributedStringOnLblSubName()
    {
        let longString = "Add your ID Proof"
        let longestWord = "ID Proof"
        let longestWordRange = (longString as NSString).rangeOfString(longestWord)
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont(name: "OpenSans", size: 24.0)!])
        attributedString.setAttributes([NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: 24.0)!, NSForegroundColorAttributeName : UIColor.whiteColor()], range: longestWordRange)
        lblSubName.attributedText = attributedString
    }
    func setAttributedString(name:String)
    {
       
        let attributedString = NSMutableAttributedString(string: name, attributes: [NSFontAttributeName : UIFont(name: "OpenSans-Semibold", size: 24.0)!])
        lblSubName.attributedText = attributedString
    }
    //MARK:- Tap Gesture Action
    
    @IBAction func cameraContentViewTapGestureAction(sender: AnyObject) {
        
       setChooseOptionView()
        
    }
    
    @IBAction func selectedImgViewTapGestureAction(sender: AnyObject) {
        print("img view")
        let proofImgViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCProofImgViewController") as! HMCProofImgViewController
        proofImgViewController.identityProofImg = selectedImgView.image
        proofImgViewController.identityName = getTitleName()
        self.navigationController?.pushViewController(proofImgViewController, animated: true)
    }
    func setChooseOptionView()
    {
        let chooseOptionView =  NSBundle.mainBundle().loadNibNamed("HMCChooseOptionView", owner: self, options: nil)[0] as! HMCChooseOptionView
        chooseOptionView.frame = self.view.frame
        chooseOptionView.index = self.index
        chooseOptionView.delegate = self
        self.view.addSubview(chooseOptionView)

    }
    func getTitleName()->String{
        let titleArr = ["IDENTITY PROOF", "ADDRESS PROOF", "BANK PROOF","PROFILE PICTURE","COOKING DETAILS"] 
         let title = titleArr[self.index]
        return title
    }
    
    //MARK:- Custom Button Action 
    @IBAction func helpButtonAction(sender: AnyObject) {
        lblSubNameConstraintHeight.constant = 0
        switch(self.index)
        {
        case 0 :
            let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpIdentityProofView", owner: self, options: nil)[0] as! UIView
            view.frame = self.view.frame
            self.view.addSubview(view)
        case 1 :
            let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpAddressProofView", owner: self, options: nil)[0] as! UIView
            view.frame = self.view.frame
            self.view.addSubview(view)
        case 2 :
            let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpBankProofView", owner: self, options: nil)[0] as! UIView
            view.frame = self.view.frame
            self.view.addSubview(view)
        case 3 :
            let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpProfilePictureView", owner: self, options: nil)[0] as! UIView
            view.frame = self.view.frame
            self.view.addSubview(view)
        case 4 :
            print("in case 4")
            let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpCookingDetailView", owner: self, options: nil)[0] as! UIView
            view.frame = self.view.frame
            self.view.addSubview(view)
        default :
            print("")
            
        }

    }
    
    @IBAction func editProfileButtonAction(sender: AnyObject) {
        
        setChooseOptionView()
        
    }
    func setLogoCenterConstraintConstant()
    {
        logoCenterConstraintY.constant = -140
    }
    
    //MARK:- HMCChooseOptionViewDelegate Method
    func setSelectedImage(image: UIImage, atIndex index: NSInteger) {
        let signUpModel = HMCSignUpModel.sharedInstance()
        
        self.selectedImgContentView.hidden = false
        self.selectedImgView.image = image
        cameraContentView.hidden = true
        setLogoCenterConstraintConstant()

        switch(index)
        {
        case 0 :
            signUpModel.identityProof = image
            
        case 1 :
             signUpModel.addressProof = image
            
        case 2 :
             signUpModel.bankProof = image
            
        case 3 :
            signUpModel.profilePicture = image
            
        case 4 :
            print("")
        default :
            print("")
        }
        
    }
    //MARK:- HMCCookingDetailsViewDelegate Method
    func setCuisinesViewForTextField(textField: UITextField) {
        
        self.view.addSubview(chooseCuisinesView)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
