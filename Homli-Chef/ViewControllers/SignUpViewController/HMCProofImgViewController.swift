//
//  HMCProofImgViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 27/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
//import ImageScrollView
class HMCProofImgViewController: HMCBaseViewController {

    var identityProofImg : UIImage!
    var identityName : String!
    @IBOutlet var scrollView: ImageScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = identityName
        setBackButton()
        setUserImage()
        self.view.backgroundColor = ColorFromHexaCode(k_Light_Gray_Color)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        
    }
    
    func setUserImage()
    {
        scrollView.displayImage(identityProofImg)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
