//
//  HMCQuestionnaireViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 31/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCQuestionnaireViewController: HMCBaseViewController {
    @IBOutlet var mainScrollView: UIScrollView!
    var flag=0
     var pageControl : UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "YOUR DETAILS"
        setBackButton()
        initCustomView()
        self.navigationController!.interactivePopGestureRecognizer!.enabled = false
        configurePageControl()
        
        
        
    }
        override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
           NSNotificationCenter.defaultCenter().addObserver( self,selector: #selector(HMCQuestionnaireViewController.sliderTouchDown(_:)),name: "NOTIFY_SLIDER_TOUCH_BEGAN", object: nil)
        
    }
    /**
     Method use to set Page Contoll
     */
    func configurePageControl() {
        pageControl = UIPageControl(frame: CGRectMake((screenWidth-200)/2, screenHeight-20, 200, 20))
        self.pageControl.numberOfPages = 2
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.redColor()
        self.pageControl.pageIndicatorTintColor = UIColor.blackColor()
        self.pageControl.currentPageIndicatorTintColor = ColorFromHexaCode(k_Theme_Color)
        self.view.addSubview(pageControl)
    }
    func sliderTouchDown(notification: NSNotification){
        print("began")
    }
    /**
     Method use to initialize custom views
   
     */
    func initCustomView()
    {
      // mainScrollView.scrollEnabled = false
        mainScrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        mainScrollView.backgroundColor = UIColor.redColor()
        mainScrollView.contentSize = CGSizeMake(screenWidth*2, 0)
        mainScrollView.contentInset = UIEdgeInsetsZero;
        mainScrollView.delaysContentTouches = false
        let yourDetailsView =  NSBundle.mainBundle().loadNibNamed("HMCYourDetailsView", owner: self, options: nil)[0] as! HMCYourDetailsView
        yourDetailsView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        yourDetailsView.initWithDefaultValue()
       self.mainScrollView.addSubview(yourDetailsView)
        
        let questionnaireView =  NSBundle.mainBundle().loadNibNamed("HMCQuestionnaireView", owner: self, options: nil)[0] as! HMCQuestionnaireView
        questionnaireView.initWithDefaultValue()
        questionnaireView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        questionnaireView.frame.origin.x = mainScrollView.frame.size.width
        //receiptView.initDefaultValue(orderDetail)
        self.mainScrollView.addSubview(questionnaireView)
        
        
    }

    /**
     Method set the Page title name when scroll view end scrolling
    
     */
    func scrollViewDidEndDecelerating(scrollView: UIScrollView)
    {
        print("scrolled")
        if(flag==0)
        {
           
            flag=1
             mainScrollView.contentOffset = CGPoint(x: screenWidth, y: 0)
            self.title = "QUESTIONNAIRE"
        }
        else
        {
            
            flag = 0
             mainScrollView.contentOffset = CGPoint(x: 0, y: 0)
            self.title = "YOUR DETAILS"
            
        }
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
   

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
