//
//  HMCEarningsTableViewCell.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/14/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCEarningsTableViewCell: UITableViewCell {

    @IBOutlet weak var earningDetail: UILabel!
    @IBOutlet weak var earningPrize: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
