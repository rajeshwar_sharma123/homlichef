//
//  HMCEarningsViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/14/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
class HMCEarningsViewController: HMCBaseViewController {
    
    var plateNameArray=[String]()
    var numberOfPlatesArray = [String]()
    var prizeArray = [String]()
    
    var unicodeArray:[String] = [String]()
    
    @IBOutlet var bottomView: UIView!
    var todaysEarnings = NSArray()
    var mtdEarnings = NSArray()
    var ltdEarnings = NSArray()
    var mainDataArr = NSArray()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var earningsTodaybtn: UIButton!
    @IBOutlet weak var earningsTillMonthbtn: UIButton!
    
    @IBOutlet var lblTotalAmnt: UILabel!
    @IBOutlet weak var earningsTillDatebtn: UIButton!
    var noDataView : HMCNoDataView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "EARNINGS"
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("Earn as you cook", description: "Start COOKING")
        self.tableView.backgroundView = noDataView
        settable()
        earningsTodaybtn.titleLabel?.numberOfLines = 2
        earningsTodaybtn.titleLabel?.textAlignment = .Center
        earningsTillMonthbtn.titleLabel?.numberOfLines = 2
        earningsTillMonthbtn.titleLabel?.textAlignment = .Center
        earningsTillDatebtn.titleLabel?.numberOfLines = 2
        earningsTillDatebtn.titleLabel?.textAlignment = .Center
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 40
        tableView.removeEmptyRows()
        earningsToday(earningsTodaybtn)
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = true
        setSideMenuPanModeDefault()
         getDeliveryDetailFromServer()
    }
    
    /**
     Method use to show Earnings Today Detail
    */
    @IBAction func earningsToday(sender: AnyObject) {
        
        earningsTodaybtn.backgroundColor = ColorFromHexaCode(0xEBC97A)
        earningsTillMonthbtn.backgroundColor=ColorFromHexaCode(0xF5F5F5)
        earningsTillDatebtn.backgroundColor = ColorFromHexaCode(0xF5F5F5)
        mainDataArr = todaysEarnings
        tableView.reloadData()
        setTotalAmount()
        
    }
    /**
     Method use to show Earnings Till Month Detail
     */
    @IBAction func earningsTillMonthAction(sender: AnyObject) {
        
        earningsTillMonthbtn.backgroundColor=ColorFromHexaCode(0xEBC97A)
        earningsTodaybtn.backgroundColor = ColorFromHexaCode(0xF5F5F5)
        earningsTillDatebtn.backgroundColor = ColorFromHexaCode(0xF5F5F5)
        mainDataArr = mtdEarnings
        tableView.reloadData()
        setTotalAmount()
        
    }
    /**
     Method use to show Earnings Til Date Detail
     */
    @IBAction func earningsTillDateAction(sender: AnyObject) {
        earningsTillDatebtn.backgroundColor = ColorFromHexaCode(0xEBC97A)
        earningsTodaybtn.backgroundColor = ColorFromHexaCode(0xF5F5F5)
        earningsTillMonthbtn.backgroundColor=ColorFromHexaCode(0xF5F5F5)
        mainDataArr = ltdEarnings
        tableView.reloadData()
        setTotalAmount()
    }
    
    func setTotalAmount(){
        let arrAmnt = mainDataArr.valueForKey("earning") as! NSArray
        var sum : Float = 0
        for amount in arrAmnt {
            sum = sum + Float(amount as! NSNumber)
        }
        lblTotalAmnt.text = sum.asLocaleCurrency
    }
    
    /**
     Calling api to get Delivery Detail List
     */
    func getDeliveryDetailFromServer()
    {
      let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Earings) + "/\(HMCUtlities.getUserContactNumber())"
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            self.setDataInArray(responseData as! NSDictionary)
            self.earningsToday(self.earningsTodaybtn)
            self.view.hideLoader()
            }) { (errorMessage) -> () in
                print(errorMessage.localizedDescription)
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription as String)
                
        }
        
    }
    /**
     Method use to hold earning details seperately
     */
    func setDataInArray(response: NSDictionary)
    {
        self.todaysEarnings = response["todaysEarning"] as! NSArray
        print(self.todaysEarnings)
        self.mtdEarnings = response["mtdEarning"] as! NSArray
        self.ltdEarnings = response["ltdEarning"] as! NSArray
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}



extension HMCEarningsViewController:  UITableViewDelegate, UITableViewDataSource
{
    
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View Delegate & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if mainDataArr.count == 0{
            
            noDataView.hidden = false
            bottomView.hidden = true
        }
        else{
            noDataView.hidden = true
            bottomView.hidden = false
        }

        return mainDataArr.count
        
        
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HMCEarningsTableViewCell
        cell.selectionStyle = .None
        if mainDataArr.count == 0
        {
            if indexPath.row % 2 == 0 {
                cell.earningDetail.text = "detail sjhdfjshdjf "
            }
            else{
                cell.earningDetail.text = "detail"
            }
            
        }
        else
        {
            let dic = mainDataArr.objectAtIndex(indexPath.row) as! NSDictionary
            cell.earningDetail.text = "\(dic["plateName"])(\(dic["numberOfPlates"]))"
            let amount = dic["earning"] as! Float
            cell.earningPrize.text = amount.asLocaleCurrency
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
   
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

