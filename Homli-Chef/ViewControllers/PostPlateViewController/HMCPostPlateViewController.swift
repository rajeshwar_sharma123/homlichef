//
//  HMCPostPlateViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 15/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCPostPlateViewController: UIViewController,SEFilterControlDelegate,UITextFieldDelegate {
    
    var previousPlateBarBtn: UIButton!
    @IBOutlet var txtFldSearch: HMCTextField!
    @IBOutlet var searchView: UIView!
    @IBOutlet var plateView: UIView!
    @IBOutlet var editPlateNumView: UIView!
    @IBOutlet var mainCourseView: UIView!
    @IBOutlet var lblCourseName: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var lblPlateCount: UILabel!
    @IBOutlet var sliderContentView: UIView!
    
    @IBOutlet var lblMainCourse: UILabel!
    
    @IBOutlet var lblSubCourse1: UILabel!
    
    @IBOutlet var lblSubCourse2: UILabel!
    
    @IBOutlet var btnEditMainCourrse: UIButton!
    
    @IBOutlet var btnEditSubCourse2: UIButton!
    @IBOutlet var btnEditSubCourse1: UIButton!
    var sliderView : SEFilterControl!
    var dishesArray = NSArray()
    var dishNameStr = ""
    var flagDishCategory = DishCategoryCourse.MAIN_COURSE.description
    let createPlateModel  = HMCCreatePlateModel.sharedInstance()
    
    @IBOutlet var btnSubmit: UIButton!
    @IBOutlet var plateTableView: UITableView!
    @IBOutlet var mainCourseViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var lblCourseNameHeightConstraint: NSLayoutConstraint!
    var flagReview = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "DEFINE PLATE"
        tableView.removeEmptyRows()
        setSliderView()
        registerCellWithTableView()
        addButtonOnLeftWithImageName()
        txtFldSearch.backgroundColor = UIColor.whiteColor()
        plateView.backgroundColor = UIColor.clearColor()
        
        plateView.hidden = true
        tableView.hidden = true
        btnEditSubCourse1.hidden = true
        btnEditSubCourse2.hidden = true
        self.lblMainCourse.font =  UIFont(name:"OpenSans-Semibold", size: 14.0)
        plateTableView.removeEmptyRows()
        setPreviousPlateNavigationBarBtn()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        if  createPlateModel.fromWhereStr == PostPlateCheck.PREVIOUS_PLATE.description{
            NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(HMCPostPlateViewController.moveToPlateConfirmation), userInfo: nil, repeats: false)
        }
        
        setBackButton()
        
    }
    
    func setBackButton()
    {
        let buttonImage = UIImage(named: "ic_action_close");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.Custom)
        leftBarButton.frame = CGRectMake(0, 0, 40,  40) ;
        leftBarButton.setImage(buttonImage, forState: UIControlState.Normal)
        leftBarButton.addTarget(self, action:#selector(HMCPostPlateViewController.backBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        self.navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: false);
    }
    
    func backBtnClicked(sender:UIButton!)
    {
        self.popToViewController(HMCYourPlatesViewController)
        
    }
    /**
     Method use to register cell with tableview
     */
    func registerCellWithTableView(){
        self.tableView.registerNib(UINib(nibName: "HMCSearchPlateCell", bundle: nil), forCellReuseIdentifier: "HMCSearchPlateCell")
        self.plateTableView.registerNib(UINib(nibName: "HMCEditPlateCell", bundle: nil), forCellReuseIdentifier: "HMCEditPlateCell")
        self.plateTableView.registerNib(UINib(nibName: "HMCCourseDetailCell", bundle: nil), forCellReuseIdentifier: "HMCCourseDetailCell")
        
        
    }
    /**
     Method use to add left image on textfield
     */
    func addButtonOnLeftWithImageName()
    {
        let paddingView = UIView(frame:CGRectMake(0, 0,46, txtFldSearch.frame.size.height))
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.enabled = false
        button.frame = CGRectMake(18, (txtFldSearch.frame.height-20)/2, 20, 20)
        txtFldSearch.leftViewMode = UITextFieldViewMode.Always
        button.setBackgroundImage(UIImage(named: "ic_search"), forState: UIControlState.Normal)
        paddingView.addSubview(button)
        txtFldSearch.leftView = paddingView
        
        
    }
    /**
     Method use to set Custom Slider View
     */
    func setSliderView()
    {
        let titleArr = ["DEFINE PLATE", "REVIEW PLATE", "PLATE CONFIRMATION"]
        
        
        let imagesArr = [
            
            [   "imgSmall" : "ic_plate_grey",
                "imgNormal" : "ic_plate",
                "imgSelected": "ic_plate_blue"
            ],
            [    "imgSmall" : "ic_edit",
                "imgNormal" : "ic_edit_plate",
                "imgSelected": "ic_edit_blue"
            ],
            [    "imgSmall" : "ic_right",
                "imgNormal" : "ic_right_plate",
                "imgSelected": "ic_right_blue"
            ]
            
        ]
        
        
        
        sliderView  = SEFilterControl(frame: CGRectMake(5, 10, screenWidth-10, 10), titles: titleArr, images: imagesArr)
        sliderView.delegate = self
        self.sliderContentView.addSubview(sliderView)
        NSTimer.scheduledTimerWithTimeInterval(0.2, target: self, selector: #selector(HMCPostPlateViewController.moveToPlateConfirmation), userInfo: nil, repeats: false)
        
        
    }
    
    
    
    func moveToPlateConfirmation() {
        
        if createPlateModel.fromWhereStr == PostPlateCheck.RECOMMENDATION.description || createPlateModel.fromWhereStr == PostPlateCheck.PREVIOUS_PLATE.description {
            sliderView.setSelectedIndex(2, animated: true)
            btnEditSubCourse1.tag = 2
            btnEditMainCourrse.tag = 3
            lblMainCourse.text = createPlateModel.mainCourse["dishName"] as? String
            lblSubCourse1.text = createPlateModel.subCourse1["dishName"] as? String
            
            if createPlateModel.mainCourse.count != 0 {
                
            }
            if createPlateModel.subCourse2.count != 0 {
                lblSubCourse2.text = createPlateModel.subCourse2["dishName"] as? String
            }
            
            
        }
    }
    //MARK:- SEFilterControlDelegate Method
    func handelerMoveAtIndex(index: Int) {
        
        switch(index)
        {
        case 0 :
            
            setValueForDefinePlate()
        case 1 :
            setValueForPlateReview()
        case 2 :
            setValueForPlateConfirmation()
        default :
            print("")
            
        }
        
    }
    /**
     Method use to set Previous Plates right navigation bar buttton
     */
    func setPreviousPlateNavigationBarBtn()
    {
        let buttonImage = UIImage(named: "ic_previousplate");
        previousPlateBarBtn = UIButton(type: UIButtonType.Custom)
        previousPlateBarBtn.frame = CGRectMake(0, 0, 30,  30) ;
        previousPlateBarBtn.setImage(buttonImage, forState: UIControlState.Normal)
        previousPlateBarBtn.addTarget(self, action:#selector(HMCPostPlateViewController.previousPlateBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        let rightBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: previousPlateBarBtn)
        self.navigationItem.setRightBarButtonItem(rightBarButtonItem, animated: false);
    }
    func previousPlateBtnClicked(sender:UIButton!)
    {
        let previousPlatesViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCPreviousPlatesViewController") as! HMCPreviousPlatesViewController
        self.navigationController?.pushViewController(previousPlatesViewController, animated: true)
        
    }
    
    /**
     Method use to set all course in normal state
     */
    func setAllCourseInNormalState(){
        previousPlateBarBtn.hidden = true
        plateTableView.reloadData()
        plateView.hidden = false
        searchView.hidden = true
        hideAllEditCourseButton()
        self.lblMainCourse.font = UIFont(name:"OpenSans", size: 14.0)
        self.lblSubCourse1.font = UIFont(name:"OpenSans", size: 14.0)
        self.lblSubCourse2.font = UIFont(name:"OpenSans", size: 14.0)
    }
    func setValueForDefinePlate()
    {
        previousPlateBarBtn.hidden = false
        self.navigationItem.title = "DEFINE PLATE"
        searchView.hidden = false
        plateView.hidden = true
        btnEditMainCourrse.hidden = false
        setFocusOnEditMainCourse()
        if createPlateModel.mainCourse.count != 0 {
            btnEditSubCourse1.hidden = false
        }
        if createPlateModel.subCourse1.count != 0 {
            btnEditSubCourse2.hidden = false
        }
        
    }
    func setValueForPlateReview()
    {
        self.navigationItem.title = "REVIEW PLATE"
        flagReview = true
        btnSubmit.hidden = true
        setAllCourseInNormalState()
        
    }
    func setValueForPlateConfirmation()
    {
        self.navigationItem.title = "PLATE CONFIRMATION"
        flagReview = false
        btnSubmit.hidden = false
        setAllCourseInNormalState()
        
    }
    
    func hideAllEditCourseButton()
    {
        btnEditMainCourrse.hidden = true
        btnEditSubCourse1.hidden = true
        btnEditSubCourse2.hidden = true
        
    }
    func getCompleteCourse() -> String{
        var courseStr = ""
        if createPlateModel.mainCourse.count != 0{
            courseStr = "\(createPlateModel.mainCourse["dishName"]!)"
        }
        if createPlateModel.subCourse1.count != 0{
            courseStr =  "\(courseStr) with \(createPlateModel.subCourse1["dishName"]!)"
        }
        if createPlateModel.subCourse2.count != 0{
            courseStr =  "\(courseStr) and \(createPlateModel.subCourse2["dishName"]!)"
        }
        return courseStr
    }
    
    //MARK:- UITableViewDelegate  Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.plateTableView{
            if flagReview {
                return 1
            }
            return 2
        }
        return dishesArray.count
        
        
    }
    //MARK: Table View Delegates & DataSource Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if tableView == self.plateTableView{
            if indexPath.row == 0
            {
                
                let cell = tableView.dequeueReusableCellWithIdentifier("HMCCourseDetailCell", forIndexPath: indexPath) as! HMCCourseDetailCell
                cell.selectionStyle = .None
                let courseStr = getCompleteCourse()
                if !courseStr.isEmpty{
                    cell.lblCourseName?.text = courseStr
                    createPlateModel.completeCourseName = courseStr
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCellWithIdentifier("HMCEditPlateCell", forIndexPath: indexPath) as! HMCEditPlateCell
                cell.selectionStyle = .None
                cell.btnDecrease.addTarget(self, action: #selector(HMCPostPlateViewController.decreaseButtonAction(_:)), forControlEvents: .TouchUpInside)
                cell.btnIncrease.addTarget(self, action: #selector(HMCPostPlateViewController.increaseButtonAction(_:)), forControlEvents: .TouchUpInside)
                cell.lblPlateCount.text = createPlateModel.numberOfPlate
                return cell
                
            }
            
            
        }
        
        
        let dic = dishesArray[indexPath.row] as! NSDictionary
        let cell = tableView.dequeueReusableCellWithIdentifier("HMCSearchPlateCell", forIndexPath: indexPath) as! HMCSearchPlateCell
        cell.selectionStyle = .None
        cell.lblPlateName.text = dic["dishName"] as? String
        cell.lblPlateDescription.text = dic["dishDescription"] as? String
        
        return cell
        
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == self.plateTableView{
            
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            }
            
            return 70
        }
        return 58
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return 50
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.tableView{
            let headerView = UIView(frame: CGRectMake(0, 0, screenWidth, 35))
            headerView.backgroundColor = ColorFromHexaCode(0xF1F1F1)
            let lineView = UIView(frame: CGRectMake(0, 0, screenWidth, 1))
            lineView.backgroundColor = UIColor.grayColor()
            let lblHeaderTitle = UILabel(frame: CGRectMake(15, 1, headerView.frame.size.width-15, 24))
            lblHeaderTitle.backgroundColor = UIColor.clearColor()
            lblHeaderTitle.textColor = UIColor.blackColor()
            lblHeaderTitle.font = UIFont(name: "OpenSans", size: 17)
            lblHeaderTitle.text = "We have \"\(dishNameStr)\""
            headerView.addSubview(lineView)
            headerView.addSubview(lblHeaderTitle)
            return headerView
        }
        return UIView()
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.plateTableView{
            return 0
        }
        return 25
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if tableView == self.plateTableView {
            return
        }
        
        if createPlateModel.fromWhereStr == PostPlateCheck.RECOMMENDATION.description || createPlateModel.fromWhereStr == PostPlateCheck.PREVIOUS_PLATE.description{
            createPlateModel.flag = true
        }
        else
        {
            createPlateModel.flag = false
        }
        print(createPlateModel.mainCourse)
        print(createPlateModel.subCourse1)
        print(flagDishCategory)
        let dic = dishesArray[indexPath.row] as! NSDictionary
        
        if flagDishCategory == DishCategoryCourse.MAIN_COURSE.description{
            lblMainCourse.text = dic["dishName"] as? String
            btnEditSubCourse1.hidden = false
            if btnEditSubCourse1.tag != 2 && btnEditSubCourse1.tag != 3{
                btnEditSubCourse1.tag = 1
            }
            showPlateAddedSuccessfullyMsg(dic["dishName"] as! String, category: "Main Course")
            createPlateModel.mainCourse = dic
            print(createPlateModel.mainCourse)
        }
        else if flagDishCategory == DishCategoryCourse.SUB_COURSE1.description{
            lblSubCourse1.text = dic["dishName"] as? String
            btnEditSubCourse2.hidden = false
            if btnEditSubCourse2.tag != 2{
                btnEditSubCourse2.tag = 1
            }
            showPlateAddedSuccessfullyMsg(dic["dishName"] as! String, category: "Sub Course")
            createPlateModel.subCourse1 = dic
            print(createPlateModel.subCourse1)
        }
        else if flagDishCategory == DishCategoryCourse.SUB_COURSE2.description{
            lblSubCourse2.text = dic["dishName"] as? String
            //btnEditSubCourse2.tag = 3
            showPlateAddedSuccessfullyMsg(dic["dishName"] as! String, category: "Sub Course")
            createPlateModel.subCourse2 = dic
            print(createPlateModel.subCourse2)
        }
        
        print(createPlateModel)
        hideDishesTableView()
        
    }
    
    func showPlateAddedSuccessfullyMsg(plate:String,category:String){
        self.showSuccessMessage("\(plate) is added as \(category)")
    }
    
    func hideDishesTableView(){
        tableView.hidden = true
        txtFldSearch.text = ""
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- UITextFieldDelegate Method
    func textField(textField: UITextField, shouldChangeCharactersInRange range:NSRange, replacementString string: String) -> Bool {
        var dishName:NSString = textField.text! as NSString
        dishName = dishName.stringByReplacingCharactersInRange(range, withString: string)
        dishNameStr = dishName as String
        getDishesListFromServer(dishName as String)
        return true
        
        
        
    }
    //***************** Search Dishes *****************
    func getDishesListFromServer(dishName:String)
    {
        var dishCategory = DishCategoryCourse.MAIN_COURSE.description
        if !(flagDishCategory == DishCategoryCourse.MAIN_COURSE.description){
            dishCategory = DishCategoryCourse.SUB_COURSE.description
        }
        
        let parameters = [
            "dishName": dishName,
            "dishCategory" : dishCategory
            
        ]
        
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Find_Dish), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            
            self.dishesArray = (response["dishes"] as? NSArray)!
            
            self.tableView .reloadData()
            self.tableView.hidden = false
            
            
            }) { (errorMessage) -> () in
                
                
        }
        
        
    }
    
    /**
     Method use to decrese number of plates
    */
    func decreaseButtonAction(sender: UIButton) {
        let cell = sender.superview?.superview?.superview as! HMCEditPlateCell
        print(cell)
        
        
        var count = Int(cell.lblPlateCount.text!)
        if count != 1{
            count = count! - 1
            cell.lblPlateCount.text = String(count!)
            createPlateModel.numberOfPlate = cell.lblPlateCount.text!
        }
        
    }
    /**
     Method use to increase number of plates
     */
    func increaseButtonAction(sender: UIButton) {
        let cell = sender.superview?.superview?.superview as! HMCEditPlateCell
        var count = Int(cell.lblPlateCount.text!)
        if count != 10{
            count = count! + 1
            cell.lblPlateCount.text = String(count!)
            createPlateModel.numberOfPlate = cell.lblPlateCount.text!
        }
        
    }
    @IBAction func backButtonAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    /**
     Method use to Submit(Create) new plates
    */
    @IBAction func submitButtonAction(sender: AnyObject) {
        
        if (createPlateModel.fromWhereStr == PostPlateCheck.RECOMMENDATION.description || createPlateModel.fromWhereStr == PostPlateCheck.PREVIOUS_PLATE.description) && !createPlateModel.flag{
            submitPlateForChef()
            return
            
        }
        if !HMCValidation.validateCreateNewPlate() {
            return
        }
        self.view.showLoader()
        let dishesArr = NSMutableArray()
        dishesArr.addObject(["dishCode":createPlateModel.mainCourse["dishCode"]!])
        dishesArr.addObject(["dishCode":createPlateModel.subCourse1["dishCode"]!])
        if createPlateModel.subCourse2.count != 0 {
            dishesArr.addObject(["dishCode":createPlateModel.subCourse2["dishCode"]!])
        }
        
        let parameter = [
            "chefMobileNumber" : HMCUtlities.getUserContactNumber(),
            "plateName": createPlateModel.completeCourseName,
            "dishes": dishesArr,
            "numberOfPlates": createPlateModel.numberOfPlate
            
        ]
        print(parameter)
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Create_Plate), parameter: parameter, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            self.createPlateModel.setInitialValue()
            self.popToViewController(HMCYourPlatesViewController)
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
                
        }
        
        
        
    }
    
    func submitPlateForChef(){
        
        let parameter = [
            "mobileNumber" : HMCUtlities.getUserContactNumber(),
            "plateCode": createPlateModel.plateCode,
            "numberOfPlates": createPlateModel.numberOfPlate
            
        ]
        self.view.showLoader()
        print(parameter)
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Submit_Plate), parameter: parameter, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            self.view.hideLoader()
            print(response)
            self.createPlateModel.setInitialValue()
            self.popToViewController(HMCYourPlatesViewController)
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
                
        }
        
    }
    
    func setFocusOnEditMainCourse(){
        hideDishesTableView()
        self.lblMainCourse.font = UIFont(name:"OpenSans-Semibold", size: 14.0)
        self.lblSubCourse1.font = UIFont(name:"OpenSans", size: 14.0)
        self.lblSubCourse2.font = UIFont(name:"OpenSans", size: 14.0)
        self.flagDishCategory = DishCategoryCourse.MAIN_COURSE.description
    }
    func setFocusOnEditSubCourse1(){
        hideDishesTableView()
        self.lblSubCourse1.font = UIFont(name:"OpenSans-Semibold", size: 14.0)
        self.lblMainCourse.font = UIFont(name:"OpenSans", size: 14.0)
        self.lblSubCourse2.font = UIFont(name:"OpenSans", size: 14.0)
        flagDishCategory = DishCategoryCourse.SUB_COURSE1.description
    }
    func setFocusOnEditSubCourse2(){
        hideDishesTableView()
        self.lblSubCourse2.font = UIFont(name:"OpenSans-Semibold", size: 14.0)//UIFont.boldSystemFontOfSize(16.0)
        self.lblMainCourse.font = UIFont(name:"OpenSans", size: 14.0)
        self.lblSubCourse1.font = UIFont(name:"OpenSans", size: 14.0)
        flagDishCategory = DishCategoryCourse.SUB_COURSE2.description
        
    }
    /**
     Method use to set the focus to the change the Main Course
     */
    @IBAction func edtitMainCourseButtonAction(sender: AnyObject) {
        
        if btnEditMainCourrse.tag == 1{
            btnEditMainCourrse.tag = 2
            setFocusOnEditMainCourse()
            
            
        }
        if (btnEditMainCourrse.tag == 3 || createPlateModel.mainCourse.count != 0) && flagDishCategory != DishCategoryCourse.MAIN_COURSE.description{
            
            
            let alert=UIAlertController(title: "Notification", message: "We are serving \"\(createPlateModel.mainCourse["dishName"]!)\"  \n Would you like to change your course?", preferredStyle: UIAlertControllerStyle.Alert);
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                
                self.setFocusOnEditMainCourse()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                
            }));
            self.presentViewController(alert, animated: true, completion: nil);
            
            
        }
        
    }
    /**
     Method use to set the focus to the change the SubCourse1
     */
    @IBAction func edtitSubCourse1ButtonAction(sender: AnyObject) {
        
        if btnEditSubCourse1.tag == 1 || createPlateModel.subCourse1.count == 0{
            btnEditSubCourse1.tag = 2
            btnEditMainCourrse.tag = 3
            setFocusOnEditSubCourse1()
            
        }
        if (btnEditSubCourse1.tag == 3 || createPlateModel.subCourse1.count != 0) && flagDishCategory != DishCategoryCourse.SUB_COURSE1.description{
            
            let alert=UIAlertController(title: "Notification", message: "We are serving \"\(createPlateModel.subCourse1["dishName"]!)\"  \n Would you like to change your course?", preferredStyle: UIAlertControllerStyle.Alert);
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.setFocusOnEditSubCourse1()
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                
            }));
            self.presentViewController(alert, animated: true, completion: nil);
            
            
        }
        
    }
    /**
     Method use to set the focus to the change the SubCourse2
     */
    @IBAction func edtitSubCourse2ButtonAction(sender: AnyObject) {
        
        if btnEditSubCourse2.tag == 1 || createPlateModel.subCourse2.count == 0{
            btnEditSubCourse2.tag = 2
            btnEditSubCourse1.tag = 3
            btnEditMainCourrse.tag = 3
            setFocusOnEditSubCourse2()
        }
        if ( createPlateModel.subCourse2.count != 0) && flagDishCategory != DishCategoryCourse.SUB_COURSE2.description{
            
            let alert=UIAlertController(title: "Notification", message: "We are serving \"\(createPlateModel.subCourse2["dishName"]!)\"  \n Would you like to change your course?", preferredStyle: UIAlertControllerStyle.Alert);
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                self.setFocusOnEditSubCourse2()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                
            }));
            self.presentViewController(alert, animated: true, completion: nil);
            
            
        }
        
    }
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
