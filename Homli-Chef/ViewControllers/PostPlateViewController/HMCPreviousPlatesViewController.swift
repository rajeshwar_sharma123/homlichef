//
//  HMCPreviousPlatesViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/21/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCPreviousPlatesViewController: HMCBaseViewController {
    var dishArray = NSArray()
    var plateListArr = [HMCPreviousPlateModel]()
    var detailList = [String]()
    var noOfplate = [String]()
    var dic : NSDictionary?
    var noDataView : HMCNoDataView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("It seems that you haven't created any plates earlier", description: "Lets create a New One")
        self.tableView.backgroundView = noDataView
        settable()
        self.title = "PREVIOUS PLATES"
        getPreviousPlateListFromServer()
       
    }
    /**
     Calling Api to get Previous Plate List
     */
    
    func getPreviousPlateListFromServer()
    {
        self.view.showLoader()
        
        let parameters = [
            "chefMobileNumber": HMCUtlities.getUserContactNumber()
            
        ]
        
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Previous_Plates), parameter: parameters, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            
            print(response)
            self.plateListArr = HMCPreviousPlateModel.bindDataWithModel(response["plates"] as! NSArray)
            self.tableView.reloadData()
            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HMCPreviousPlatesViewController:  UITableViewDelegate, UITableViewDataSource
{
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View Delegates & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if plateListArr.count == 0{
            
            noDataView.hidden = false
        }
        else{
            noDataView.hidden = true
        }

        return plateListArr.count
    }
   
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let previousPlate = plateListArr[indexPath.row]
        
        let cell = tableView.dequeueReusableCellWithIdentifier("HMCPreviousPlatesTableViewCell", forIndexPath: indexPath) as! HMCPreviousPlatesTableViewCell
        cell.selectionStyle = .None
        if plateListArr.count == 0
        {
            cell.detailOfReceipe.text = "detail"
            
        }
        else
        {
            cell.detailOfReceipe.text = previousPlate.plateName
            cell.nameOfReceipe.text = getDishName(previousPlate)
            
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let previousPlate = plateListArr[indexPath.row]
        print(previousPlate)
        
       let createPlateModel  = HMCCreatePlateModel.sharedInstance()
       createPlateModel.bindPreviousPlateDataWithModel(previousPlate)
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return 65
    }
    func getDishName(previousPlate:HMCPreviousPlateModel) -> String{
        for dishDic in previousPlate.dishes {
             let dishDic = dishDic as! NSDictionary
            
            if (dishDic["dishCategory"] as! String == "MAIN_COURSE") {
                return dishDic["dishName"] as! String
            }
            
        }
        return ""
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}


