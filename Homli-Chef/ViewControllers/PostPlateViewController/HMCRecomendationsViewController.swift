//
//  HMCRecomendationsViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/20/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCRecomendationsViewController: UIViewController {
    var derList = NSArray()
    var detailList = [HMCRecommendationListModel]()
    var noOfplate = [String]()
    var dic : NSDictionary?
    let createPlateModel  = HMCCreatePlateModel.sharedInstance()
    var noDataView : HMCNoDataView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.removeEmptyRows()
        settable()
        self.title = "RECOMMENDATIONS"
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("No recommendations available", description: "")
        self.tableView.backgroundView = noDataView
        getRecommendedListFromServer()
        self.hideDefaultBackButton()
        
    }
    @IBAction func skipTapGestureAction(sender: AnyObject) {
        self.createPlateModel.setInitialValue()
        navigateToPostPlateViewController()
        
    }
    /**
     Calling api to get Plates recommended list
     */
    func getRecommendedListFromServer(){
        
        self.view.showLoader()
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Recommendation)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
            let dic = responseData as! NSDictionary
            
            self.detailList = HMCRecommendationModel.bindDataWithModel(dic["recommendations"] as! NSArray)
            self.tableView.reloadData()
            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
        
    }
    
    /**
     Method use to navigate PostPlateViewController
     */
    func navigateToPostPlateViewController(){
        
        let postPlateViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCPostPlateViewController") as! HMCPostPlateViewController
        self.navigationController?.pushViewController(postPlateViewController, animated: true)
    }
    /**
     Calling api to get Plate Details
    */
    func getPlateDetail(plateCode:String,numberOfPlate:String){
        
        self.view.showLoader()
        
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Plate_Detail)+"/\(plateCode)"
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
            let responseData = responseData as! NSDictionary
            let dic = responseData["plate"] as! NSDictionary
            print(dic)
            
            self.createPlateModel.bindRecommendedDataWithModel(dic, numberOfPlate: numberOfPlate)
            
            self.navigateToPostPlateViewController()
            
            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HMCRecomendationsViewController:  UITableViewDelegate, UITableViewDataSource
{
    
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
     //MARK: Table View Delegates & DataSource Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
   
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if detailList.count == 0{
            
            noDataView.hidden = false
        }
        else{
            noDataView.hidden = true
        }
        return detailList.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let previousPlate = detailList[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("HMCRecommendationTableViewCell", forIndexPath: indexPath) as! HMCRecommendationTableViewCell
        cell.selectionStyle = .None
        if detailList.count == 0
        {
            cell.detailOfPlates.text = "detail"
            
        }
        else
        {
            cell.detailOfPlates.text = previousPlate.plateName
            print(previousPlate.numberOfPlates)
            cell.noOfPlates.text = previousPlate.numberOfPlates
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let recommendedPlate = detailList[indexPath.row]
        self.getPlateDetail(recommendedPlate.plateCode!,numberOfPlate: recommendedPlate.numberOfPlates!)
        
       
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}


