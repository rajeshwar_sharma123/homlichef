//
//  HMCYourPlatesViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/20/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
class HMCYourPlatesViewController: HMCBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var plateListArr = [HMCYourPlateModel]()
    var noDataView : HMCNoDataView!
    var dic : NSDictionary?
    var selectedIndexPath : NSIndexPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "YOUR PLATES"
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("No Plates uploaded today", description: "Start COOKING")
        self.tableView.backgroundView = noDataView
        tableView.removeEmptyRows()
        settable()
        self.setBackButton()
        setSideMenuPanModeNone()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        
        getChefPlateInventoryListFromServer()
        
    }
    
    
    @IBAction func addMoreTapGestureAction(sender: AnyObject) {
        
        let recomendationsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCRecomendationsViewController") as! HMCRecomendationsViewController
        self.navigationController?.pushViewController(recomendationsViewController, animated: true)
        
    }
    /**
     Calling api to get ChefPlateInventoryList
     */
    func getChefPlateInventoryListFromServer()
    {
        self.view.showLoader()
        let parameters = [
            "chefMobileNumber": HMCUtlities.getUserContactNumber()
        ]
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Chef_Plate_Inventory), parameter: parameters, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            if !(response["chefPlates"] is NSNull){
                self.plateListArr = HMCYourPlateModel.bindDataWithModel(response["chefPlates"] as! NSArray)
                self.tableView.reloadData()
            }
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension HMCYourPlatesViewController:  UITableViewDelegate, UITableViewDataSource
{
    
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: TableView Delegates & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if plateListArr.count == 0{
            
            noDataView.hidden = false
        }
        else{
            noDataView.hidden = true
        }
        
        return plateListArr.count
        
        
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let yourPlate = plateListArr[indexPath.row]
        let cell = tableView.dequeueReusableCellWithIdentifier("HMCYourPlatesTableViewCell", forIndexPath: indexPath) as! HMCYourPlatesTableViewCell
        cell.selectionStyle = .None
        if plateListArr.count == 0
        {
            cell.detailOfPlates.text = "detail"
            
        }
        else
        {
            cell.detailOfPlates.text = yourPlate.plateName
            cell.detail.text = yourPlate.plateName
            cell.noOfPlates.text = yourPlate.noOfPlates
            
        }
        cell.layoutIfNeeded()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let yourPlate = plateListArr[indexPath.row]
        setChooseOptionView(yourPlate)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return UITableViewAutomaticDimension
    }
    
    func setChooseOptionView(yourPlate:HMCYourPlateModel)
    {
        let chooseOptionView =  NSBundle.mainBundle().loadNibNamed("HMCYourPlatesView", owner: self, options: nil)[0] as! HMCYourPlatesView
        chooseOptionView.initWithDefaultValue(yourPlate)
        chooseOptionView.frame = self.view.bounds
        chooseOptionView.delegate = self
        self.view.addSubview(chooseOptionView)
        
    }
    
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool
    {
        
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
            let yourPlate = self.plateListArr[indexPath.row]
            self.selectedIndexPath = indexPath
            
            self.setConfimationbox(yourPlate)
        }
        delete.backgroundColor = ColorFromHexaCode(0xEBC97A)
        return [delete]
    }
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    
    
    func setConfimationbox(yourPlate:HMCYourPlateModel)
    {
        let confimationBox =  NSBundle.mainBundle().loadNibNamed("HMCConfirmCancelPlate", owner: self, options: nil)[0] as! HMCConfirmCancelPlate
        confimationBox.frame = self.view.bounds
        confimationBox.delegate = self
        confimationBox.message.text = "You want to cancel plate \"\(yourPlate.plateName!)\" from your plates list."
        self.view.addSubview(confimationBox)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension HMCYourPlatesViewController: HMCYourPlatesViewDelegate,HMCConfirmCancelPlateDelegate{
    //MARK:- HMCYourPlatesViewDelegate Method
    func pressedUpdatebutton(message: String) {
        self.tableView.reloadData()
    }
    //MARK:- HMCConfirmCancelPlateDelegate Method
    func confirmCancelPlate(check: Bool) {
        if check{
            cancelPlate()
        }
        else{
            self.tableView.reloadData()
        }
        
    }
    /**
     Method use to cancel chef plates
     */
    func cancelPlate(){
        HMCUtlities.showLoader()
        let yourPlate = self.plateListArr[self.selectedIndexPath.row]
        let parameters = [
            "chefMobileNumber": HMCUtlities.getUserContactNumber(),
            "plateCode" : yourPlate.plateCode!
            
        ]
        print(parameters)
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Cancel_Plate), parameter: parameters, successBlock: { (responseData) -> () in
            HMCUtlities.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            if response["msg"] != nil{
                
                HMCUtlities.showSuccessMessage(response["msg"] as! String)
                self.plateListArr.removeAtIndex(self.selectedIndexPath.row)
                self.tableView.reloadData()
                
            }
            else
            {
                
                HMCUtlities.showErrorMessage(response["errorMessage"] as! String)
            }
            }) { (errorMessage) -> () in
                HMCUtlities.hideLoader()
                
        }
    }
    
    
    


}
