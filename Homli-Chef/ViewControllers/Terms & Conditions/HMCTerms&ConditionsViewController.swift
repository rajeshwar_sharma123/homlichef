//
//  HMCTerms&ConditionsViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 3/31/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTerms_ConditionsViewController: HMCBaseViewController {
    
    @IBOutlet var btnCondition: UIButton!
    @IBOutlet weak var mywebViewforurl: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "TERMS & CONDITIONS"
        let url = NSURL (string: Url_TnC);
        setBackButton()
        let requestObj = NSURLRequest(URL: url!);
        mywebViewforurl.loadRequest(requestObj);
        
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = false
        
        
    }
    

    //MARK:- Custom button action
    @IBAction func acceptterms(sender: AnyObject) {
        let btn : UIButton = sender as! UIButton
        
        if(btn.tag==0)
        {
            
            btn.tag = 1;
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_selected"), forState: UIControlState.Normal)
        }
        else{
            
            btn.tag = 0
            btn.setBackgroundImage(UIImage(named:"icn_checkbox_unselected"), forState: UIControlState.Normal)
            
        }
        
    }
    /**
     Method to accept terms & condition
     */
      @IBAction func acceptTermsConditionBtn(sender: AnyObject) {
        
        if btnCondition.tag == 0{
            self.showErrorMessage("Please accept Terms & Conditions")
        }
        else
        {
            self.view.showLoader()
            let parameters  = [
                "chefMobileNumber" : HMCUtlities.getUserContactNumber(),
                "termsAccepted" : "true"
            ]
            print(parameters)

            HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Accept_TnC), parameter: parameters, successBlock: { (responseData) -> () in
                
                let response : NSDictionary = responseData as! NSDictionary
                self.view.hideLoader()
                print(response)
                HMCUtlities.setTNCStatus(true)
                
                let questionnaireViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCQuestionnaireViewController") as? HMCQuestionnaireViewController
                self.navigationController?.pushViewController(questionnaireViewController!, animated: true)
                
                }) { (errorMessage) -> () in
                    
                    self.view.hideLoader()
                    self.showErrorMessage(errorMessage.localizedDescription)
                    
                    
            }
            

        }
   
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
