//
//  HMCSignInViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 25/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
import RangeSliderView
class HMCSignInViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var MobileNo: HMCTextField!
    @IBOutlet weak var message: UILabel!
    
    var myMutableString = NSMutableAttributedString()
    override func viewDidLoad() {
        super.viewDidLoad()
        /**
         *  Check user current status, if it is active then navigate to HomeViewController
         */
        if (HMCUtlities.getUserCurrentStatus() == ChefStatus.ACTIVE.description) {
            HMCUtlities.sharedInstance.navigateToHomeViewController()
            return
        }
        if (HMCUtlities.getUserStatus() == ChefStatus.ACTIVE.description) {
            self.view.showLoader()
            checkUserStatus()
        }

        message.setTwoTypesAttributedString("Have an account with us ?", subText: "with us ?", fontFamilyForTxtName: "OpenSans", fontFamilyForSubTxtName: "OpenSans-Semibold", fontSizeTxtName: COMMON_SIZE, fontSizeSubTxtName: COMMON_SIZE)
        
        MobileNo.addButtonOnLeftWithImageName("ic_signin_mobile")
        self.navigationController?.navigationBarHidden = true
        self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
        MobileNo.text = HMCUtlities.getUserContactNumber()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController!.navigationBarHidden = true
        
    }
    @IBAction func Helpbutton(sender: AnyObject) {
        
        let view =  NSBundle.mainBundle().loadNibNamed("HMCHelpSignInView", owner: self, options: nil)[0] as! UIView
        view.frame = self.view.frame
        self.view.addSubview(view)
        
        
    }
    
    /**
     This method perform the SignIn process
     */
    @IBAction func signInButton(sender: AnyObject) {
        
        if(!HMCValidation.validateSignIn(self.MobileNo.text!))
        {
            return
        }
        self.view.showLoader()
        
        let parameters = [
            "mobileNumber": self.MobileNo.text!,
            
        ]
    HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_SignIn), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            print(response)
            let chefStatus = response["chefStatus"] as? String
            let lastLoginContact = HMCUtlities.getUserContactNumber()
            HMCUtlities.saveUserContactNumber(self.MobileNo.text!)
            let currentLoginContact = HMCUtlities.getUserContactNumber()
            if chefStatus == ChefStatus.NOT_REGISTERED.description{
                HMCUtlities.setUserCurrentStatus(ChefStatus.REGISTRATION_UNDER_REVIEW.description)
                self.view.hideLoader()
                HMCUtlities.setUserBasicCheckWithDefaultInitialValue()
                let signUpViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCSignUpViewController") as! HMCSignUpViewController
                print(signUpViewController)
                self.navigationController?.pushViewController(signUpViewController, animated: true)
                
            }
            else if chefStatus == ChefStatus.REGISTRATION_UNDER_REVIEW.description{
                self.view.hideLoader()
                HMCUtlities.setUserBasicCheckWithDefaultInitialValue()
                HMCUtlities.sharedInstance.navigateToHomeViewController()
            }
            else if chefStatus == ChefStatus.ACTIVE.description{
                HMCUtlities.setUserStatus(ChefStatus.ACTIVE.description)
                
                if (lastLoginContact != currentLoginContact || lastLoginContact == ""){
                    HMCUtlities.setUserBasicCheckWithDefaultInitialValue()
                }
                
                self.checkUserStatus()
            }
        }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
           
        }
        
        
    }
    /**
     Use to check user current status
     */
    func checkUserStatus()
    {
        if !HMCUtlities.isOTPVerified() {
            self.checkUserOTPStatus()
        }
        else
        {
            if !HMCUtlities.isTermsNConditionAccepted() {
                self.checkUserTermsNConditionStatus()
            }
            else
            {
                if !HMCUtlities.isQNASubmitted() {
                    self.checkUserQuestionnarieStatus()
                }
                else
                {
                    HMCUtlities.setUserCurrentStatus(ChefStatus.ACTIVE.description)
                    self.view.hideLoader()
                    HMCUtlities.sharedInstance.navigateToHomeViewController()
                }
            }
        }
        
    }
    
    /**
     Check user has complted OTP verification or not
     */
    func checkUserOTPStatus()
    {
    let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Check_OTP_Status)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            
            print(responseData)
            let responseData = responseData as! NSDictionary
            var statusOTP = Bool()
            if (responseData["statusOTP"] is NSNull) {
                statusOTP = false
                
            }else
            {
                statusOTP = responseData["statusOTP"] as! Bool
            }
            if statusOTP {
                HMCUtlities.setOTPStatus(true)
                self.checkUserTermsNConditionStatus()
            }
            else
            {
                self.view.hideLoader()
                let verificationViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCOTPVerificationViewController") as? HMCOTPVerificationViewController
                self.navigationController?.pushViewController(verificationViewController!, animated: true)
            }
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
        
    }
    /**
     Check user has accepted terms & condition or not
     */
    func checkUserTermsNConditionStatus()
    {
        
    let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Check_TnC_Status)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            print(responseData)
            let responseData = responseData as! NSDictionary
            let success : Bool = responseData["isTnCAccepted"] as! Bool
            if success {
                HMCUtlities.setTNCStatus(true)
                self.checkUserQuestionnarieStatus()
            }
            else
            {
                self.view.hideLoader()
                let terms_ConditionsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCTerms_ConditionsViewController") as? HMCTerms_ConditionsViewController
                self.navigationController?.pushViewController(terms_ConditionsViewController!, animated: true)
                
            }
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
        
    }
    /**
     Check user has filled Questionnaries Part or not
     */
    func checkUserQuestionnarieStatus()
    {
        
        let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Check_QnA_Status)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            
            print(responseData)
            let responseData = responseData as! NSDictionary
//            if (responseData["error"] == nil){
//                
//                return
//            }
            var statusChefQnA = Bool()
            
            if (responseData["chefQnA"] is NSNull) {
                statusChefQnA = false
                
            }else
            {
                statusChefQnA = true
            }
            
            if statusChefQnA {
                HMCUtlities.setQNAStatus(true)
                self.view.hideLoader()
                HMCUtlities.setUserCurrentStatus(ChefStatus.ACTIVE.description)
                HMCUtlities.sharedInstance.navigateToHomeViewController()
                
            }
            else
            {
                self.view.hideLoader()
                let questionnaireViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCQuestionnaireViewController") as? HMCQuestionnaireViewController
                self.navigationController?.pushViewController(questionnaireViewController!, animated: true)
            }
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
        
    }
    //MARK:- UITextFieldDelegate Method
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 10 // Bool
    }
    
    func navigateToWelcomeScreen()
    {
        let welcomeScreenViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCWelcomeScreenViewController") as! HMCWelcomeScreenViewController
        self.navigationController?.pushViewController(welcomeScreenViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
