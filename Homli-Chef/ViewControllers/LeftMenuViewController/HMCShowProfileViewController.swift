//
//  HMCShowProfileViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/12/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import SDWebImage
//import ImageScrollView
class HMCShowProfileViewController: UIViewController ,UIScrollViewDelegate{

    var userImg : UIImage!
    
    @IBOutlet var scrollView: ImageScrollView!
    enum ErrorHandling:ErrorType
    {
        case NetworkError
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let response = HMCUtlities.getSignInUserDetail()
        
        if response.count == 0{
            return
        }
        if (response["firstName"] != nil) {
            
            self.title = "\(response["firstName"])"
        }
        else
        {
        self.title = "User Name"
        }
        setUserImage()
        self.view.backgroundColor = ColorFromHexaCode(k_Light_Gray_Color)
    }
    
    
    func setUserImage()
    {
        scrollView.displayImage(userImg)
        
    }
    

   }
