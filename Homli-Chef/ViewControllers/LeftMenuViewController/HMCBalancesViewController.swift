//
//  HMCBalancesViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/1/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCBalancesViewController: HMCBaseViewController{

    var balanceList = [:]
    var detailArray = [String]()
    var detailPrize = [String]()

    @IBOutlet var tableSubView: UIView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var contactNumberView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contactNumberLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "BALANCES"
        setEffectOfLabel()
        getChefBalanceListFromServer()
        settable()
        setBackButton()
        setSideMenuPanModeNone()
        
      }
    
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = false
        
    }
    /**
     Set the Shadow effect on contolls
     */
    func setEffectOfLabel()
    {
        
        tableSubView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tableSubView.layer.shadowOpacity = 0.3
        tableSubView.layer.shadowRadius = 2
        tableSubView.layer.masksToBounds = false
        tableSubView.layer.cornerRadius = 5
        
        contactNumberView.layer.shadowOffset = CGSize(width: 0, height: 0)
        contactNumberView.layer.shadowOpacity = 0.3
        contactNumberView.layer.shadowRadius = 2
        contactNumberView.layer.masksToBounds = false
        contactNumberView.layer.cornerRadius = 5

        
    }
    /**
     Calling api to get Chef Balance Detail
     */
    func getChefBalanceListFromServer()
    {
        let vc = HMCUtlities.getTopViewController()
        //vc.view.showLoader()
        
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Balance_List) + "/" + HMCUtlities.getUserContactNumber()
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
          
        print(responseData)
          self.setChefBalancedata(responseData as! NSDictionary)
            
           
        }) { (errorMessage) -> () in
                vc.view.hideLoader()
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
        }
    }
    func setChefBalancedata(response: NSDictionary)
    {
        let no = HMCUtlities.getUserContactNumber()
        contactNumberLabel.text = String(no)
        contactLabel.text = "Contact Number"
        for (k,v) in response{
            print("Key : \(k) Value \(v) \n")
            
             if (!(k.isEqual("mobileNo")))
             {
                print(k)
            self.detailArray.append(k as! String)
                if v is NSNull{
                    self.detailPrize.append(String(0))
                }
                else
                {
                
                self.detailPrize.append(String(v))
                }
            }
        }
  
        self.tableView.reloadData()
        
       
    }
}
extension HMCBalancesViewController:  UITableViewDelegate, UITableViewDataSource
{
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        

    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if  self.detailArray.count == 0
        {
            return 0
        }
        else
        {
            return self.detailArray.count
        }
    }
    
    //MARK: Table View Data
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! HMCBalanceTableViewCell
        if detailArray.count == 0
        {
            cell.detailLabel.text = "detail"
            
        }
        else
        {
            cell.detailLabel.text = self.detailArray[indexPath.row].capitalizedString
            cell.prizeLabel.text = "Rs. \(self.detailPrize[indexPath.row]) .00"
        }
        return cell
        
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 20
        
        
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
}
