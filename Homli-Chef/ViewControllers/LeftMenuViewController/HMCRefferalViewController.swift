//
//  HMCRefferalViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 3/30/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCRefferalViewController: HMCBaseViewController {
    @IBOutlet var btnInviteAction: UIButton!
    
    @IBOutlet var lblReferralCode: UILabel!
    var referralStr = ""
    var referralCheck = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "REFERRAL"
        setBackButton()
        // Do any additional setup after loading the view.
        setSideMenuPanModeNone()
        getReferralCode()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = false
        
    }
    /**
     Invite button Action
    */
    @IBAction func inviteButtonAction(sender: AnyObject) {
    
        let activityViewController = UIActivityViewController(
            activityItems: [referralStr],
            applicationActivities: nil)
        
        presentViewController(activityViewController, animated: true, completion: nil)
        
        
    }
    /**
     Calling Api to get referral code
     */
    func getReferralCode()
    {
        self.view.showLoader()
        let urlString : String = HMCServiceUrls.getCompleteUrlFor(Url_Referral_Code)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlString, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            print(responseData)
             let responseData = responseData as! NSDictionary
            var referralCode = "No invite code"
            
            if (!(responseData["referralCode"] is NSNull)) {
               
                referralCode = responseData["referralCode"] as! String
                self.referralStr =   "Use my invite code \(responseData["referralCode"]), and start earning as a home chef. Sign up now at  \(responseData["referralLink"])"
            }else
            {
                
                self.btnInviteAction.userInteractionEnabled = false
                
            }
            
            self.lblReferralCode.text = referralCode
            
            }) { (errorMessage) -> () in
                HMCUtlities.showErrorMessage(errorMessage.localizedDescription)
                self.view.hideLoader()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
