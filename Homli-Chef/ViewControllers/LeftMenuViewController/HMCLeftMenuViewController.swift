//
//  HMCLeftMenuViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 28/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
class HMCLeftMenuViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var menuName =  []
    
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        menuName = ["Balances", "Profile", "Referrals","Contact Us","Help","Signout"]
        self.view.backgroundColor = ColorFromHexaCode(k_Side_Menu_Color)
        userImgView.layer.cornerRadius = userImgView.frame.size.height/2
        userImgView.clipsToBounds = true
        userImgView.layer.borderWidth = 3
        userImgView.layer.borderColor = ColorFromHexaCode(k_Light_OrangeColor).CGColor
        
      
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver( self,selector: #selector(HMCLeftMenuViewController.menuStateEventOccurred(_:)),name: MFSideMenuStateNotificationEvent, object: nil)
        
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: MFSideMenuStateNotificationEvent, object: nil)
    }
    
    
    /**
     This method get call whenever side menu open or close
     */
    func menuStateEventOccurred(notification: NSNotification){
        
        if notification.name == MFSideMenuStateNotificationEvent {
            
           dispatch_async(dispatch_get_main_queue()) { [unowned self] in
            
            let userInfo = HMCUtlities.getSignInUserDetail()
            if userInfo.count != 0{
            let url = NSURL(string: userInfo["profilePic"] as! String)
            self.userImgView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "icn_default"))
            }
            self.userName.text = HMCUtlities.getUserFirstnLastName()
            }
            let userStatus =  HMCUtlities.getUserCurrentStatus()
            if (!(userStatus == ChefStatus.ACTIVE.description)){
                tableView.reloadData()
            }
            else
            {
            tableView.reloadData()
            }
        }
    }
    //MARK- TableView Delegates & DataSource Method
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuName.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        let cell : HMCLeftMenuCell = tableView.dequeueReusableCellWithIdentifier("HMCLeftMenuCell", forIndexPath: indexPath) as! HMCLeftMenuCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.imgView.image = getIconForSideMenuOption(atIndex: indexPath.row)
        cell.imgView.setImageColorFromHexaCode(k_Side_Menu_Default_Color)
        cell.lblName.text = menuName[indexPath.row] as? String
        cell.lblName.textColor = ColorFromHexaCode(k_Side_Menu_Default_Color)
        
        if indexPath.row <= 3{
        let userStatus =  HMCUtlities.getUserCurrentStatus()
        if (!(userStatus == ChefStatus.ACTIVE.description)){
            
            cell.imgView.setImageColorFromHexaCode(k_Side_Menu_InActive)
            cell.lblName.textColor = ColorFromHexaCode(k_Side_Menu_InActive)
        }
        }
        
        return cell
    }
    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell : HMCLeftMenuCell = tableView.cellForRowAtIndexPath(indexPath) as! HMCLeftMenuCell
        
        if indexPath.row <= 3{
            let userStatus =  HMCUtlities.getUserCurrentStatus()
            if (!(userStatus == ChefStatus.ACTIVE.description)){
                return
            }
        }
        cell.imgView.setImageColorFromHexaCode(k_Light_OrangeColor)
        cell.lblName.textColor = ColorFromHexaCode(k_Light_OrangeColor)
        
    }
    
    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
        let cell : HMCLeftMenuCell = tableView.cellForRowAtIndexPath(indexPath) as! HMCLeftMenuCell
        if indexPath.row <= 3{
            let userStatus =  HMCUtlities.getUserCurrentStatus()
            if (!(userStatus == ChefStatus.ACTIVE.description)){
                return
            }
        }

        cell.imgView.setImageColorFromHexaCode(k_Side_Menu_Default_Color)
        cell.lblName.textColor = ColorFromHexaCode(k_Side_Menu_Default_Color)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        
        return 60
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var viewController = UIViewController()
        if indexPath.row <= 3{
            let userStatus =  HMCUtlities.getUserCurrentStatus()
            if (!(userStatus == ChefStatus.ACTIVE.description)){
                return
            }
        }

        switch(indexPath.row)
        {
        case 0 :
            
            viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMCBalancesViewController") as? HMCBalancesViewController)!
            self.navigationController?.pushViewController(viewController, animated: true)
            
        case 1 :
            viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMCProfileViewController") as? HMCProfileViewController)!
            self.navigationController?.pushViewController(viewController, animated: true)
            
            
        case 2 :
            viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMCRefferalViewController") as? HMCRefferalViewController)!
            self.navigationController?.pushViewController(viewController, animated: true)
        case 3 :
            viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMCContactusViewController") as? HMCContactusViewController)!
            self.navigationController?.pushViewController(viewController, animated: true)
            
        case 4 :
            viewController = (self.storyboard!.instantiateViewControllerWithIdentifier("HMCHelpViewController") as? HMCHelpViewController)!
            self.navigationController?.pushViewController(viewController, animated: true)
        case 5 :
            showAlertView()
            return
        default :
            print("")
            
        }
        self.menuContainerViewController.centerViewController.pushViewController(viewController, animated: true)
        self.toggleLeftSide()
        
    }
    
    /**
     Method to get image for side menu option
     */
    func getIconForSideMenuOption(atIndex index:NSInteger) -> UIImage
    {
        
        var icnName = String()
        
        switch(index)
        {
        case 0 :
            icnName = "ic_slidemenu_balances"
        case 1 :
            icnName = "ic_slidemenu_profile"
            
        case 2 :
            icnName = "icn_slidemenu_referral"
        case 3 :
            icnName = "icn_slidemenu_contact"
        case 4 :
            icnName = "ic_help"
        case 5 :
            icnName = "icn_slidemenu_logout"
        default :
            print("")
            
        }
        
        
        return UIImage(named: icnName)!
        
    }
    /**
     This method is use to show sign out alert view
     */
    func showAlertView()
    {
        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
            
            
            
            let alert=UIAlertController(title: "Confirm Sign Out", message: "\(HMCUtlities.getUserFirstnLastName()), you are signing out of Homli app on this device.", preferredStyle: UIAlertControllerStyle.Alert);
            
            //no event handler (just close dialog box)
            alert.addAction(UIAlertAction(title: "Signout", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
                
                HMCUtlities.signOutFromApp()
                let signInViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCSignInViewController") as? HMCSignInViewController
                self.menuContainerViewController.centerViewController = UINavigationController(rootViewController: signInViewController!)
                self.toggleLeftSide()
                
            }))
            //event handler with closure
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: {(action:UIAlertAction) in
                
                self.closeSideMenu()
                
                
            }));
            self.presentViewController(alert, animated: true, completion: nil);
            
        }
    }
    /**
     This method is use to close side menu
     */
    func closeSideMenu()
    {
        self.menuContainerViewController.setMenuState(MFSideMenuStateClosed, completion: { () -> Void in
            
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
