//
//  HMCContactusViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 3/30/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCContactusViewController: HMCBaseViewController,UITextViewDelegate {
    @IBOutlet weak var nameTextField: HMCTextField!
    @IBOutlet weak var mobileNoTextField: HMCTextField!
    @IBOutlet weak var emailTextField: HMCTextField!
    @IBOutlet weak var messageTextView: UITextView!
    override func viewDidLoad() {
        self.title = "CONTACT US"
        messageTextView.delegate = self
        super.viewDidLoad()
        setBackButton()
        messageTextView.layer.cornerRadius = 25
        messageTextView.layer.borderWidth = 2
        messageTextView.layer.borderColor = UIColor.whiteColor().CGColor
        messageTextView.text = "Message"
        messageTextView.textContainerInset = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 0)
        nameTextField.text = HMCUtlities.getUserFullName()
        let name = HMCUtlities.getUserFullName()
        if !name.isEmpty{
            nameTextField.userInteractionEnabled = false
        }
        mobileNoTextField.text = HMCUtlities.getUserContactNumber()
        setSideMenuPanModeNone()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = false
        
    }
    func textViewDidBeginEditing(textView: UITextView) {
        textView.text = ""
    }
    func textViewDidEndEditing(textView: UITextView) {
        if(textView.text == "") {
            textView.text = "Message"
        }
        
    }
    /**
     Method use to submit user views
     */
    @IBAction func submitButtonAction(sender: AnyObject) {
        
        if (!HMCValidation.validateContactUs(messageTextView.text!,email: emailTextField.text!))
        {
            return
        }
        
        self.view.showLoader()
        let parameters  = [
            "mobileNumber" : HMCUtlities.getUserContactNumber(),
            "message" : messageTextView.text!,
            "emailId" : emailTextField.text!
        ]
        print(parameters)
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_ContactUs_Query), parameter: parameters, successBlock: { (responseData) -> () in
            
            let response : NSDictionary = responseData as! NSDictionary
            self.view.hideLoader()
            print(response)
            
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
}
