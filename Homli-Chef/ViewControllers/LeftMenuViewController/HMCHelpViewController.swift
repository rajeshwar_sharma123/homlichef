//
//  HMCHelpViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/1/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCHelpViewController: HMCBaseViewController {

    @IBOutlet weak var webviewofHelp: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL (string: Url_Help);
        let requestObj = NSURLRequest(URL: url!);
        webviewofHelp.loadRequest(requestObj);
        self.title = "HELP"
        setSideMenuPanModeNone()
        setBackButton()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = false
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
