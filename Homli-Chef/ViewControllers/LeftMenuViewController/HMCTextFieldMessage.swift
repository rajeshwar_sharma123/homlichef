//
//  HMCTextFieldMessage.swift
//  Homli-Chef
//
//  Created by Jenkins on 3/30/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTextFieldMessage: UITextField {
   
        
        
        let padding = UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 5);
        
        override func textRectForBounds(bounds: CGRect) -> CGRect {
            return self.newBounds(bounds)
        }
        
        override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
            return self.newBounds(bounds)
        }
        
        override func editingRectForBounds(bounds: CGRect) -> CGRect {
            return self.newBounds(bounds)
        }
        
        private func newBounds(bounds: CGRect) -> CGRect {
            
            var newBounds = bounds
            newBounds.origin.x += padding.left
            newBounds.origin.y += padding.top
            newBounds.size.height -= padding.top + padding.bottom
            newBounds.size.width -= padding.left + padding.right
            return newBounds
        }
        
}
