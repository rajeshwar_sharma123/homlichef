//
//  HMCProfileViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/4/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import Cosmos
class HMCProfileViewController: HMCBaseViewController,HMCTopTenDishesViewDelegate,HMCProfileEditViewDelegate {
    var dishesTag : ANTagsView!
    var dishNameArr = NSMutableArray()
    var dishesArr = NSMutableArray()
    @IBOutlet weak var gradientview: UIView!
    @IBOutlet var ratingStarView: CosmosView!
    
    @IBOutlet var addressImgView: UIImageView!
    @IBOutlet var userImgView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var pinCode: UILabel!
    
    @IBOutlet weak var editYourPreferences: UIButton!
    @IBOutlet weak var type: UILabel!
    
    @IBOutlet weak var cuisins: UILabel!
    
    @IBOutlet weak var editTop10dishes: UIImageView!
    
    @IBOutlet var yourPreferencesView: UIView!
    @IBOutlet var topTenDishesView: UIView!
    
    @IBOutlet var topTenDishesHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var cuisineHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var yourPreferencesHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var addressHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var stateHeightConstraint: NSLayoutConstraint!
    @IBOutlet var pinCodeHeightConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "PROFILE"
        userImgView.layer.cornerRadius = userImgView.frame.size.height/2
        userImgView.clipsToBounds = true
        userImgView.layer.borderWidth = 3
        userImgView.layer.borderColor = UIColor.whiteColor().CGColor
        self.setBackButton()
        setSideMenuPanModeNone()
        var x : CGFloat =  55
        if screenWidth == 375{
        x = 82.5
        }
        let view: UIView = UIView(frame: CGRectMake(x, -x, 210, screenWidth))
       
        let gradient : CAGradientLayer = CAGradientLayer()
        let arrayColors: [AnyObject] = [
            ColorFromHexaCode(0x9D482C).CGColor,ColorFromHexaCode(0xA88C67).CGColor,ColorFromHexaCode(0x614F2D).CGColor
        ]
        gradient.frame = view.bounds
        gradient.colors = arrayColors
        //gradient.locations = [0.2,0.6, 1.0]
        view.layer.insertSublayer(gradient, atIndex: 0)
        view.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        gradientview.addSubview(view);
        gradientview.sendSubviewToBack(view)
        
        let profileImgTap = UITapGestureRecognizer(target: self, action: #selector(HMCProfileViewController.profileImgTapGestureAction(_:)))
        userImgView.userInteractionEnabled = true
        userImgView.addGestureRecognizer(profileImgTap)
        setChefProfiledata()
        setDishesTagView()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = false
       
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.scrollView.contentSize = CGSizeMake(screenWidth, topTenDishesView.frame.origin.y + self.topTenDishesHeightConstraint.constant)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /**
     Profile Image tap gesture action
     */
    func profileImgTapGestureAction(sender: UITapGestureRecognizer? = nil) {
        let showProfileViewController : HMCShowProfileViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCShowProfileViewController") as! HMCShowProfileViewController
        showProfileViewController.userImg = userImgView.image
        self.navigationController?.pushViewController(showProfileViewController, animated: true)
    }
    /**
     This method is use to set user profile data
     */
    func setChefProfiledata()
    {
        let response = HMCUtlities.getSignInUserDetail()
        if response.count == 0{
           return
        }
        
        let url = NSURL(string: response["profilePic"] as! String)
        userImgView.sd_setImageWithURL(url, placeholderImage: UIImage(named: "icn_default"))
        var fullName = String()
        
        if (response["firstName"] != nil) {
            
            fullName = "\(response["firstName"])"
        }
        if (response["middleName"] != nil) {
            
            fullName = fullName + "\(response["middleName"])"
        }
        if (response["lastName"] != nil) {
            
            fullName = fullName + "\(response["lastName"])"
        }
        
        
        if !fullName.isEmpty{
            name.text = fullName
        }
        let no = response["mobile"]
        mobileNumber.text = String(no!)
        var addressStr = String()
        var stateStr = String()
        var pincodeStr = String()
        if (response["address"] != nil)
        {
        let completeaddress = response["address"] as! NSDictionary
         
            addressStr = "\(completeaddress["firstLine"])"
            addressStr = addressStr + "\(completeaddress["secondLine"])"
            stateStr = "\(completeaddress["city"])"
            stateStr = stateStr + "\(completeaddress["state"])"
            pincodeStr = "\(completeaddress["pinCode"])"
        }
        
        
       if !addressStr.isEmpty{
            address.text = addressStr
        }
        else
        {
            address.hidden = true
            addressImgView.hidden = true
            addressHeightConstraint.constant = 0
        }
       if !addressStr.isEmpty{
            state.text = stateStr
        }
        else
        {
            state.hidden = true
            stateHeightConstraint.constant = 0
        }
        if !addressStr.isEmpty{
            pinCode.text = pincodeStr
        }
        else
        {
            pinCode.hidden = true
            pinCodeHeightConstraint.constant = 0
        }
        
       
        var typeStr = String()
        if (response["cookingDetails"] != nil)
        {
          let  completeDetail = response["cookingDetails"] as! NSDictionary
            let veg = completeDetail["veg"] as! Bool
            if veg {
                typeStr  = "Vegetarian,"
                
            }
            let nonVeg = completeDetail["nonVeg"] as! Bool
            if nonVeg {
                typeStr = typeStr + " Non-Vegetarian"
                
            }
             type.text = typeStr
            let cuisines = completeDetail["cuisines"] as! NSArray
            var cookingdetail = ""
            for cuisine in cuisines{
                
                cookingdetail += " \(cuisine),"
                
            }
            cookingdetail = String(cookingdetail.characters.dropLast())
            cuisins.text = cookingdetail
            let height = cookingdetail.heightWithConstrainedWidth(cuisins.frame.size.width, font: UIFont(name: "OpenSans", size: 15)!)
            cuisins.text = cookingdetail
            cuisineHeightConstraint.constant = height
            yourPreferencesHeightConstraint.constant = 76 + height

        }
        
      ratingStarView.rating =  Double(0)
    }
    
    //MARK:- Custome Button Action
    @IBAction func yourPreferencesButtonAction(sender: AnyObject) {
        
        let profileEditView =  NSBundle.mainBundle().loadNibNamed("HMCProfileEditView", owner: self, options: nil)[0] as! HMCProfileEditView
        profileEditView.delegate = self
        profileEditView.initWithDefaultValue()
        profileEditView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        self.view.addSubview(profileEditView)
        hideNavigationBar()
    }
    @IBAction func topTenDishesButtonAction(sender: AnyObject) {
        
        let topTenDishesView =  NSBundle.mainBundle().loadNibNamed("HMCTopTenDishesView", owner: self, options: nil)[0] as! HMCTopTenDishesView
        topTenDishesView.initWithDefaultValue()
        topTenDishesView.delegate = self
        topTenDishesView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
        self.view.addSubview(topTenDishesView)
        hideNavigationBar()
        
        
    }
    //MARK:- Setting top ten dishes view
    func setDishesTagView()
    {
        
        let tmpArr = HMCUtlities.getUserPreferredDishes() as NSMutableArray
        let arr = tmpArr.valueForKey("dishName") as! NSArray
        self.dishNameArr = arr.mutableCopy() as! NSMutableArray
        if self.dishesTag != nil{
            self.dishesTag.removeFromSuperview()
        }
        
        self.dishesTag = ANTagsView(tags: self.dishNameArr  as [AnyObject], frame: CGRectMake(0, 35, topTenDishesView.frame.size.width, 10))
        self.dishesTag.setTagCornerRadius(10)
        self.dishesTag.tag = 1
        self.dishesTag.setTagBackgroundColor(UIColor.clearColor())
        self.dishesTag.setTagTextColor(UIColor.blackColor())
        self.dishesTag.setTagTextFont(15)
        self.dishesTag.backgroundColor = UIColor.clearColor()
        self.dishesTag.setFrameWidth(Int32(screenWidth))
        self.dishesTag.layer.cornerRadius = 15
        self.dishesTag.clipsToBounds = true
        self.topTenDishesView.addSubview(self.dishesTag)
        self.dishesTag.setTagBackgroundShadowEffect()
         self.topTenDishesHeightConstraint.constant = self.dishesTag.frame.size.height + self.dishesTag.frame.origin.y + 30
        
        
    }

    
    //MARK:- HMCTopTenDishesViewDelegate Method
    func topTenDishesUpdated() {
        print("updated")
        setDishesTagView()
    }
    //MARK:- HMCProfileEditViewDelegate Method
    func cookingDetailsUpdated() {
        showNavigationBar()
        print("cooking details updated")
        setChefProfiledata()
    }
    
}
