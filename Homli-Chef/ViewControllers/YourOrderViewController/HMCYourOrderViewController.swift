//
//  HMCYourOrderViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 28/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
class HMCYourOrderViewController: HMCBaseViewController,UITableViewDataSource,UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let margin : CGFloat = 12
    let size : CGFloat = 40
    @IBOutlet var tableView: UITableView!
    var orderListArr = [HMCYourOrderModel]()
    var noDataView : HMCNoDataView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "YOUR ORDERS"
        registerCellWithTableView()
        getUserDetailFromService()
        noDataView = HMCNoDataView.initNoDataView(self.view)
        noDataView.initWithDefaultValue("No orders at present", description: "Check back after sometime")
        self.tableView.backgroundView = noDataView
        
      
    }
    
    
    func registerCellWithTableView(){
        self.tableView.registerNib(UINib(nibName: "HMCYourOrderCell", bundle: nil), forCellReuseIdentifier: "HMCYourOrderCell")
        self.tableView.registerNib(UINib(nibName: "HMCOrderItemTableViewCell", bundle: nil), forCellReuseIdentifier: "HMCOrderItemTableViewCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        setSideMenuPanModeDefault()
        HMCUtlities.sharedInstance.mainNC.navigationBarHidden = true
        getChefPlateOrders()
        
    }
    //MARK:- Webservice Calling
    func getUserDetailFromService()
    {
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Read_User_Detail)+"/\(HMCUtlities.getUserContactNumber())"
        
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
            
            print(responseData)
            let responseData = responseData as! NSDictionary
            let errormMsg = responseData["errorMessage"] as? String
            if(errormMsg != nil)
            {
                
            }
            else
            {
                
                let userProfile : NSDictionary = responseData 
                HMCUtlities.saveUserProfile(userProfile)
            }
            }) { (errorMessage) -> () in
        }
    }
    /**
     Calling Api to get Chef Plate Orders
     */
    func getChefPlateOrders()
    {
        self.view.showLoader()
        let urlStr = HMCServiceUrls.getCompleteUrlFor(Url_Chef_Plate_Orders) + "/\(HMCUtlities.getUserContactNumber())"
        HMCAppServices.getServiceRequest(urlString: urlStr, successBlock: { (responseData) -> () in
        print(responseData)
        let dic : NSDictionary = responseData as! NSDictionary
        self.orderListArr =   HMCYourOrderModel.bindDataWithModel(dic["orders"] as! NSArray)
        self.tableView.reloadData()
        self.view.hideLoader()
            
            }) { (errorMessage) -> () in
                
                self.view.hideLoader()
                self.showErrorMessage(errorMessage.localizedDescription)
                
                
        }
    }
    //MARK:- UITableViewDelegate Method
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if orderListArr.count == 0{
         
            noDataView.hidden = false
        }
        else{
           noDataView.hidden = true
          }
        return orderListArr.count
        
    }
    
    //MARK: Table View Delegates & Data Source Methods
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let order = orderListArr[indexPath.row]
        var cell : HMCYourOrderCell
        if order.isChild{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("HMCOrderItemTableViewCell", forIndexPath: indexPath) as! HMCOrderItemTableViewCell
            configureHMCOrderItemTableViewCell(cell, atIndexPath: indexPath)
            return cell
            
        }
        else
        {
            cell  = tableView.dequeueReusableCellWithIdentifier("HMCYourOrderCell", forIndexPath: indexPath) as! HMCYourOrderCell
            configureHMCYourOrderCell(cell, atIndexPath: indexPath)
            
        }
        
        return cell
    }
    func configureHMCOrderItemTableViewCell(cell:HMCOrderItemTableViewCell,atIndexPath indexPath:NSIndexPath){
        
        cell.backgroundColor = UIColor.whiteColor()
        cell.selectionStyle = .None
        cell.collectionView.dataSource = self
        cell.collectionView.delegate = self
        cell.collectionView.registerNib(UINib(nibName: "HMCOrderItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HMCOrderItemCollectionViewCell")
        cell.collectionView.backgroundColor = UIColor.whiteColor()
        cell.contentView.addSubview(cell.collectionView)
        cell.collectionView.reloadData()
        
    }
    
    func configureHMCYourOrderCell(cell:HMCYourOrderCell,atIndexPath indexPath:NSIndexPath){
        let order = orderListArr[indexPath.row]
        cell.selectionStyle = .None
        cell.lblDishName.text = order.plateName
        cell.lblCount.text = order.numberOfPlates
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let order = orderListArr[indexPath.row]
        
        if (!order.isExpanded && !order.isChild){
            
            expandCell(order, atIndexPath: indexPath)
        }
        else{
            collapseCell(order, atIndexPath: indexPath)
        }
    }
        func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let order = orderListArr[indexPath.row]
        if order.isChild{
            var divNum : Int = 6
            var num : Int = order.orderItemIds.count
            if screenWidth == 375{
                divNum = 7
                
            }
            if num%divNum == 0 {
                num =  num/divNum
                return CGFloat(num*52-num*2 + 16)
            }
            
            num =  num/divNum + 1
            return CGFloat(num*52-num*2 + 16)

        }
        
        return UITableViewAutomaticDimension
    }
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return 50
    }
    // MARK:- CollectionView DataSource and Delegate Method
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let cell = collectionView.superview?.superview as! HMCOrderItemTableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        print(indexPath)
        let order = orderListArr[indexPath!.row]
        let num = order.orderItemIds.count
        return num
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HMCOrderItemCollectionViewCell", forIndexPath: indexPath) as! HMCOrderItemCollectionViewCell
        cell.btnOrderItem.addTarget(self, action: #selector(HMCYourOrderViewController.buttonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        cell.btnOrderItem.tag = indexPath.row
        return cell
    }
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: size, height: size) // The size of one cell
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(margin, margin, margin, margin) // margin between cells
    }
    //Mark:- Expand and Collapse Cell
    func expandCell(order:HMCYourOrderModel,atIndexPath indexPath:NSIndexPath)
    {
        order.isExpanded = true
        let orderChild = HMCYourOrderModel()
        orderChild.orderItemIds = order.orderItemIds
        orderChild.plateCode = order.plateCode
        orderChild.plateName = order.plateName
        orderChild.numberOfPlates = order.numberOfPlates
        orderChild.isChild = true
        orderListArr.insert(orderChild, atIndex: indexPath.row+1)
        self.tableView .beginUpdates()
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: indexPath.row+1, inSection: indexPath.section)], withRowAnimation: .Automatic)
        self.tableView.endUpdates()
        
    }
    func collapseCell(order:HMCYourOrderModel,atIndexPath indexPath:NSIndexPath)
    {
        if order.isChild{
            return
        }
        
        order.isExpanded = false
        orderListArr.removeAtIndex(indexPath.row+1)
        self.tableView .beginUpdates()
        self.tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: indexPath.row+1, inSection: indexPath.section)], withRowAnimation: .Automatic)
        self.tableView.endUpdates()
        
    }
    //MARK:- Custome Button Action
    func buttonAction(sender:UIButton!)
    {
        let cell = sender.superview?.superview?.superview?.superview?.superview as! HMCOrderItemTableViewCell
        let indexPath = self.tableView.indexPathForCell(cell)
        let order = orderListArr[indexPath!.row]
        let dic = order.orderItemIds[sender.tag] as! NSDictionary
        print(sender.tag)
        
        let orderDetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("HMCOrderDetailViewController") as? HMCOrderDetailViewController
        orderDetailViewController?.plateCode = order.plateCode!
        orderDetailViewController?.orderItemId =  "\(dic["orderItemId"]!)"
        HMCUtlities.sharedInstance.mainNC.pushViewController(orderDetailViewController!, animated: true)
    }
    
        
}
