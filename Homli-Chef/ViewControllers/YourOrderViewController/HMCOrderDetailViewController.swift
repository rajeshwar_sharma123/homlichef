//
//  HMCOrderDetailViewController.swift
//  Homli-Chef
//
//  Created by Jenkins on 4/25/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCOrderDetailViewController: HMCBaseViewController {
    
    var plateCode : String!
    var orderItemId : String!
    var derList = NSArray()
    var detailList = [HMCRecommendationListModel]()
    var noOfplate = [String]()
    var dic = [:]
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackButton()
        settable()
        self.title = "ORDER DETAILS"
        print(dic)
        getOrderDetailFromServer()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = false
        
    }
    /**
     Calling Api to get Order Detail
     */
    func getOrderDetailFromServer()
    {
        self.view.showLoader()
        
        let parameters = [
            "chefMobile": HMCUtlities.getUserContactNumber(),
            "plateCode" : plateCode,
            "orderItemId" : orderItemId
            
        ]
        print(parameters)
        
        HMCAppServices.postServiceRequest(urlString: HMCServiceUrls.getCompleteUrlFor(Url_Order_Details), parameter: parameters, successBlock: { (responseData) -> () in
            self.view.hideLoader()
            let response : NSDictionary = responseData as! NSDictionary
             print(response)
            if response["errorMessage"] != nil{
              self.showErrorMessage(response["errorMessage"] as!
                String)
            }
            else{
            self.dic = response
            self.tableView.reloadData()
            }
            
            
            }) { (errorMessage) -> () in
                self.view.hideLoader()
                
        }
        
        
    }
    
  override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension HMCOrderDetailViewController:  UITableViewDelegate, UITableViewDataSource
{
    
    func settable()
    {
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //MARK: Table View return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dic.count == 0{
            return 0
        }
        let driverInfoDic = dic["driverInfo"] as! NSDictionary
        print(driverInfoDic)
        if driverInfoDic.removeAllNullValues().count != 0{
            return 3
        }
        print(driverInfoDic.removeAllNullValues())
        return 2
        
    }
    
    //MARK: Table View Data
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell0: HMCOrderDetailCell0!
        var cell1: HMCOrderDeatailCell1!
        var cell2: HMCOrderDetailCell2!
        let rowCount = tableView.numberOfRowsInSection(0)
        if rowCount == 3{
            if (indexPath.row == 0) {
                cell0 = configureHMCOrderDetailCell0(indexPath)
                cell0.selectionStyle = .None
                return cell0
            }else if (indexPath.row == 1) {
                cell1 = configureHMCOrderDetailCell1(indexPath)
                cell1.selectionStyle = .None
                return cell1
            }else if (indexPath.row == 2) {
                cell2 = configureHMCOrderDetailCell2(indexPath)
                cell2.selectionStyle = .None
                return cell2
                
            }
            
        }
        else {
            if (indexPath.row == 0) {
                cell0 = configureHMCOrderDetailCell0(indexPath)
                cell0.selectionStyle = .None
                return cell0
            }
            else if (indexPath.row == 1) {
                cell2 = configureHMCOrderDetailCell2(indexPath)
                cell2.selectionStyle = .None
                return cell2
                
            }
        }
        
        
        return cell0
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        
        return 65
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        let rowCount = tableView.numberOfRowsInSection(0)
        
        if rowCount == 3 {
            if indexPath.row == 0{
                return UITableViewAutomaticDimension
            }
            else  if indexPath.row == 1{
                return 145
            }
            else  if indexPath.row == 2{
                return 65
            }
            
        }
        else {
            if indexPath.row == 0{
                return UITableViewAutomaticDimension
            }
            else  if indexPath.row == 1{
                return 65
            }
        }
        
        return 65
    }
    
    
    func configureHMCOrderDetailCell0(indexPath : NSIndexPath)-> HMCOrderDetailCell0{
        let cell0 = tableView.dequeueReusableCellWithIdentifier("HMCOrderDetailCell0", forIndexPath: indexPath) as!  HMCOrderDetailCell0
        let prz = dic["orderItemId"] as? Int
        cell0.orderNo.text = String(prz!)
        cell0.customerName.text = dic["customerName"] as? String
        return cell0
        
    }
    func configureHMCOrderDetailCell1(indexPath : NSIndexPath)-> HMCOrderDeatailCell1{
        let cell1 = tableView.dequeueReusableCellWithIdentifier("HMCOrderDeatailCell1", forIndexPath: indexPath) as!  HMCOrderDeatailCell1
        let driverInfoDic = dic["driverInfo"] as! NSDictionary
        
        cell1.nameOfDeriver.text = driverInfoDic["driverName"] as? String
        cell1.mobileNo.text = "\(driverInfoDic["driverMobile"]!)"
        return cell1
        
    }
    func configureHMCOrderDetailCell2(indexPath : NSIndexPath)-> HMCOrderDetailCell2{
        let cell2 = tableView.dequeueReusableCellWithIdentifier("HMCOrderDetailCell2", forIndexPath: indexPath) as!  HMCOrderDetailCell2
        cell2.status.text = dic["orderStatus"] as? String
        
        return cell2
        
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.Normal, title: "Delete" , handler: { (action:UITableViewRowAction!, indexPath:NSIndexPath!) -> Void in
        })
        return [deleteAction]
    }
    
    //empty implementation
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}
