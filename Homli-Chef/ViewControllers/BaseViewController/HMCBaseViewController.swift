//
//  HMCBaseViewController.swift
//  Homli-Chef
//
//  Created by daffolapmac on 28/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
         setLeftMenuButton()
    }
    /**
     This method set the left navigation bar menu button
     */
    func setLeftMenuButton()
    {
        let buttonImage = UIImage(named: "icn_menu.png");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.Custom)
        leftBarButton.frame = CGRectMake(0, 0, 30,  30) ;
        leftBarButton.setImage(buttonImage, forState: UIControlState.Normal)
        leftBarButton.addTarget(self, action:#selector(HMCBaseViewController.leftMenuBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        
        self.navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: false);
    }
    func leftMenuBtnClicked(sender:UIButton!)
    {
        self.toggleLeftSide()
        
    }
    /**
     This method set the back button
     */
    func setBackButton()
    {
        let buttonImage = UIImage(named: "ic_action_close");
        let leftBarButton: UIButton = UIButton(type: UIButtonType.Custom)
        leftBarButton.frame = CGRectMake(0, 0, 40,  40) ;
        leftBarButton.setImage(buttonImage, forState: UIControlState.Normal)
        leftBarButton.addTarget(self, action:#selector(HMCBaseViewController.backBtnClicked(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        let leftBarButtonItem: UIBarButtonItem = UIBarButtonItem(customView: leftBarButton)
        
        self.navigationItem.setLeftBarButtonItem(leftBarButtonItem, animated: false);
    }

    func backBtnClicked(sender:UIButton!)
    {
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
