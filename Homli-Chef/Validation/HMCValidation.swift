//
//  HMCValidation.swift
//  Homli-Chef
//
//  Created by daffolapmac on 30/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
class HMCValidation {
    class func validateSignIn(mobileNumber:String)->Bool
    {
        if(mobileNumber.characters.count==0)
        {
            showErrorMessage(ENTER_MOBILE_NUMBER)
            return false
        }
        if(!validateContactNumber(mobileNumber))
        {
            showErrorMessage(ENTER_VALID_MOBILE_NUMBER)
            return false
        }
        return true
    }
    class func validateSignUp()->Bool
    {
        let signUpModel = HMCSignUpModel.sharedInstance()
        if(signUpModel.identityProof == nil)
        {
            showErrorMessage(ENTER_IDENTITY_PROOF)
            return false
        }
        if(signUpModel.addressProof == nil)
        {
            showErrorMessage(ENTER_ADDRESS_PROOF)
            return false
        }
        if(signUpModel.bankProof == nil)
        {
            showErrorMessage(ENTER_BANK_PROOF)
            return false
        }
        if(signUpModel.profilePicture == nil)
        {
            showErrorMessage(ENTER_PROFILE_PICTURE)
            return false
        }
        if(signUpModel.cuisines.count == 0)
        {
            showErrorMessage(ENTER_CUISINE)
            return false
        }
        if(signUpModel.numberOfPlate == nil || signUpModel.numberOfPlate.characters.count == 0)
        {
            showErrorMessage(ENTER_NUMBER_OF_PLATES)
            return false
        }
        if(Int(signUpModel.numberOfPlate) > 10 )
        {
            showErrorMessage(ENTER_PLATE_LIMIT)
            return false
        }

        if(!signUpModel.veg && !signUpModel.nonVeg)
        {
            showErrorMessage(ENTER_PREFERENCES)
            return false
        }
        
       return true
      }
    
    class func validateUpdateCookingDetails()->Bool
    {
        let signUpModel = HMCSignUpModel.sharedInstance()
        if(signUpModel.cuisines.count == 0)
        {
            showErrorMessage(ENTER_CUISINE)
            return false
        }
                
        if(!signUpModel.veg && !signUpModel.nonVeg)
        {
            showErrorMessage(ENTER_PREFERENCES)
            return false
        }
        
        return true

    }
    
    class func validateContactUs(msg:String ,email:String ) -> Bool
    {
        if(msg.isEmpty || msg == "Message")
        {
                showErrorMessage(ENTER_MESSAGE)
                return false
        }
        if (!email.isEmpty)
        {
        
        if(!isValidEmail(email as String ))
        {
            showErrorMessage(ENTER_VALID_EMAIL_ID)
            return false
        }
        }
        return true;
    }
    
    class func validateCreateNewPlate()->Bool
    {
        let signUpModel = HMCCreatePlateModel.sharedInstance()
        if(signUpModel.mainCourse.count == 0)
        {
            showErrorMessage(ENTER_MAIN_COURSE)
            return false
        }
        if(signUpModel.subCourse1.count == 0)
        {
            showErrorMessage(ENTER_SUB_COURSE)
            return false
        }
        
        
       
        return true
        
    }

    
    
    class func validateOTP(otp:NSString) -> Bool
    {
        if(otp.length == 0)
        {
            showErrorMessage(ENTER_OTP)
            return false
        }
        
        return true
    }

    
    
    
    class func showErrorMessage(message:String)
    {
        let vc : UIViewController = HMCUtlities.getTopViewController()
        vc.showErrorMessage(message)
    }
    
    class func validateContactNumber(contact:NSString) -> Bool
    {
        
        if(contact.length == 10)
        {
            return true
        }
        
        return false
        
    }
    
    class func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(email)
    }
    

}