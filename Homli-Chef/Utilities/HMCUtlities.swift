//
//  HMCUtlities.swift
//  Homli-Chef
//
//  Created by daffolapmac on 30/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import MFSideMenu
import RappleProgressHUD
class HMCUtlities: NSObject,UITabBarControllerDelegate {
    var mainNC = UINavigationController()
    
    class var sharedInstance: HMCUtlities {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: HMCUtlities? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = HMCUtlities()
        }
        return Static.instance!
    }
    
    
    
    class func saveUserProfile(userProfile:NSDictionary)
    {
        
        let userProf = userProfile.removeAllNullValues()
        print(userProf)
        var tmpDic = NSMutableDictionary()
        if (userProf["address"] != nil){
            
            tmpDic = (userProf["address"]?.removeAllNullValues())!
            print(tmpDic)
            if tmpDic.count == 0{
                
                userProf.removeObjectForKey("address")
            }
            else
            {
                userProf.setObject(tmpDic, forKey: "address")
            }
        }
        print(userProf)
        tmpDic = (userProf["cookingDetails"]?.removeAllNullValues())!
        userProf.setObject(tmpDic, forKey: "cookingDetails")
        
        print(userProf)
        UserDefault.setObject(userProf, forKey: UserProfile)
        
        UserDefault.synchronize()
    }
    
    
    class func getSignInUserDetail()-> NSMutableDictionary
    {
        if HMCUtlities.isUserLogedIn(){
            return UserDefault.objectForKey(UserProfile) as! NSMutableDictionary
            
        }
        return [:]
    }
    
    class func isUserLogedIn() -> Bool
    {
        if UserDefault.objectForKey(UserProfile) != nil{
            return true
        }
        return false
    }
    
    
    class func signOutFromApp()
    {
        HMCUtlities.setUserBasicCheckWithDefaultInitialValue()
        HMCUtlities.setUserCurrentStatus("")
        HMCUtlities.setUserStatus("")
        UserDefault.removeObjectForKey(UserProfile)
        
    }
    class func saveUserContactNumber(contact:String)
    {
        UserDefault.setObject(contact, forKey: ContactNumber)
        UserDefault.synchronize()
        
    }
    
    class func setUserBasicCheckWithDefaultInitialValue()
    {
        
        UserDefault.setObject(false, forKey: User_OTP_Status)
        UserDefault.setObject(false, forKey: User_QNA_Status)
        UserDefault.setObject(false, forKey: User_TNC_Status)
        UserDefault.synchronize()
        
    }
    class func setOTPStatus(status:Bool)
    {
        UserDefault.setObject(status, forKey: User_OTP_Status)
        UserDefault.synchronize()
    }
    class func setTNCStatus(status:Bool)
    {
        UserDefault.setObject(status, forKey: User_TNC_Status)
        UserDefault.synchronize()
    }
    class func setQNAStatus(status:Bool)
    {
        UserDefault.setObject(status, forKey: User_QNA_Status)
        UserDefault.synchronize()
    }
    
    class func setUserCurrentStatus(status:String)
    {
        UserDefault.setObject(status, forKey: User_Current_Status)
        UserDefault.synchronize()
    }
    
    class func setUserStatus(status:String)
    {
        UserDefault.setObject(status, forKey: User_Status)
        UserDefault.synchronize()
    }
    
    class func isOTPVerified()-> Bool
    {
        
        return UserDefault.objectForKey(User_OTP_Status) as! Bool
        
    }
    class func isTermsNConditionAccepted()-> Bool
    {
        
        return UserDefault.objectForKey(User_TNC_Status) as! Bool
        
    }
    class func isQNASubmitted()-> Bool
    {
        
        return UserDefault.objectForKey(User_QNA_Status) as! Bool
        
    }
    
    class func getUserCurrentStatus() -> String
    {
        if UserDefault.objectForKey(User_Current_Status) != nil{
            return UserDefault.objectForKey(User_Current_Status) as! String
        }
        return ""
        
    }
    class func getUserStatus() -> String
    {
        if UserDefault.objectForKey(User_Status) != nil{
            return UserDefault.objectForKey(User_Status) as! String
        }
        return ""
        
    }
    
    class func getUserFullName() -> String
    {
        let userInfo = HMCUtlities.getSignInUserDetail()
        var fullName = ""
        
        if (userInfo[User_First_Name] != nil) {
            
            fullName = "\(userInfo[User_First_Name])"
        }
        if (userInfo[User_Middle_Name] != nil) {
            
            fullName = fullName + "\(userInfo[User_Middle_Name])"
        }
        if (userInfo[User_Last_Name] != nil) {
            
            fullName = fullName + "\(userInfo[User_Last_Name])"
        }
        return fullName
        
    }
    
   class func getUserFirstnLastName() -> String
    {
        let userInfo = HMCUtlities.getSignInUserDetail()
        var name = User_Name
        
        if userInfo[User_First_Name] != nil{
            
            name = userInfo[User_First_Name] as! String
            
        }
        if userInfo[User_Last_Name] != nil{
            
            name =  "\(name) \(userInfo[User_Last_Name])"
        }
        return name

    }
    
    class func getUserContactNumber()-> String
    {
        if (NSUserDefaults.standardUserDefaults().objectForKey(ContactNumber) != nil) {
            return UserDefault.objectForKey(ContactNumber) as! String
        }
        
        return ""
    }
    
    class func getUserPreferredDishes() -> NSMutableArray
    {
        let userInfo = HMCUtlities.getSignInUserDetail()
        var preferredDishes = NSArray()
        
        if (userInfo["chefQnA"] != nil) {
            
            let chefQnA = userInfo["chefQnA"] as! NSDictionary
            
            preferredDishes = chefQnA["preferredDishes"] as! NSArray
            
            //preferredDishes = tmpArr.valueForKey("dishName") as! NSArray
            
        }
        
        return preferredDishes.mutableCopy() as! NSMutableArray
        
    }
    
    
    
    
    
    func getCurrentNavigationController() -> UINavigationController
    {
        let navigationController = UIApplication.sharedApplication().keyWindow?.rootViewController?.navigationController
        return navigationController!
    }
    class func getTopViewController()-> UIViewController
    {
        
        
        let topController = UIApplication.sharedApplication().keyWindow?.rootViewController
        var vc = UIViewController()
        if  ((topController?.isKindOfClass(MFSideMenuContainerViewController)) != nil) {
            
            let  sideMenuVC : MFSideMenuContainerViewController = topController as! MFSideMenuContainerViewController
            let navController = sideMenuVC.centerViewController
            vc = navController.topViewController!!
            
            
        }
        
        return vc
        
        
    }
    
    
    
    class func showErrorMessage(message:String)
    {
        let vc : UIViewController = HMCUtlities.getTopViewController()
        vc.showErrorMessage(message)
    }
    class func showSuccessMessage(message:String)
    {
        let vc : UIViewController = HMCUtlities.getTopViewController()
        vc.showSuccessMessage(message)
    }

    
    class func showLoader()
    {
        RappleActivityIndicatorView.startAnimatingWithLabel("Loading...", attributes: RappleAppleAttributes)
    }
    class func showLoaderWithMessage(message:String)
    {
        RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: RappleAppleAttributes)
    }
    class func hideLoader()
    {
        RappleActivityIndicatorView.stopAnimating()
    }
    class func hideNavigationBar()
    {
        let vc = HMCUtlities.getTopViewController()
        vc.navigationController!.navigationBarHidden = true
    }
    class func showNavigationBar()
    {
        let vc = HMCUtlities.getTopViewController()
        vc.navigationController!.navigationBarHidden = false
    }
    
    func navigateToHomeViewController()
    {
        let vc = HMCUtlities.getTopViewController()
        let navC =   vc.navigationController
        let arr = (navC?.viewControllers)! as NSArray
        let vc2 = arr.firstObject as! UIViewController
        mainNC  = navC!

        let nav1 = UINavigationController()
        let yourOrderViewController = vc2.storyboard!.instantiateViewControllerWithIdentifier("HMCYourOrderViewController") as? HMCYourOrderViewController
        nav1.viewControllers = [yourOrderViewController!]
        nav1.tabBarItem.title = "Home"
        nav1.tabBarItem.image = UIImage(named: "ic_tab_home")
        nav1.tabBarItem.selectedImage = UIImage(named: "ic_tab_home_selection")
        nav1.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -4.0)
       
        let nav2 = UINavigationController()
        let todaysPickupsViewController = vc2.storyboard!.instantiateViewControllerWithIdentifier("HMCTodaysPickupsViewController") as? HMCTodaysPickupsViewController
        nav2.viewControllers = [todaysPickupsViewController!]
        nav2.title = "Delivery"
        nav2.tabBarItem.image = UIImage(named: "ic_tab_delivery")
        nav2.tabBarItem.selectedImage = UIImage(named: "ic_tab_delivery_selection")
        nav2.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -4.0)

        let nav3 = UINavigationController()
        let earningsViewController = vc2.storyboard!.instantiateViewControllerWithIdentifier("HMCEarningsViewController") as? HMCEarningsViewController
        nav3.viewControllers = [earningsViewController!]
        nav3.title = "Earning"
        nav3.tabBarItem.image = UIImage(named: "ic_tab_earning")
        nav3.tabBarItem.selectedImage = UIImage(named: "ic_tab_earning_selection")
        nav3.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -4.0)

        
        let fourth = UIViewController()
        let nav4 = UINavigationController()
        nav4.viewControllers = [fourth]
        nav4.title = "Your Plate"
        nav4.tabBarItem.image = UIImage(named: "ic_tab_postplate")
        nav4.tabBarItem.selectedImage = UIImage(named: "ic_tab_postplate_selection")
        nav4.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -4.0)
        
        let tabsController = UITabBarController()
        tabsController.delegate = self
        tabsController.tabBar.barTintColor = ColorFromHexaCode(k_Header_Color)
      
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "OpenSans", size: 14)!,NSForegroundColorAttributeName : UIColor.whiteColor()], forState: UIControlState.Normal)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName:UIFont(name: "OpenSans", size: 14)!,NSForegroundColorAttributeName : ColorFromHexaCode(k_Light_BlackColor)], forState: UIControlState.Selected)
        tabsController.viewControllers = [nav1,nav2,nav3,nav4]
        UITabBar.appearance().tintColor = UIColor.blackColor()
        
        let numberOfItems = CGFloat(tabsController.tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabsController.tabBar.frame.width / numberOfItems, height: tabsController.tabBar.frame.height)
        tabsController.tabBar.selectionIndicatorImage = UIImage.imageWithColor(ColorFromHexaCode(k_Light_OrangeColor), size: tabBarItemSize).resizableImageWithCapInsets(UIEdgeInsetsZero)
        tabsController.tabBar.frame.size.width = screenWidth + 4
        tabsController.tabBar.frame.origin.x = -2
       
       vc2.menuContainerViewController.centerViewController.pushViewController(tabsController, animated: true)
    
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        
        let userStatus =  HMCUtlities.getUserCurrentStatus()
        if (!(userStatus == ChefStatus.ACTIVE.description)){
            return false
        }
        
        
        let vc = HMCUtlities.getTopViewController()
        let navC =   vc.navigationController
        mainNC  = navC!
        if viewController.title == "Your Plate"{
            let arr = (navC?.viewControllers)! as NSArray
            let vc2 = arr.firstObject
            let yourPlatesViewController = (vc2!.storyboard!!.instantiateViewControllerWithIdentifier("HMCYourPlatesViewController") as? HMCYourPlatesViewController)!
        
            navC?.pushViewController(yourPlatesViewController, animated: true)
            return false
        }
        return true
    }
     
}
