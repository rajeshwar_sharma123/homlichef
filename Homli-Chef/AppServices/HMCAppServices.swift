//
//  HMCAppServices.swift
//  Homli-Chef
//
//  Created by daffolapmac on 29/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit
import Alamofire
class HMCAppServices: NSObject {
    
    static let instance = HMCAppServices()
    
    class func sharedInstance() -> HMCAppServices
    {
        return instance;
    }

    
    let headers = [
        "Authorization": "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
        "Content-Type": "application/json",
        "charset" : "utf-8"
    ]

    
    
    
    
    
    
    func performPostResquest(
        urlString: String,
        requestData: AnyObject,
        successBlock:(request: AnyObject?, response: AnyObject?, headers:AnyObject? )->(),
        errorBlock:(error: AnyObject, headers: NSDictionary)->()
        )
    {
            Alamofire.request(.POST , urlString,headers: headers,encoding:.JSON, parameters: requestData as? [String : AnyObject])
            .responseJSON() { response in
                if(response.result.isSuccess){
                    let responseData = response.result.value as! NSDictionary
                    let headersData = response.response!.allHeaderFields as NSDictionary
                    successBlock(request:response.request, response: responseData, headers:headersData)
                }
                else{
                    let headersData = response.response!.allHeaderFields as NSDictionary
                    errorBlock(error : response.result.error!,  headers: headersData)
                    
                }
                
        }
    }
    
    
   class func postServiceRequest(
        urlString urlString: String, parameter: AnyObject,
        successBlock:(responseData: AnyObject)->(),
        errorBlock:(errorMessage: NSError)->()
        )
    {
        
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameter, options: [])
       
        Alamofire.request(request)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Failure(let error):
                    
                    errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):
                    
                    print(responseObject)
                    
                    successBlock(responseData: responseObject as! NSDictionary)
                    
                    
                }
        }
        
    }
    
    class func getServiceRequest(
        urlString urlString: String ,
        successBlock:(responseData: AnyObject)->(),
        errorBlock:(errorMessage: NSError)->()
        )
    {
        let request = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        Alamofire.request(request)
            .responseJSON { response in
                
                switch response.result {
                    
                case .Failure(let error):
                    
                    errorBlock(errorMessage: error)
                    
                case .Success(let responseObject):
                    
                    print(responseObject)
                    
                    successBlock(responseData: responseObject as! NSDictionary)
                    
                    
                }
        }
        
    }

}
