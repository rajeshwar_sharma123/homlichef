//
//  String+Utility.swift
//  Homli
//
//  Created by daffolapmac on 16/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
extension String
{

    func truncateLastTwoCharacter() -> String
    {
        let endIndex = self.endIndex.advancedBy(-2)
        return self.substringToIndex(endIndex)
    }
    
    func convertToDate() -> String
    {
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "d-MM-yyyy"
         let date: NSDate = formatter.dateFromString(self)!
        formatter.dateFormat = "d MMMM yyyy"
        let newDate: String = formatter.stringFromDate(date)
        return newDate

    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }

}
