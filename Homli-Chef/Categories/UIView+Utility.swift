//
//  UIView+Utility.swift
//  Homli
//
//  Created by daffolapmac on 15/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import UIKit
import RappleProgressHUD
extension UIView {
    
    func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
    
    func transparentBackground()
    {
        self.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
    }
    func transparentBackground(withAlphaValue aplha: Float)
    {
        self.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: aplha)
    }
    
    func showLoader()
    {
        RappleActivityIndicatorView.startAnimatingWithLabel("Loading...", attributes: RappleAppleAttributes)
        //RappleActivityIndicatorView.startAnimating(attributes: RappleAppleAttributes)
        //RappleActivityIndicatorView.startAnimating()
    }
    func showLoaderWithMessage(message:String)
    {
        RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: RappleAppleAttributes)
    }
    func hideLoader()
    {
        RappleActivityIndicatorView.stopAnimating()
    }
    
    
}