//
//  UITableView+Utility.swift
//  Homli-Chef
//
//  Created by daffolapmac on 20/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
extension UITableView {
    
    func removeEmptyRows(){
        self.tableFooterView = UIView()
    }
    
}