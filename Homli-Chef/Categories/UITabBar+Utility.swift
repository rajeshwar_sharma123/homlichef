//
//  UITabBar+Utility.swift
//  Homli-Chef
//
//  Created by daffolapmac on 15/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
extension UITabBar {
    
    override public func sizeThatFits(size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 60
        return sizeThatFits
    }
}
