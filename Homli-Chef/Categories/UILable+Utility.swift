//
//  UILable+Utility.swift
//  Homli-Chef
//
//  Created by daffolapmac on 08/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation

extension UILabel
{
    
    func setAttributedStringWith(txtName:String,fontFamily family:String,fontSize size:CGFloat,hexaColorCode rgbColorValue: UInt)
    {
        let attributedString = NSMutableAttributedString(string: txtName, attributes: [NSFontAttributeName : UIFont(name: family , size: size)!,NSForegroundColorAttributeName : ColorFromHexaCode(rgbColorValue)])
        self.attributedText = attributedString
    }
    
    func setTwoTypesAttributedString(txtName:String,subText subTxtName:String,fontFamilyForTxtName family:String,fontFamilyForSubTxtName subfamily:String,fontSizeTxtName size:CGFloat,fontSizeSubTxtName subSize:CGFloat)
    {
        let longString = txtName
        let longestWord = subTxtName
        let longestWordRange = (longString as NSString).rangeOfString(longestWord)
        let attributedString = NSMutableAttributedString(string: longString, attributes: [NSFontAttributeName : UIFont(name: family, size: size)!])
        attributedString.setAttributes([NSFontAttributeName : UIFont(name: subfamily, size: subSize)!, NSForegroundColorAttributeName : UIColor.whiteColor()], range: longestWordRange)
        self.attributedText = attributedString
    }

    
}