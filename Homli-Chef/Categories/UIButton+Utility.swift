//
//  UIButton+Utility.swift
//  Homli
//
//  Created by daffolapmac on 23/03/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
extension UIButton
{
    //
    func setImageColorFromHexaCodeForNormalState(hexaValue: UInt)
    {
        
        let origImage = self.imageForState(UIControlState.Normal)
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.setImage(tintedImage, forState: .Normal)
        self.tintColor = ColorFromHexaCode(hexaValue)
    }
    func setImageColorFromHexaCodeForHighlightedState(hexaValue: UInt)
    {
        
        let origImage = self.imageForState(UIControlState.Highlighted)
        let tintedImage = origImage?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
        self.setImage(tintedImage, forState: .Highlighted)
        self.tintColor = ColorFromHexaCode(hexaValue)
    }

    
}