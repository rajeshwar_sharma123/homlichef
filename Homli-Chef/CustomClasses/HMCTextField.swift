//
//  HMCTextField.swift
//  Homli-Chef
//
//  Created by daffolapmac on 31/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import UIKit

class HMCTextField: UITextField,UITextFieldDelegate {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder){
        
        super.init(coder: aDecoder)
        self.backgroundColor = ColorFromHexaCode(0xF1F1F1)
        self.layer.cornerRadius = self.frame.size.height/2
        self.clipsToBounds = true
        self.leftPadding(25)
        
    }
    
    func addButtonOnLeftWithImageName(name:String)
    {
        let paddingView = UIView(frame:CGRectMake(0, 0,46, self.frame.size.height))
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.enabled = false
        button.frame = CGRectMake(22, (self.frame.height-20)/2, 16, 20)
        self.leftViewMode = UITextFieldViewMode.Always
        //button.backgroundColor = UIColor.redColor()
        button.setBackgroundImage(UIImage(named: name), forState: UIControlState.Normal)
        paddingView.addSubview(button)
        self.leftView = paddingView
        
        
    }
    func addButtonOnRightWithImageName(name:String)
    {
        let paddingView = UIView(frame:CGRectMake(0, 0,46, self.frame.size.height))
        //paddingView.backgroundColor = UIColor.redColor()
        let button   = UIButton(type: UIButtonType.System) as UIButton
        button.enabled = false
        button.frame = CGRectMake(10, (self.frame.height-20)/2, 20, 20)
        self.rightViewMode = UITextFieldViewMode.Always
        button.setBackgroundImage(UIImage(named: name), forState: UIControlState.Normal)
        paddingView.addSubview(button)
        self.rightView = paddingView
        
        
    }
    


}
