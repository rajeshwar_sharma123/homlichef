//
//  HMCConstants.swift
//  Homli-Chef
//
//  Created by daffolapmac on 25/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation
// ***********************************  Basic String Constants ****************************************

let UserDefault = NSUserDefaults.standardUserDefaults()
let App_Delegate = UIApplication.sharedApplication().delegate as! AppDelegate
let ContactNumber                = "contactNumber"
let UserBasickCheck              = "UserBasickCheck"
let User_OTP_Status              = "OTP_STATUS"
let User_QNA_Status              = "QNA_STATUS"
let User_TNC_Status              = "TNC_STATUS"
let UserProfile                  = "userProfile"
let User_Current_Status          = "userCurrentStatus"
let User_Status                  = "userStatus"
let No_Referral                  = "Oops... no referral code is available"
let User_Name                    = "User Name"
let User_First_Name              = "firstName"
let User_Middle_Name             = "middleName"
let User_Last_Name               = "lastName"



// **********************************  Check for Device ************************


let screenWidth = UIScreen.mainScreen().bounds.width
let screenHeight = UIScreen.mainScreen().bounds.height
let COMMON_SIZE  = screenWidth == 320 ? 21 : 24 as CGFloat
// ************************************* Validation messages ****************************************

let ENTER_MOBILE_NUMBER          = "Please enter contact number"
let ENTER_VALID_MOBILE_NUMBER    = "Please enter valid contact number"
let ENTER_IDENTITY_PROOF         = "Please provide your Identity Proof."
let ENTER_ADDRESS_PROOF          = "Please provide your Address Proof."
let ENTER_BANK_PROOF             = "Please provide your Bank Proof."
let ENTER_PROFILE_PICTURE        = "Please provide your Profile Picture."
let ENTER_CUISINE                = "Please provide cuisines you cook."
let ENTER_NUMBER_OF_PLATES       = "Please provide number of plate you can cook in a day."
let ENTER_PLATE_LIMIT            = "You can give maximum 10 plates only"
let ENTER_PREFERENCES            = "Please provide which type of food you can cook."
let ENTER_OTP                    = "Please enter OTP"
let ENTER_MESSAGE                = "Please enter Message"
let ENTER_VALID_EMAIL_ID         = "Please enter valid email id"
let ENTER_MAIN_COURSE            = "Please add your Main Course first"
let ENTER_SUB_COURSE             = "A main course and minimum 1 sub-course required to create Plate"

/* User Profile */
let Update_Cooking_Details_Successful  = "Cooking details updated successfully."
let Top_Dishes_Updated_Successfully    = "Top dishes Updated successfully."
let Exceeded_top_dishes                = "You have already added 10 dishes."
let Add_top_dishes                     = "Please add your top 10 dishes."

/*ContactUs */
let Query_successful                 = "Your query submitted successfully."


// ***********************************  AWS3 Constants ****************************************


let S3BucketName = "homli-chef-images-ios"
let CognitoRegionType = AWSRegionType.USEast1  // e.g. AWSRegionType.USEast1
let DefaultServiceRegionType = AWSRegionType.USEast1 // e.g. AWSRegionType.USEast1
let CognitoIdentityPoolId = "us-east-1:88e9d2e8-830b-426c-9174-b4f0a27dec92"



