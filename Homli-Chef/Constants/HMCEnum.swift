//
//  HMCEnum.swift
//  Homli-Chef
//
//  Created by daffolapmac on 05/04/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation


enum ChefStatus: String {
    
    case ACTIVE
    case NOT_REGISTERED
    case REGISTRATION_UNDER_REVIEW
    case REGISTRATION_DOCS_PENDING
    var description: String {
        return self.rawValue
    }
    
}

enum DishCategoryCourse: String {
    
    case MAIN_COURSE
    case SUB_COURSE
    case SUB_COURSE1
    case SUB_COURSE2
    case SUB_COURSE_1
    case SUB_COURSE_2
    
    var description: String {
        return self.rawValue
    }
}
enum PostPlateCheck: String {
    
    case RECOMMENDATION
    case PREVIOUS_PLATE
    
    var description: String {
        return self.rawValue
}
}


