//
//  HMCConfig.swift
//  Homli-Chef
//
//  Created by daffolapmac on 29/03/16.
//  Copyright © 2016 Daffodil Software Ltd. Gurgaon. All rights reserved.
//

import Foundation

// ************************************* Url Constants ****************************************

//**************** Base Url for live *********************************
/*let Base_Url = "http://ec2-54-149-185-211.us-west-2.compute.amazonaws.com:8080/lunchbox"
let Url_SignIn                 = "/chef/login"
let Url_Cuisines_List          = "/cuisines/cuisinesList"
let Url_Get_Doument_Proof      = "/chef/currentDocumentProofs"


let Url_Register               = "/chef/registerNewChef"
let Url_Find_Dish              = "/dish/findDish"
*/



//**************** Base Url for Dev *********************************

let Base_Url = "http://ec2-54-254-134-129.ap-southeast-1.compute.amazonaws.com:8080/homli"


let Url_SignIn                 = "/chef/v1/login"
let Url_Cuisines_List          = "/cuisines/v1/cuisinesList"
let Url_Get_Doument_Proof      = "/chef/v1/currentDocumentProofs"
let Url_Register               = "/chef/v1/registerNewChef"
let Url_Find_Dish              = "/dish/v1/findDish"
//let Url_Chef_List              = "/chef/v1/getChefDetails"
let Url_Read_User_Detail       = "/chef/v1/getChefDetails"
let Url_Confirm_OTP            = "/chef/v1/otp/confirmotp"
let Url_Resend_OTP             = "/chef/v1/otp/resendotp"
let Url_Check_OTP_Status       = "/chef/v1/otp/checkotpstatus"
let Url_Check_TnC_Status       = "/chef/v1/isTnCAccepted"
let Url_Check_QnA_Status       = "/chef/v1/qna"
let Url_Accept_TnC             = "/chef/v1/tncAccepted"
let Url_Submit_QnA             = "/chef/v1/qna"
let Url_Balance_List           = "/chef/v1/balance"
let Url_ContactUs_Query        = "/user/v1/query"
let Url_Update_Top_Dishes      = "/chef/v1/updateTopDishes"
let Url_Update_Cooking_Details = "/chef/v1/updateCookingDetails"
let Url_Referral_Code          = "/chef/v1/referral"
let Url_Earings                = "/chef/v1/getChefEarnings"
let Url_TodayPickUps           = "/chef/v1/getDriverPickups"


//Your Orders Api
let Url_Chef_Plate_Orders      = "/chef/v1/getChefPlateOrders"
let Url_Order_Details          = "/chef/v1/getOrderDetails"

//Post Plate
let Url_Chef_Plate_Inventory  = "/plate/v1/getChefPlateInventory"
let Url_Recommendation        = "/chef/v1/recommendation"
let Url_Previous_Plates       = "/plate/v1/findPreviousPlates"
let Url_Plate_Detail          = "/plate/v1"
let Url_Create_Plate          = "/plate/v1/createNewPlate"
let Url_Submit_Plate          = "/plate/v1/submitPlateForChef"
let Url_Update_Plate_Num      = "/plate/v1/updateNumberOfPlates"
let Url_Cancel_Plate          = "/plate/v1/cancelChefPlate"




let Url_TnC                    = "http://homli.in/tnc";
let Url_Help                   = "http://homli.in/chefHelp";



//let Url_First_Detail           = "/firstDetails"
//let Url_Request_OTP            = "/requestotp"
//let Url_Menu_Plate_Search      = "/menu/plateSearch"



// *******************************  Basic Config Constants ****************************


